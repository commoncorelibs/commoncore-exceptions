# Change Log

## Release v1.0.0-pre.7

* Removed all deprecated code #23
* Add `Path` hooks #29

### Dependencies

* [CommonCore.Shims](https://gitlab.com/commoncorelibs/commoncore-shims) made a non-conditional dependency #28

## 1.0.0-pre.6

* Deprecate `Throw.As.Arg.Index.InvalidInclusive()` as warning [#25](https://gitlab.com/commoncorelibs/commoncore-exceptions/-/issues/25)
* Deprecate `Throw.As.Arg.Index.ValidInclusive()` as warning [#25](https://gitlab.com/commoncorelibs/commoncore-exceptions/-/issues/25)
* Add `Throw.If.Arg.Index.Invalid()` overload with `int` length parameter [#24](https://gitlab.com/commoncorelibs/commoncore-exceptions/-/issues/24)
* Add `Throw.Try.Arg.Index.Invalid()` overload with `int` length parameter [#24](https://gitlab.com/commoncorelibs/commoncore-exceptions/-/issues/24)
* Add `Throw.If.Arg.Index.Invalid()` inclusive length `bool` parameter [#25](https://gitlab.com/commoncorelibs/commoncore-exceptions/-/issues/25)
* Add `Throw.Try.Arg.Index.Invalid()` inclusive length `bool` parameter [#25](https://gitlab.com/commoncorelibs/commoncore-exceptions/-/issues/25)

### Dependencies

These changes only affect the dotnet standard 2.0 target.

* Add direct dependency [CommonCore.Shims](https://gitlab.com/commoncorelibs/commoncore-shims) [#26](https://gitlab.com/commoncorelibs/commoncore-exceptions/-/issues/26)
* Remove direct dependency [System.Memory](https://www.nuget.org/packages/System.Memory/) [#26](https://gitlab.com/commoncorelibs/commoncore-exceptions/-/issues/26)
* Remove direct dependency [Microsoft.Bcl.HashCode](https://www.nuget.org/packages/Microsoft.Bcl.HashCode) [#26](https://gitlab.com/commoncorelibs/commoncore-exceptions/-/issues/26)

## 1.0.0-pre.5

* Stabilize the library [#14](https://gitlab.com/commoncorelibs/commoncore-exceptions/-/issues/14)
* Deprecate old api as error [#13](https://gitlab.com/commoncorelibs/commoncore-exceptions/-/issues/13)

## 1.0.0-pre.1

* Initial project creation.
