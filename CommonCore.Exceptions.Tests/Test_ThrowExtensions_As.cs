﻿#region Copyright (c) 2021 Jay Jeckel
// Copyright (c) 2021 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using Xunit;

namespace CommonCore.Exceptions.Tests
{
    [ExcludeFromCodeCoverage]
    public class Test_ThrowExtensions_As
    {

        [Fact]
        public void General()
        {
            Throw.As.Null().Should().BeOfType<NullReferenceException>();

            Throw.As.Arg.Default("").Should().BeOfType<ArgumentDefaultException>();
            Throw.As.Arg.DefaultOrEmpty("").Should().BeOfType<ArgumentDefaultOrEmptyException>();
            Throw.As.Arg.Empty("").Should().BeOfType<ArgumentEmptyException>();
            Throw.As.Arg.Null("").Should().BeOfType<ArgumentNullException>();
            Throw.As.Arg.NullOrEmpty("").Should().BeOfType<ArgumentNullOrEmptyException>();
            Throw.As.Arg.ReadOnly("").Should().BeOfType<ArgumentReadOnlyException>();
            Throw.As.Arg.NotReadOnly("").Should().BeOfType<ArgumentReadOnlyException>();

            Throw.As.Arg.Enum.Invalid("", TestEnum.Zero).Should().BeOfType<ArgumentEnumOutOfRangeException>();

            Throw.As.Arg.Index.Invalid("", -1, 4, false).Should().BeOfType<ArgumentIndexOutOfRangeException>();
            Throw.As.Arg.Index.Invalid("", -1, 4, true).Should().BeOfType<ArgumentIndexOutOfRangeException>();
            Throw.As.Arg.Index.Valid("", 0, 4, false).Should().BeOfType<ArgumentIndexOutOfRangeException>();
            Throw.As.Arg.Index.Valid("", 0, 4, true).Should().BeOfType<ArgumentIndexOutOfRangeException>();

            Throw.As.Arg.Range.Less("", -1, 0).Should().BeOfType<ArgumentOutOfRangeException>();
            Throw.As.Arg.Range.LessOrEqual("", -1, 0).Should().BeOfType<ArgumentOutOfRangeException>();
            Throw.As.Arg.Range.More("", 1, 0).Should().BeOfType<ArgumentOutOfRangeException>();
            Throw.As.Arg.Range.MoreOrEqual("", 1, 0).Should().BeOfType<ArgumentOutOfRangeException>();
            Throw.As.Arg.Range.Outside("", -1, 0, 4, false, false).Should().BeOfType<ArgumentOutOfRangeException>();
            Throw.As.Arg.Range.Outside("", -1, 0, 4, false, true).Should().BeOfType<ArgumentOutOfRangeException>();
            Throw.As.Arg.Range.Outside("", -1, 0, 4, true, false).Should().BeOfType<ArgumentOutOfRangeException>();
            Throw.As.Arg.Range.Outside("", -1, 0, 4, true, true).Should().BeOfType<ArgumentOutOfRangeException>();
        }

    }
}
