﻿#region Copyright (c) 2021 Jay Jeckel
// Copyright (c) 2021 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using Xunit;

namespace CommonCore.Exceptions.Tests
{
    [ExcludeFromCodeCoverage]
    public class Test_ThrowExtensions_If_Arg_Enum
    {
        [Theory]
        [MemberData(nameof(TestData.GetData_Arg_Enum_Invalid), MemberType = typeof(TestData))]
        public void Throw_If_Arg_Enum_Invalid(bool throws, TestEnum value)
        {
            Action act = delegate { Throw.If.Arg.Enum.Invalid("arg", value); };
            if (!throws) { act.Should().NotThrow(); }
            else { act.Should().ThrowExactly<ArgumentEnumOutOfRangeException>(); }
        }
    }
}
