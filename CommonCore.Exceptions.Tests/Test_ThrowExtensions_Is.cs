#region Copyright (c) 2021 Jay Jeckel
// Copyright (c) 2021 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using Xunit;

namespace CommonCore.Exceptions.Tests
{
    [ExcludeFromCodeCoverage]
    public class Test_ThrowExtensions_Is
    {

        [Theory]
        [InlineData(true, 0)]
        [InlineData(false, 1)]
        public void Default(bool expected, int value)
            => Throw.Is.Default(value).Should().Be(expected);

        [Theory]
        [InlineData(true, null)]
        [InlineData(false, "")]
        public void Null(bool expected, object value)
            => Throw.Is.Null(value).Should().Be(expected);

        [Fact]
        public void Empty_False()
        {
            Throw.Is.Empty(TestItem.Value.Span).Should().BeFalse();
            Throw.Is.Empty(TestItem.Value.ReadOnlySpan).Should().BeFalse();
            Throw.Is.Empty(TestItem.Value.Memory).Should().BeFalse();
            Throw.Is.Empty(TestItem.Value.ReadOnlyMemory).Should().BeFalse();
            Throw.Is.Empty(TestItem.Value.Collection).Should().BeFalse();
            Throw.Is.Empty(TestItem.Value.CollectionGeneric).Should().BeFalse();
            Throw.Is.Empty(TestItem.Value.List).Should().BeFalse();
            Throw.Is.Empty(TestItem.Value.ListGeneric).Should().BeFalse();
            Throw.Is.Empty(TestItem.Value.String).Should().BeFalse();
            Throw.Is.Empty(TestItem.Value.Builder).Should().BeFalse();
            Throw.Is.Empty(TestItem.Value.Secure).Should().BeFalse();

            Throw.Is.Empty(TestItem.Null.Collection).Should().BeFalse();
            Throw.Is.Empty(TestItem.Null.CollectionGeneric).Should().BeFalse();
            Throw.Is.Empty(TestItem.Null.List).Should().BeFalse();
            Throw.Is.Empty(TestItem.Null.ListGeneric).Should().BeFalse();
            Throw.Is.Empty(TestItem.Null.String).Should().BeFalse();
            Throw.Is.Empty(TestItem.Null.Builder).Should().BeFalse();
            Throw.Is.Empty(TestItem.Null.Secure).Should().BeFalse();
        }

        [Fact]
        public void Empty_True()
        {
            Throw.Is.Empty(TestItem.Empty.Span).Should().BeTrue();
            Throw.Is.Empty(TestItem.Empty.ReadOnlySpan).Should().BeTrue();
            Throw.Is.Empty(TestItem.Empty.Memory).Should().BeTrue();
            Throw.Is.Empty(TestItem.Empty.ReadOnlyMemory).Should().BeTrue();
            Throw.Is.Empty(TestItem.Empty.Collection).Should().BeTrue();
            Throw.Is.Empty(TestItem.Empty.CollectionGeneric).Should().BeTrue();
            Throw.Is.Empty(TestItem.Empty.List).Should().BeTrue();
            Throw.Is.Empty(TestItem.Empty.ListGeneric).Should().BeTrue();
            Throw.Is.Empty(TestItem.Empty.String).Should().BeTrue();
            Throw.Is.Empty(TestItem.Empty.Builder).Should().BeTrue();
            Throw.Is.Empty(TestItem.Empty.Secure).Should().BeTrue();

            Throw.Is.Empty(TestItem.Null.Span).Should().BeTrue();
            Throw.Is.Empty(TestItem.Null.ReadOnlySpan).Should().BeTrue();
            Throw.Is.Empty(TestItem.Null.Memory).Should().BeTrue();
            Throw.Is.Empty(TestItem.Null.ReadOnlyMemory).Should().BeTrue();
        }

        [Fact]
        public void NullOrEmpty_False()
        {
            Throw.Is.NullOrEmpty(TestItem.Value.Collection).Should().BeFalse();
            Throw.Is.NullOrEmpty(TestItem.Value.CollectionGeneric).Should().BeFalse();
            Throw.Is.NullOrEmpty(TestItem.Value.List).Should().BeFalse();
            Throw.Is.NullOrEmpty(TestItem.Value.ListGeneric).Should().BeFalse();
            Throw.Is.NullOrEmpty(TestItem.Value.String).Should().BeFalse();
            Throw.Is.NullOrEmpty(TestItem.Value.Builder).Should().BeFalse();
            Throw.Is.NullOrEmpty(TestItem.Value.Secure).Should().BeFalse();
        }

        [Fact]
        public void NullOrEmpty_True()
        {
            Throw.Is.NullOrEmpty(TestItem.Empty.Collection).Should().BeTrue();
            Throw.Is.NullOrEmpty(TestItem.Empty.CollectionGeneric).Should().BeTrue();
            Throw.Is.NullOrEmpty(TestItem.Empty.List).Should().BeTrue();
            Throw.Is.NullOrEmpty(TestItem.Empty.ListGeneric).Should().BeTrue();
            Throw.Is.NullOrEmpty(TestItem.Empty.String).Should().BeTrue();
            Throw.Is.NullOrEmpty(TestItem.Empty.Builder).Should().BeTrue();
            Throw.Is.NullOrEmpty(TestItem.Empty.Secure).Should().BeTrue();

            Throw.Is.NullOrEmpty(TestItem.Null.Collection).Should().BeTrue();
            Throw.Is.NullOrEmpty(TestItem.Null.CollectionGeneric).Should().BeTrue();
            Throw.Is.NullOrEmpty(TestItem.Null.List).Should().BeTrue();
            Throw.Is.NullOrEmpty(TestItem.Null.ListGeneric).Should().BeTrue();
            Throw.Is.NullOrEmpty(TestItem.Null.String).Should().BeTrue();
            Throw.Is.NullOrEmpty(TestItem.Null.Builder).Should().BeTrue();
            Throw.Is.NullOrEmpty(TestItem.Null.Secure).Should().BeTrue();
        }

        [Fact]
        public void DefaultOrEmpty_False()
        {
            Throw.Is.DefaultOrEmpty(TestItem.Value.Span).Should().BeFalse();
            Throw.Is.DefaultOrEmpty(TestItem.Value.ReadOnlySpan).Should().BeFalse();
            Throw.Is.DefaultOrEmpty(TestItem.Value.Memory).Should().BeFalse();
            Throw.Is.DefaultOrEmpty(TestItem.Value.ReadOnlyMemory).Should().BeFalse();
        }

        [Fact]
        public void DefaultOrEmpty_True()
        {
            Throw.Is.DefaultOrEmpty(default(Span<char>)).Should().BeTrue();
            Throw.Is.DefaultOrEmpty(default(Span<char>)).Should().BeTrue();
            Throw.Is.DefaultOrEmpty(default(Memory<char>)).Should().BeTrue();
            Throw.Is.DefaultOrEmpty(default(ReadOnlyMemory<char>)).Should().BeTrue();

            Throw.Is.DefaultOrEmpty(TestItem.Empty.Span).Should().BeTrue();
            Throw.Is.DefaultOrEmpty(TestItem.Empty.ReadOnlySpan).Should().BeTrue();
            Throw.Is.DefaultOrEmpty(TestItem.Empty.Memory).Should().BeTrue();
            Throw.Is.DefaultOrEmpty(TestItem.Empty.ReadOnlyMemory).Should().BeTrue();

            Throw.Is.DefaultOrEmpty(TestItem.Null.Span).Should().BeTrue();
            Throw.Is.DefaultOrEmpty(TestItem.Null.ReadOnlySpan).Should().BeTrue();
            Throw.Is.DefaultOrEmpty(TestItem.Null.Memory).Should().BeTrue();
            Throw.Is.DefaultOrEmpty(TestItem.Null.ReadOnlyMemory).Should().BeTrue();
        }

        [Fact]
        public void ReadOnly()
        {
            Throw.Is.ReadOnly(TestItem.Null.Secure).Should().BeFalse();
            Throw.Is.ReadOnly(TestItem.Null.List).Should().BeFalse();
            Throw.Is.ReadOnly(TestItem.Null.CollectionGeneric).Should().BeFalse();

            Throw.Is.ReadOnly(TestItem.SecureReadOnly).Should().BeTrue();
            Throw.Is.ReadOnly(TestItem.SecureNotReadOnly).Should().BeFalse();

            Throw.Is.ReadOnly(new List<char>()).Should().BeFalse();
            Throw.Is.ReadOnly(new List<char>().AsReadOnly()).Should().BeTrue();

            Throw.Is.ReadOnly((IList) new List<char>()).Should().BeFalse();
            Throw.Is.ReadOnly((IList) new List<char>().AsReadOnly()).Should().BeTrue();
        }

        [Fact]
        public void NotReadOnly()
        {
            Throw.Is.NotReadOnly(TestItem.Null.Secure).Should().BeFalse();
            Throw.Is.NotReadOnly(TestItem.Null.List).Should().BeFalse();
            Throw.Is.NotReadOnly(TestItem.Null.CollectionGeneric).Should().BeFalse();

            Throw.Is.NotReadOnly(TestItem.SecureReadOnly).Should().BeFalse();
            Throw.Is.NotReadOnly(TestItem.SecureNotReadOnly).Should().BeTrue();

            Throw.Is.NotReadOnly(new List<char>()).Should().BeTrue();
            Throw.Is.NotReadOnly(new List<char>().AsReadOnly()).Should().BeFalse();

            Throw.Is.NotReadOnly((IList) new List<char>()).Should().BeTrue();
            Throw.Is.NotReadOnly((IList) new List<char>().AsReadOnly()).Should().BeFalse();
        }

    }
}
