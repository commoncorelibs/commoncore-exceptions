﻿#region Copyright (c) 2021 Jay Jeckel
// Copyright (c) 2021 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using Xunit;

namespace CommonCore.Exceptions.Tests
{
    [ExcludeFromCodeCoverage]
    public class Test_ThrowExtensions_If_Arg_Index
    {
        [Theory]
        [MemberData(nameof(TestData.GetData_Arg_Index_Invalid), MemberType = typeof(TestData))]
        public void Throw_If_Arg_Index_Invalid(string? text, bool throws, int index, bool inclusiveLength)
        {
            TestItem data = new(text);

            foreach (Action act in TestData.GetAction_Throw_If_Arg_Index_Invalid(data, index, inclusiveLength,
                includeStructs: text is not null))
            {
                if (!throws) { act.Should().NotThrow(); }
                else { act.Should().ThrowExactly<ArgumentIndexOutOfRangeException>(); }
            }
        }
    }
}
