﻿#region Copyright (c) 2021 Jay Jeckel
// Copyright (c) 2021 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using Xunit;

namespace CommonCore.Exceptions.Tests
{
    [ExcludeFromCodeCoverage]
    public class Test_ThrowExtensions_If
    {
        [Fact]
        public void NotThrow()
        {
            Action act;

            act = delegate { Throw.If.SelfNull(new object()); };
            act.Should().NotThrow();

            act = delegate { Throw.If.Arg.Default("arg", 1); };
            act.Should().NotThrow();

            act = delegate { Throw.If.Arg.DefaultOrEmpty("arg", TestItem.Value.Span); };
            act.Should().NotThrow();

            act = delegate { Throw.If.Arg.DefaultOrEmpty("arg", TestItem.Value.ReadOnlySpan); };
            act.Should().NotThrow();

            act = delegate { Throw.If.Arg.DefaultOrEmpty("arg", TestItem.Value.Memory); };
            act.Should().NotThrow();

            act = delegate { Throw.If.Arg.DefaultOrEmpty("arg", TestItem.Value.ReadOnlyMemory); };
            act.Should().NotThrow();

            act = delegate { Throw.If.Arg.Empty("arg", TestItem.Value.Span); };
            act.Should().NotThrow();

            act = delegate { Throw.If.Arg.Empty("arg", TestItem.Value.ReadOnlySpan); };
            act.Should().NotThrow();

            act = delegate { Throw.If.Arg.Empty("arg", TestItem.Value.Memory); };
            act.Should().NotThrow();

            act = delegate { Throw.If.Arg.Empty("arg", TestItem.Value.ReadOnlyMemory); };
            act.Should().NotThrow();

            act = delegate { Throw.If.Arg.Empty("arg", TestItem.Value.Collection); };
            act.Should().NotThrow();

            act = delegate { Throw.If.Arg.Empty("arg", TestItem.Value.CollectionGeneric); };
            act.Should().NotThrow();

            act = delegate { Throw.If.Arg.Empty("arg", TestItem.Value.List); };
            act.Should().NotThrow();

            act = delegate { Throw.If.Arg.Empty("arg", TestItem.Value.ListGeneric); };
            act.Should().NotThrow();

            act = delegate { Throw.If.Arg.Empty("arg", TestItem.Value.String); };
            act.Should().NotThrow();

            act = delegate { Throw.If.Arg.Empty("arg", TestItem.Value.Builder); };
            act.Should().NotThrow();

            act = delegate { Throw.If.Arg.Empty("arg", TestItem.Value.Secure); };
            act.Should().NotThrow();

            act = delegate { Throw.If.Arg.Empty("arg", TestItem.Null.Collection); };
            act.Should().NotThrow();

            act = delegate { Throw.If.Arg.Empty("arg", TestItem.Null.CollectionGeneric); };
            act.Should().NotThrow();

            act = delegate { Throw.If.Arg.Empty("arg", TestItem.Null.List); };
            act.Should().NotThrow();

            act = delegate { Throw.If.Arg.Empty("arg", TestItem.Null.ListGeneric); };
            act.Should().NotThrow();

            act = delegate { Throw.If.Arg.Empty("arg", TestItem.Null.String); };
            act.Should().NotThrow();

            act = delegate { Throw.If.Arg.Empty("arg", TestItem.Null.Builder); };
            act.Should().NotThrow();

            act = delegate { Throw.If.Arg.Empty("arg", TestItem.Null.Secure); };
            act.Should().NotThrow();

            act = delegate { Throw.If.Arg.Null("arg", new object()); };
            act.Should().NotThrow();

            act = delegate { Throw.If.Arg.NullOrEmpty("arg", TestItem.Value.Collection); };
            act.Should().NotThrow();

            act = delegate { Throw.If.Arg.NullOrEmpty("arg", TestItem.Value.CollectionGeneric); };
            act.Should().NotThrow();

            act = delegate { Throw.If.Arg.NullOrEmpty("arg", TestItem.Value.List); };
            act.Should().NotThrow();

            act = delegate { Throw.If.Arg.NullOrEmpty("arg", TestItem.Value.ListGeneric); };
            act.Should().NotThrow();

            act = delegate { Throw.If.Arg.NullOrEmpty("arg", TestItem.Value.String); };
            act.Should().NotThrow();

            act = delegate { Throw.If.Arg.NullOrEmpty("arg", TestItem.Value.Builder); };
            act.Should().NotThrow();

            act = delegate { Throw.If.Arg.NullOrEmpty("arg", TestItem.Value.Secure); };
            act.Should().NotThrow();

            act = delegate { Throw.If.Arg.ReadOnly("arg", TestItem.SecureNotReadOnly); };
            act.Should().NotThrow();

            act = delegate { Throw.If.Arg.NotReadOnly("arg", TestItem.SecureReadOnly); };
            act.Should().NotThrow();
        }

        [Fact]
        public void ThrowExactly()
        {
            Action act;

            act = delegate { Throw.If.SelfNull(default(object)); };
            act.Should().ThrowExactly<NullReferenceException>();

            act = delegate { Throw.If.Arg.Default("arg", default(int)); };
            act.Should().ThrowExactly<ArgumentDefaultException>();

            act = delegate { Throw.If.Arg.DefaultOrEmpty("arg", default(Span<char>)); };
            act.Should().ThrowExactly<ArgumentDefaultOrEmptyException>();

            act = delegate { Throw.If.Arg.DefaultOrEmpty("arg", default(ReadOnlySpan<char>)); };
            act.Should().ThrowExactly<ArgumentDefaultOrEmptyException>();

            act = delegate { Throw.If.Arg.DefaultOrEmpty("arg", default(Memory<char>)); };
            act.Should().ThrowExactly<ArgumentDefaultOrEmptyException>();

            act = delegate { Throw.If.Arg.DefaultOrEmpty("arg", default(ReadOnlyMemory<char>)); };
            act.Should().ThrowExactly<ArgumentDefaultOrEmptyException>();

            act = delegate { Throw.If.Arg.DefaultOrEmpty("arg", TestItem.Empty.Span); };
            act.Should().ThrowExactly<ArgumentDefaultOrEmptyException>();

            act = delegate { Throw.If.Arg.DefaultOrEmpty("arg", TestItem.Empty.ReadOnlySpan); };
            act.Should().ThrowExactly<ArgumentDefaultOrEmptyException>();

            act = delegate { Throw.If.Arg.DefaultOrEmpty("arg", TestItem.Empty.Memory); };
            act.Should().ThrowExactly<ArgumentDefaultOrEmptyException>();

            act = delegate { Throw.If.Arg.DefaultOrEmpty("arg", TestItem.Empty.ReadOnlyMemory); };
            act.Should().ThrowExactly<ArgumentDefaultOrEmptyException>();

            act = delegate { Throw.If.Arg.Empty("arg", default(Span<char>)); };
            act.Should().ThrowExactly<ArgumentEmptyException>();

            act = delegate { Throw.If.Arg.Empty("arg", default(ReadOnlySpan<char>)); };
            act.Should().ThrowExactly<ArgumentEmptyException>();

            act = delegate { Throw.If.Arg.Empty("arg", default(Memory<char>)); };
            act.Should().ThrowExactly<ArgumentEmptyException>();

            act = delegate { Throw.If.Arg.Empty("arg", default(ReadOnlyMemory<char>)); };
            act.Should().ThrowExactly<ArgumentEmptyException>();

            act = delegate { Throw.If.Arg.Empty("arg", TestItem.Null.Span); };
            act.Should().ThrowExactly<ArgumentEmptyException>();

            act = delegate { Throw.If.Arg.Empty("arg", TestItem.Null.ReadOnlySpan); };
            act.Should().ThrowExactly<ArgumentEmptyException>();

            act = delegate { Throw.If.Arg.Empty("arg", TestItem.Null.Memory); };
            act.Should().ThrowExactly<ArgumentEmptyException>();

            act = delegate { Throw.If.Arg.Empty("arg", TestItem.Null.ReadOnlyMemory); };
            act.Should().ThrowExactly<ArgumentEmptyException>();

            act = delegate { Throw.If.Arg.Empty("arg", TestItem.Empty.Span); };
            act.Should().ThrowExactly<ArgumentEmptyException>();

            act = delegate { Throw.If.Arg.Empty("arg", TestItem.Empty.ReadOnlySpan); };
            act.Should().ThrowExactly<ArgumentEmptyException>();

            act = delegate { Throw.If.Arg.Empty("arg", TestItem.Empty.Memory); };
            act.Should().ThrowExactly<ArgumentEmptyException>();

            act = delegate { Throw.If.Arg.Empty("arg", TestItem.Empty.ReadOnlyMemory); };
            act.Should().ThrowExactly<ArgumentEmptyException>();

            act = delegate { Throw.If.Arg.Empty("arg", TestItem.Empty.Collection); };
            act.Should().ThrowExactly<ArgumentEmptyException>();

            act = delegate { Throw.If.Arg.Empty("arg", TestItem.Empty.CollectionGeneric); };
            act.Should().ThrowExactly<ArgumentEmptyException>();

            act = delegate { Throw.If.Arg.Empty("arg", TestItem.Empty.List); };
            act.Should().ThrowExactly<ArgumentEmptyException>();

            act = delegate { Throw.If.Arg.Empty("arg", TestItem.Empty.ListGeneric); };
            act.Should().ThrowExactly<ArgumentEmptyException>();

            act = delegate { Throw.If.Arg.Empty("arg", TestItem.Empty.String); };
            act.Should().ThrowExactly<ArgumentEmptyException>();

            act = delegate { Throw.If.Arg.Empty("arg", TestItem.Empty.Builder); };
            act.Should().ThrowExactly<ArgumentEmptyException>();

            act = delegate { Throw.If.Arg.Empty("arg", TestItem.Empty.Secure); };
            act.Should().ThrowExactly<ArgumentEmptyException>();

            act = delegate { Throw.If.Arg.Null("arg", default(object)); };
            act.Should().ThrowExactly<ArgumentNullException>();

            act = delegate { Throw.If.Arg.NullOrEmpty("arg", TestItem.Null.Collection); };
            act.Should().ThrowExactly<ArgumentNullOrEmptyException>();

            act = delegate { Throw.If.Arg.NullOrEmpty("arg", TestItem.Null.CollectionGeneric); };
            act.Should().ThrowExactly<ArgumentNullOrEmptyException>();

            act = delegate { Throw.If.Arg.NullOrEmpty("arg", TestItem.Null.List); };
            act.Should().ThrowExactly<ArgumentNullOrEmptyException>();

            act = delegate { Throw.If.Arg.NullOrEmpty("arg", TestItem.Null.ListGeneric); };
            act.Should().ThrowExactly<ArgumentNullOrEmptyException>();

            act = delegate { Throw.If.Arg.NullOrEmpty("arg", TestItem.Null.String); };
            act.Should().ThrowExactly<ArgumentNullOrEmptyException>();

            act = delegate { Throw.If.Arg.NullOrEmpty("arg", TestItem.Null.Builder); };
            act.Should().ThrowExactly<ArgumentNullOrEmptyException>();

            act = delegate { Throw.If.Arg.NullOrEmpty("arg", TestItem.Null.Secure); };
            act.Should().ThrowExactly<ArgumentNullOrEmptyException>();

            act = delegate { Throw.If.Arg.NullOrEmpty("arg", TestItem.Empty.Collection); };
            act.Should().ThrowExactly<ArgumentNullOrEmptyException>();

            act = delegate { Throw.If.Arg.NullOrEmpty("arg", TestItem.Empty.CollectionGeneric); };
            act.Should().ThrowExactly<ArgumentNullOrEmptyException>();

            act = delegate { Throw.If.Arg.NullOrEmpty("arg", TestItem.Empty.List); };
            act.Should().ThrowExactly<ArgumentNullOrEmptyException>();

            act = delegate { Throw.If.Arg.NullOrEmpty("arg", TestItem.Empty.ListGeneric); };
            act.Should().ThrowExactly<ArgumentNullOrEmptyException>();

            act = delegate { Throw.If.Arg.NullOrEmpty("arg", TestItem.Empty.String); };
            act.Should().ThrowExactly<ArgumentNullOrEmptyException>();

            act = delegate { Throw.If.Arg.NullOrEmpty("arg", TestItem.Empty.Builder); };
            act.Should().ThrowExactly<ArgumentNullOrEmptyException>();

            act = delegate { Throw.If.Arg.NullOrEmpty("arg", TestItem.Empty.Secure); };
            act.Should().ThrowExactly<ArgumentNullOrEmptyException>();

            act = delegate { Throw.If.Arg.ReadOnly("arg", TestItem.SecureReadOnly); };
            act.Should().ThrowExactly<ArgumentReadOnlyException>();

            act = delegate { Throw.If.Arg.NotReadOnly("arg", TestItem.SecureNotReadOnly); };
            act.Should().ThrowExactly<ArgumentReadOnlyException>();
        }
    }
}
