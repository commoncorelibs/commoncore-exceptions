﻿#region Copyright (c) 2021 Jay Jeckel
// Copyright (c) 2021 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Security;
using System.Text;

namespace CommonCore.Exceptions.Tests
{
    [ExcludeFromCodeCoverage]
    public class TestItem : IDisposable
    {
        public const string TextValue = "test";
        public const string TextEmpty = "";
        public const string? TextNull = null;

        public static TestItem Value { get; } = new(TextValue);

        public static TestItem Empty { get; } = new("");

        public static TestItem Null { get; } = new(null);

        static TestItem()
        {
            SecureNotReadOnly = new();
            SecureReadOnly = new();
            SecureReadOnly.MakeReadOnly();
        }

        public static SecureString SecureReadOnly { get; }

        public static SecureString SecureNotReadOnly { get; }

        public TestItem(string? data)
        {
            this.String = data;
            if (this.String is not null)
            {
                this.Array = this.String.ToCharArray();
                this.Builder = new(this.String);
                this.Secure = new();
                foreach (char c in this.String) { this.Secure.AppendChar(c); }
                this.Secure.MakeReadOnly();
            }
        }

        public string? String { get; }

        public char[]? Array { get; }

        public StringBuilder? Builder { get; }

        public SecureString? Secure { get; }

        public ICollection? Collection => this.Array;

        public ICollection<char>? CollectionGeneric => this.Array;

        public IList? List => this.Array;

        public IList<char>? ListGeneric => this.Array;

        public IEnumerable<char>? Enumerable => this.Array;

        public Span<char> Span => this.Array.AsSpan();

        public ReadOnlySpan<char> ReadOnlySpan => this.String.AsSpan();

        public Memory<char> Memory => this.Array.AsMemory();

        public ReadOnlyMemory<char> ReadOnlyMemory => this.String.AsMemory();

        public int Length => this.String?.Length ?? 0;

        public bool Disposed { get; private set; }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.Disposed)
            {
                if (disposing)
                {
                    this.Secure?.Dispose();
                }

                this.Disposed = true;
            }
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            this.Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
