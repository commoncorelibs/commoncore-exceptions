﻿#region Copyright (c) 2021 Jay Jeckel
// Copyright (c) 2021 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace CommonCore.Exceptions.Tests
{
    [ExcludeFromCodeCoverage]
    public static class TestData
    {

        public static IEnumerable<object?[]> GetData_Arg_Enum_Invalid()
        {
            bool yes = true;
            bool not = false;

            foreach (TestEnum value in Enum.GetValues<TestEnum>())
            { yield return new object[] { not, value }; }

            yield return new object[] { yes, TestEnum.NegOne - 1 };
            yield return new object[] { yes, TestEnum.Six - 1 };
            yield return new object[] { yes, TestEnum.Six + 1 };
            yield return new object[] { yes, TestEnum.Eight + 1 };
        }

        public static IEnumerable<object?[]> GetData_Arg_Index_Invalid()
        {
            bool yes = true;
            bool not = false;
            bool inclusive = true;
            bool exclusive = false;

            // Both throw
            int negone = -1;
            yield return new object[] { TestItem.TextValue, yes, negone, exclusive };
            yield return new object[] { TestItem.TextValue, yes, negone, inclusive };

            // Neither throw
            int zero = 0;
            yield return new object[] { TestItem.TextValue, not, zero, exclusive };
            yield return new object[] { TestItem.TextValue, not, zero, inclusive };

            // Exclusive throw
            int length = TestItem.Value.String!.Length;
            yield return new object[] { TestItem.TextValue, yes, length, exclusive };
            yield return new object[] { TestItem.TextValue, not, length, inclusive };

            // Neither throw
            int lengthMinusOne = length - 1;
            yield return new object[] { TestItem.TextValue, not, lengthMinusOne, exclusive };
            yield return new object[] { TestItem.TextValue, not, lengthMinusOne, inclusive };

            // Both throw
            int lengthPlusOne = length + 1;
            yield return new object[] { TestItem.TextValue, yes, lengthPlusOne, exclusive };
            yield return new object[] { TestItem.TextValue, yes, lengthPlusOne, inclusive };

            // None throw for null
            yield return new object?[] { TestItem.TextNull, not, negone, exclusive };
            yield return new object?[] { TestItem.TextNull, not, negone, inclusive };
            yield return new object?[] { TestItem.TextNull, not, zero, exclusive };
            yield return new object?[] { TestItem.TextNull, not, zero, inclusive };
            yield return new object?[] { TestItem.TextNull, not, length, exclusive };
            yield return new object?[] { TestItem.TextNull, not, length, inclusive };
            yield return new object?[] { TestItem.TextNull, not, lengthMinusOne, exclusive };
            yield return new object?[] { TestItem.TextNull, not, lengthMinusOne, inclusive };
            yield return new object?[] { TestItem.TextNull, not, lengthPlusOne, exclusive };
            yield return new object?[] { TestItem.TextNull, not, lengthPlusOne, inclusive };
        }

        public static IEnumerable<Action> GetAction_Throw_If_Arg_Index_Invalid(TestItem data, int index, bool inclusiveLength,
            bool includeStructs = true, bool includeClasses = true)
        {
            if (includeStructs)
            {
                yield return delegate { Throw.If.Arg.Index.Invalid("arg", index, data.Length, inclusiveLength); };
                yield return delegate { Throw.If.Arg.Index.Invalid("arg", index, data.Span, inclusiveLength); };
                yield return delegate { Throw.If.Arg.Index.Invalid("arg", index, data.ReadOnlySpan, inclusiveLength); };
                yield return delegate { Throw.If.Arg.Index.Invalid("arg", index, data.Memory, inclusiveLength); };
                yield return delegate { Throw.If.Arg.Index.Invalid("arg", index, data.ReadOnlyMemory, inclusiveLength); };
            }

            if (includeClasses)
            {
                yield return delegate { Throw.If.Arg.Index.Invalid("arg", index, data.Collection, inclusiveLength); };
                yield return delegate { Throw.If.Arg.Index.Invalid("arg", index, data.CollectionGeneric, inclusiveLength); };
                yield return delegate { Throw.If.Arg.Index.Invalid("arg", index, data.List, inclusiveLength); };
                yield return delegate { Throw.If.Arg.Index.Invalid("arg", index, data.ListGeneric, inclusiveLength); };
                yield return delegate { Throw.If.Arg.Index.Invalid("arg", index, data.String, inclusiveLength); };
                yield return delegate { Throw.If.Arg.Index.Invalid("arg", index, data.Builder, inclusiveLength); };
                yield return delegate { Throw.If.Arg.Index.Invalid("arg", index, data.Secure, inclusiveLength); };
            }
        }
    }
}
