﻿#region Copyright (c) 2021 Jay Jeckel
// Copyright (c) 2021 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using Xunit;

namespace CommonCore.Exceptions.Tests
{
    [ExcludeFromCodeCoverage]
    public class Test_ThrowExtensions_From
    {
        [Fact]
        public void ArgumentSubstitution()
        {
            const string paramname = "TestArgName";
            const string expected = "Argument 'TestArgName' ";

            Throw.From.Arg.Default(paramname).Should().StartWith(expected);
            Throw.From.Arg.DefaultOrEmpty(paramname).Should().StartWith(expected);
            Throw.From.Arg.Empty(paramname).Should().StartWith(expected);
            Throw.From.Arg.Null(paramname).Should().StartWith(expected);
            Throw.From.Arg.NullOrEmpty(paramname).Should().StartWith(expected);
            Throw.From.Arg.ReadOnly(paramname).Should().StartWith(expected);
            Throw.From.Arg.NotReadOnly(paramname).Should().StartWith(expected);

            Throw.From.Arg.Enum.Invalid(paramname, TestEnum.NegOne, TestEnum.Zero, TestEnum.Four)
                .Should().StartWith(expected)
                .And.Contain($"'{nameof(TestEnum)}'")
                .And.Contain($"'{nameof(TestEnum.NegOne)}'")
                .And.Contain($"'{nameof(TestEnum.Zero)}'")
                .And.Contain($"'{nameof(TestEnum.Four)}'");

            Throw.From.Arg.Index.Invalid(paramname, -1, 4)
                .Should().StartWith(expected)
                .And.Contain($"'{-1}'")
                .And.Contain($"'{4}'");

            Throw.From.Arg.Index.InvalidInclusive(paramname, -1, 4)
                .Should().StartWith(expected)
                .And.Contain($"'{-1}'")
                .And.Contain($"'{4}'");

            Throw.From.Arg.Index.Valid(paramname, 0, 4)
                .Should().StartWith(expected)
                .And.Contain($"'{0}'")
                .And.Contain($"'{4}'");

            Throw.From.Arg.Index.ValidInclusive(paramname, 0, 4)
                .Should().StartWith(expected)
                .And.Contain($"'{0}'")
                .And.Contain($"'{4}'");

            Throw.From.Arg.Range.Less(paramname, -1, 0)
                .Should().StartWith(expected)
                .And.Contain($"'{-1}'")
                .And.Contain($"'{0}'");

            Throw.From.Arg.Range.LessOrEqual(paramname, -1, 0)
                .Should().StartWith(expected)
                .And.Contain($"'{-1}'")
                .And.Contain($"'{0}'");

            Throw.From.Arg.Range.More(paramname, 1, 0)
                .Should().StartWith(expected)
                .And.Contain($"'{1}'")
                .And.Contain($"'{0}'");

            Throw.From.Arg.Range.MoreOrEqual(paramname, 1, 0)
                .Should().StartWith(expected)
                .And.Contain($"'{1}'")
                .And.Contain($"'{0}'");

            Throw.From.Arg.Range.OutsideInclusive(paramname, -1, 0, 4)
                .Should().StartWith(expected)
                .And.Contain($"'{-1}'")
                .And.Contain($"'{0}'")
                .And.Contain($"'{4}'");

            Throw.From.Arg.Range.OutsideExclusive(paramname, -1, 0, 4)
                .Should().StartWith(expected)
                .And.Contain($"'{-1}'")
                .And.Contain($"'{0}'")
                .And.Contain($"'{4}'");

            Throw.From.Arg.Range.OutsideExclusiveInclusive(paramname, -1, 0, 4)
                .Should().StartWith(expected)
                .And.Contain($"'{-1}'")
                .And.Contain($"'{0}'")
                .And.Contain($"'{4}'");

            Throw.From.Arg.Range.OutsideInclusiveExclusive(paramname, -1, 0, 4)
                .Should().StartWith(expected)
                .And.Contain($"'{-1}'")
                .And.Contain($"'{0}'")
                .And.Contain($"'{4}'");
        }
    }
}
