﻿#region Copyright (c) 2021 Jay Jeckel
// Copyright (c) 2021 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using Xunit;

namespace CommonCore.Exceptions.Tests
{
    [ExcludeFromCodeCoverage]
    public class Test_ThrowExtensions_Try_Arg_Index
    {
        [Theory]
        [MemberData(nameof(TestData.GetData_Arg_Index_Invalid), MemberType = typeof(TestData))]
        public void Throw_Try_Arg_Index_Invalid(string? text, bool throws, int index, bool inclusiveLength)
        {
            TestItem data = new(text);

            if (text is not null)
            {
                {
                    Throw.Try.Arg.Index.Invalid("arg", index, data.Length, out var exception, inclusiveLength).Should().Be(throws);
                    if (!throws) { exception.Should().BeNull(); }
                    else { exception.Should().NotBeNull().And.BeOfType<ArgumentIndexOutOfRangeException>(); }
                }
                {
                    Throw.Try.Arg.Index.Invalid("arg", index, data.Span, out var exception, inclusiveLength).Should().Be(throws);
                    if (!throws) { exception.Should().BeNull(); }
                    else { exception.Should().NotBeNull().And.BeOfType<ArgumentIndexOutOfRangeException>(); }
                }
                {
                    Throw.Try.Arg.Index.Invalid("arg", index, data.ReadOnlySpan, out var exception, inclusiveLength).Should().Be(throws);
                    if (!throws) { exception.Should().BeNull(); }
                    else { exception.Should().NotBeNull().And.BeOfType<ArgumentIndexOutOfRangeException>(); }
                }
                {
                    Throw.Try.Arg.Index.Invalid("arg", index, data.Memory, out var exception, inclusiveLength).Should().Be(throws);
                    if (!throws) { exception.Should().BeNull(); }
                    else { exception.Should().NotBeNull().And.BeOfType<ArgumentIndexOutOfRangeException>(); }
                }
                {
                    Throw.Try.Arg.Index.Invalid("arg", index, data.ReadOnlyMemory, out var exception, inclusiveLength).Should().Be(throws);
                    if (!throws) { exception.Should().BeNull(); }
                    else { exception.Should().NotBeNull().And.BeOfType<ArgumentIndexOutOfRangeException>(); }
                }
            }
            {
                Throw.Try.Arg.Index.Invalid("arg", index, data.Collection, out var exception, inclusiveLength).Should().Be(throws);
                if (!throws) { exception.Should().BeNull(); }
                else { exception.Should().NotBeNull().And.BeOfType<ArgumentIndexOutOfRangeException>(); }
            }
            {
                Throw.Try.Arg.Index.Invalid("arg", index, data.CollectionGeneric, out var exception, inclusiveLength).Should().Be(throws);
                if (!throws) { exception.Should().BeNull(); }
                else { exception.Should().NotBeNull().And.BeOfType<ArgumentIndexOutOfRangeException>(); }
            }
            {
                Throw.Try.Arg.Index.Invalid("arg", index, data.List, out var exception, inclusiveLength).Should().Be(throws);
                if (!throws) { exception.Should().BeNull(); }
                else { exception.Should().NotBeNull().And.BeOfType<ArgumentIndexOutOfRangeException>(); }
            }
            {
                Throw.Try.Arg.Index.Invalid("arg", index, data.ListGeneric, out var exception, inclusiveLength).Should().Be(throws);
                if (!throws) { exception.Should().BeNull(); }
                else { exception.Should().NotBeNull().And.BeOfType<ArgumentIndexOutOfRangeException>(); }
            }
            {
                Throw.Try.Arg.Index.Invalid("arg", index, data.String, out var exception, inclusiveLength).Should().Be(throws);
                if (!throws) { exception.Should().BeNull(); }
                else { exception.Should().NotBeNull().And.BeOfType<ArgumentIndexOutOfRangeException>(); }
            }
            {
                Throw.Try.Arg.Index.Invalid("arg", index, data.Builder, out var exception, inclusiveLength).Should().Be(throws);
                if (!throws) { exception.Should().BeNull(); }
                else { exception.Should().NotBeNull().And.BeOfType<ArgumentIndexOutOfRangeException>(); }
            }
            {
                Throw.Try.Arg.Index.Invalid("arg", index, data.Secure, out var exception, inclusiveLength).Should().Be(throws);
                if (!throws) { exception.Should().BeNull(); }
                else { exception.Should().NotBeNull().And.BeOfType<ArgumentIndexOutOfRangeException>(); }
            }
        }
    }
}
