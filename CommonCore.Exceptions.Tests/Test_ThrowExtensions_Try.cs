﻿#region Copyright (c) 2021 Jay Jeckel
// Copyright (c) 2021 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using Xunit;

namespace CommonCore.Exceptions.Tests
{
    [ExcludeFromCodeCoverage]
    public class Test_ThrowExtensions_Try
    {
        [Fact]
        public void False()
        {
            bool result;

            {
                result = Throw.Try.SelfNull(new object(), out var exception);
                result.Should().BeFalse();
                exception.Should().BeNull();
            }
            {
                result = Throw.Try.Arg.Default("arg", 1, out var exception);
                result.Should().BeFalse();
                exception.Should().BeNull();
            }
            {
                result = Throw.Try.Arg.DefaultOrEmpty("arg", TestItem.Value.Span, out var exception);
                result.Should().BeFalse();
                exception.Should().BeNull();
            }
            {
                result = Throw.Try.Arg.DefaultOrEmpty("arg", TestItem.Value.ReadOnlySpan, out var exception);
                result.Should().BeFalse();
                exception.Should().BeNull();
            }
            {
                result = Throw.Try.Arg.DefaultOrEmpty("arg", TestItem.Value.Memory, out var exception);
                result.Should().BeFalse();
                exception.Should().BeNull();
            }
            {
                result = Throw.Try.Arg.DefaultOrEmpty("arg", TestItem.Value.ReadOnlyMemory, out var exception);
                result.Should().BeFalse();
                exception.Should().BeNull();
            }
            {
                result = Throw.Try.Arg.Empty("arg", TestItem.Value.Span, out var exception);
                result.Should().BeFalse();
                exception.Should().BeNull();
            }
            {
                result = Throw.Try.Arg.Empty("arg", TestItem.Value.ReadOnlySpan, out var exception);
                result.Should().BeFalse();
                exception.Should().BeNull();
            }
            {
                result = Throw.Try.Arg.Empty("arg", TestItem.Value.Memory, out var exception);
                result.Should().BeFalse();
                exception.Should().BeNull();
            }
            {
                result = Throw.Try.Arg.Empty("arg", TestItem.Value.ReadOnlyMemory, out var exception);
                result.Should().BeFalse();
                exception.Should().BeNull();
            }
            {
                result = Throw.Try.Arg.Empty("arg", TestItem.Value.Collection, out var exception);
                result.Should().BeFalse();
                exception.Should().BeNull();
            }
            {
                result = Throw.Try.Arg.Empty("arg", TestItem.Value.CollectionGeneric, out var exception);
                result.Should().BeFalse();
                exception.Should().BeNull();
            }
            {
                result = Throw.Try.Arg.Empty("arg", TestItem.Value.List, out var exception);
                result.Should().BeFalse();
                exception.Should().BeNull();
            }
            {
                result = Throw.Try.Arg.Empty("arg", TestItem.Value.ListGeneric, out var exception);
                result.Should().BeFalse();
                exception.Should().BeNull();
            }
            {
                result = Throw.Try.Arg.Empty("arg", TestItem.Value.String, out var exception);
                result.Should().BeFalse();
                exception.Should().BeNull();
            }
            {
                result = Throw.Try.Arg.Empty("arg", TestItem.Value.Builder, out var exception);
                result.Should().BeFalse();
                exception.Should().BeNull();
            }
            {
                result = Throw.Try.Arg.Empty("arg", TestItem.Value.Secure, out var exception);
                result.Should().BeFalse();
                exception.Should().BeNull();
            }
            {
                result = Throw.Try.Arg.Empty("arg", TestItem.Null.Collection, out var exception);
                result.Should().BeFalse();
                exception.Should().BeNull();
            }
            {
                result = Throw.Try.Arg.Empty("arg", TestItem.Null.CollectionGeneric, out var exception);
                result.Should().BeFalse();
                exception.Should().BeNull();
            }
            {
                result = Throw.Try.Arg.Empty("arg", TestItem.Null.List, out var exception);
                result.Should().BeFalse();
                exception.Should().BeNull();
            }
            {
                result = Throw.Try.Arg.Empty("arg", TestItem.Null.ListGeneric, out var exception);
                result.Should().BeFalse();
                exception.Should().BeNull();
            }
            {
                result = Throw.Try.Arg.Empty("arg", TestItem.Null.String, out var exception);
                result.Should().BeFalse();
                exception.Should().BeNull();
            }
            {
                result = Throw.Try.Arg.Empty("arg", TestItem.Null.Builder, out var exception);
                result.Should().BeFalse();
                exception.Should().BeNull();
            }
            {
                result = Throw.Try.Arg.Empty("arg", TestItem.Null.Secure, out var exception);
                result.Should().BeFalse();
                exception.Should().BeNull();
            }
            {
                result = Throw.Try.Arg.Null("arg", new object(), out var exception);
                result.Should().BeFalse();
                exception.Should().BeNull();
            }
            {
                result = Throw.Try.Arg.NullOrEmpty("arg", TestItem.Value.Collection, out var exception);
                result.Should().BeFalse();
                exception.Should().BeNull();
            }
            {
                result = Throw.Try.Arg.NullOrEmpty("arg", TestItem.Value.CollectionGeneric, out var exception);
                result.Should().BeFalse();
                exception.Should().BeNull();
            }
            {
                result = Throw.Try.Arg.NullOrEmpty("arg", TestItem.Value.List, out var exception);
                result.Should().BeFalse();
                exception.Should().BeNull();
            }
            {
                result = Throw.Try.Arg.NullOrEmpty("arg", TestItem.Value.ListGeneric, out var exception);
                result.Should().BeFalse();
                exception.Should().BeNull();
            }
            {
                result = Throw.Try.Arg.NullOrEmpty("arg", TestItem.Value.String, out var exception);
                result.Should().BeFalse();
                exception.Should().BeNull();
            }
            {
                result = Throw.Try.Arg.NullOrEmpty("arg", TestItem.Value.Builder, out var exception);
                result.Should().BeFalse();
                exception.Should().BeNull();
            }
            {
                result = Throw.Try.Arg.NullOrEmpty("arg", TestItem.Value.Secure, out var exception);
                result.Should().BeFalse();
                exception.Should().BeNull();
            }
            {
                result = Throw.Try.Arg.ReadOnly("arg", TestItem.SecureNotReadOnly, out var exception);
                result.Should().BeFalse();
                exception.Should().BeNull();
            }
            {
                result = Throw.Try.Arg.NotReadOnly("arg", TestItem.SecureReadOnly, out var exception);
                result.Should().BeFalse();
                exception.Should().BeNull();
            }
        }

        [Fact]
        public void True()
        {
            bool result;

            {
                result = Throw.Try.SelfNull(default(object), out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<NullReferenceException>();
            }
            {
                result = Throw.Try.Arg.Default("arg", default(int), out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentDefaultException>();
            }
            {
                result = Throw.Try.Arg.DefaultOrEmpty("arg", default(Span<char>), out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentDefaultOrEmptyException>();
            }
            {
                result = Throw.Try.Arg.DefaultOrEmpty("arg", default(ReadOnlySpan<char>), out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentDefaultOrEmptyException>();
            }
            {
                result = Throw.Try.Arg.DefaultOrEmpty("arg", default(Memory<char>), out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentDefaultOrEmptyException>();
            }
            {
                result = Throw.Try.Arg.DefaultOrEmpty("arg", default(ReadOnlyMemory<char>), out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentDefaultOrEmptyException>();
            }
            {
                result = Throw.Try.Arg.DefaultOrEmpty("arg", TestItem.Empty.Span, out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentDefaultOrEmptyException>();
            }
            {
                result = Throw.Try.Arg.DefaultOrEmpty("arg", TestItem.Empty.ReadOnlySpan, out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentDefaultOrEmptyException>();
            }
            {
                result = Throw.Try.Arg.DefaultOrEmpty("arg", TestItem.Empty.Memory, out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentDefaultOrEmptyException>();
            }
            {
                result = Throw.Try.Arg.DefaultOrEmpty("arg", TestItem.Empty.ReadOnlyMemory, out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentDefaultOrEmptyException>();
            }
            {
                result = Throw.Try.Arg.Empty("arg", default(Span<char>), out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentEmptyException>();
            }
            {
                result = Throw.Try.Arg.Empty("arg", default(ReadOnlySpan<char>), out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentEmptyException>();
            }
            {
                result = Throw.Try.Arg.Empty("arg", default(Memory<char>), out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentEmptyException>();
            }
            {
                result = Throw.Try.Arg.Empty("arg", default(ReadOnlyMemory<char>), out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentEmptyException>();
            }
            {
                result = Throw.Try.Arg.Empty("arg", TestItem.Null.Span, out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentEmptyException>();
            }
            {
                result = Throw.Try.Arg.Empty("arg", TestItem.Null.ReadOnlySpan, out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentEmptyException>();
            }
            {
                result = Throw.Try.Arg.Empty("arg", TestItem.Null.Memory, out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentEmptyException>();
            }
            {
                result = Throw.Try.Arg.Empty("arg", TestItem.Null.ReadOnlyMemory, out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentEmptyException>();
            }
            {
                result = Throw.Try.Arg.Empty("arg", TestItem.Empty.Span, out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentEmptyException>();
            }
            {
                result = Throw.Try.Arg.Empty("arg", TestItem.Empty.ReadOnlySpan, out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentEmptyException>();
            }
            {
                result = Throw.Try.Arg.Empty("arg", TestItem.Empty.Memory, out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentEmptyException>();
            }
            {
                result = Throw.Try.Arg.Empty("arg", TestItem.Empty.ReadOnlyMemory, out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentEmptyException>();
            }
            {
                result = Throw.Try.Arg.Empty("arg", TestItem.Empty.Collection, out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentEmptyException>();
            }
            {
                result = Throw.Try.Arg.Empty("arg", TestItem.Empty.CollectionGeneric, out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentEmptyException>();
            }
            {
                result = Throw.Try.Arg.Empty("arg", TestItem.Empty.List, out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentEmptyException>();
            }
            {
                result = Throw.Try.Arg.Empty("arg", TestItem.Empty.ListGeneric, out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentEmptyException>();
            }
            {
                result = Throw.Try.Arg.Empty("arg", TestItem.Empty.String, out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentEmptyException>();
            }
            {
                result = Throw.Try.Arg.Empty("arg", TestItem.Empty.Builder, out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentEmptyException>();
            }
            {
                result = Throw.Try.Arg.Empty("arg", TestItem.Empty.Secure, out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentEmptyException>();
            }
            {
                result = Throw.Try.Arg.Null("arg", default(object), out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentNullException>();
            }
            {
                result = Throw.Try.Arg.NullOrEmpty("arg", TestItem.Null.Collection, out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentNullOrEmptyException>();
            }
            {
                result = Throw.Try.Arg.NullOrEmpty("arg", TestItem.Null.CollectionGeneric, out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentNullOrEmptyException>();
            }
            {
                result = Throw.Try.Arg.NullOrEmpty("arg", TestItem.Null.List, out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentNullOrEmptyException>();
            }
            {
                result = Throw.Try.Arg.NullOrEmpty("arg", TestItem.Null.ListGeneric, out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentNullOrEmptyException>();
            }
            {
                result = Throw.Try.Arg.NullOrEmpty("arg", TestItem.Null.String, out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentNullOrEmptyException>();
            }
            {
                result = Throw.Try.Arg.NullOrEmpty("arg", TestItem.Null.Builder, out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentNullOrEmptyException>();
            }
            {
                result = Throw.Try.Arg.NullOrEmpty("arg", TestItem.Null.Secure, out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentNullOrEmptyException>();
            }
            {
                result = Throw.Try.Arg.NullOrEmpty("arg", TestItem.Empty.Collection, out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentNullOrEmptyException>();
            }
            {
                result = Throw.Try.Arg.NullOrEmpty("arg", TestItem.Empty.CollectionGeneric, out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentNullOrEmptyException>();
            }
            {
                result = Throw.Try.Arg.NullOrEmpty("arg", TestItem.Empty.List, out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentNullOrEmptyException>();
            }
            {
                result = Throw.Try.Arg.NullOrEmpty("arg", TestItem.Empty.ListGeneric, out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentNullOrEmptyException>();
            }
            {
                result = Throw.Try.Arg.NullOrEmpty("arg", TestItem.Empty.String, out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentNullOrEmptyException>();
            }
            {
                result = Throw.Try.Arg.NullOrEmpty("arg", TestItem.Empty.Builder, out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentNullOrEmptyException>();
            }
            {
                result = Throw.Try.Arg.NullOrEmpty("arg", TestItem.Empty.Secure, out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentNullOrEmptyException>();
            }
            {
                result = Throw.Try.Arg.ReadOnly("arg", TestItem.SecureReadOnly, out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentReadOnlyException>();
            }
            {
                result = Throw.Try.Arg.NotReadOnly("arg", TestItem.SecureNotReadOnly, out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentReadOnlyException>();
            }
        }
    }
}
