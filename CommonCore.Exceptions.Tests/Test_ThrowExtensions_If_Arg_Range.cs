﻿#region Copyright (c) 2021 Jay Jeckel
// Copyright (c) 2021 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using Xunit;

namespace CommonCore.Exceptions.Tests
{
    [ExcludeFromCodeCoverage]
    public class Test_ThrowExtensions_If_Arg_Range
    {
        private const int MIN = -10;
        private const int MIN_MINUS_ONE = MIN - 1;
        private const int MIN_EQUAL = MIN;
        private const int MIN_PLUS_ONE = MIN + 1;
        private const int MAX = 10;
        private const int MAX_MINUS_ONE = MAX - 1;
        private const int MAX_EQUAL = MAX;
        private const int MAX_PLUS_ONE = MAX + 1;

        [Fact]
        public void Less()
        {
            Action act;

            act = delegate { Throw.If.Arg.Range.Less("arg", MIN_MINUS_ONE, MIN); };
            act.Should().ThrowExactly<ArgumentOutOfRangeException>();

            act = delegate { Throw.If.Arg.Range.Less("arg", MIN_EQUAL, MIN); };
            act.Should().NotThrow();

            act = delegate { Throw.If.Arg.Range.Less("arg", MIN_PLUS_ONE, MIN); };
            act.Should().NotThrow();
        }

        [Fact]
        public void LessOrEqual()
        {
            Action act;

            act = delegate { Throw.If.Arg.Range.LessOrEqual("arg", MIN_MINUS_ONE, MIN); };
            act.Should().ThrowExactly<ArgumentOutOfRangeException>();

            act = delegate { Throw.If.Arg.Range.LessOrEqual("arg", MIN_EQUAL, MIN); };
            act.Should().ThrowExactly<ArgumentOutOfRangeException>();

            act = delegate { Throw.If.Arg.Range.LessOrEqual("arg", MIN_PLUS_ONE, MIN); };
            act.Should().NotThrow();
        }

        [Fact]
        public void More()
        {
            Action act;

            act = delegate { Throw.If.Arg.Range.More("arg", MAX_MINUS_ONE, MAX); };
            act.Should().NotThrow();

            act = delegate { Throw.If.Arg.Range.More("arg", MAX_EQUAL, MAX); };
            act.Should().NotThrow();

            act = delegate { Throw.If.Arg.Range.More("arg", MAX_PLUS_ONE, MAX); };
            act.Should().ThrowExactly<ArgumentOutOfRangeException>();
        }

        [Fact]
        public void MoreOrEqual()
        {
            Action act;

            act = delegate { Throw.If.Arg.Range.MoreOrEqual("arg", MAX_MINUS_ONE, MAX); };
            act.Should().NotThrow();

            act = delegate { Throw.If.Arg.Range.MoreOrEqual("arg", MAX_EQUAL, MAX); };
            act.Should().ThrowExactly<ArgumentOutOfRangeException>();

            act = delegate { Throw.If.Arg.Range.MoreOrEqual("arg", MAX_PLUS_ONE, MAX); };
            act.Should().ThrowExactly<ArgumentOutOfRangeException>();
        }

        [Fact]
        public void Outside_Min_Inclusive()
        {
            Action act;

            act = delegate { Throw.If.Arg.Range.Outside("arg", MIN_MINUS_ONE, MIN, MAX, true, true); };
            act.Should().ThrowExactly<ArgumentOutOfRangeException>();

            act = delegate { Throw.If.Arg.Range.Outside("arg", MIN_EQUAL, MIN, MAX, true, true); };
            act.Should().NotThrow();

            act = delegate { Throw.If.Arg.Range.Outside("arg", MIN_PLUS_ONE, MIN, MAX, true, true); };
            act.Should().NotThrow();
        }

        [Fact]
        public void Outside_Min_Exclusive()
        {
            Action act;

            act = delegate { Throw.If.Arg.Range.Outside("arg", MIN_MINUS_ONE, MIN, MAX, false, true); };
            act.Should().ThrowExactly<ArgumentOutOfRangeException>();

            act = delegate { Throw.If.Arg.Range.Outside("arg", MIN_EQUAL, MIN, MAX, false, true); };
            act.Should().ThrowExactly<ArgumentOutOfRangeException>();

            act = delegate { Throw.If.Arg.Range.Outside("arg", MIN_PLUS_ONE, MIN, MAX, false, true); };
            act.Should().NotThrow();
        }

        [Fact]
        public void Outside_Max_Inclusive()
        {
            Action act;

            act = delegate { Throw.If.Arg.Range.Outside("arg", MAX_MINUS_ONE, MIN, MAX, true, true); };
            act.Should().NotThrow();

            act = delegate { Throw.If.Arg.Range.Outside("arg", MAX_EQUAL, MIN, MAX, true, true); };
            act.Should().NotThrow();

            act = delegate { Throw.If.Arg.Range.Outside("arg", MAX_PLUS_ONE, MIN, MAX, true, true); };
            act.Should().ThrowExactly<ArgumentOutOfRangeException>();
        }

        [Fact]
        public void Outside_Max_Exclusive()
        {
            Action act;

            act = delegate { Throw.If.Arg.Range.Outside("arg", MAX_MINUS_ONE, MIN, MAX, true, false); };
            act.Should().NotThrow();

            act = delegate { Throw.If.Arg.Range.Outside("arg", MAX_EQUAL, MIN, MAX, true, false); };
            act.Should().ThrowExactly<ArgumentOutOfRangeException>();

            act = delegate { Throw.If.Arg.Range.Outside("arg", MAX_PLUS_ONE, MIN, MAX, true, false); };
            act.Should().ThrowExactly<ArgumentOutOfRangeException>();
        }
    }
}
