﻿#region Copyright (c) 2021 Jay Jeckel
// Copyright (c) 2021 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using Xunit;

namespace CommonCore.Exceptions.Tests
{
    [ExcludeFromCodeCoverage]
    public class Test_ThrowExtensions_Try_Arg_Range
    {
        private const int MIN = -10;
        private const int MIN_MINUS_ONE = MIN - 1;
        private const int MIN_EQUAL = MIN;
        private const int MIN_PLUS_ONE = MIN + 1;
        private const int MAX = 10;
        private const int MAX_MINUS_ONE = MAX - 1;
        private const int MAX_EQUAL = MAX;
        private const int MAX_PLUS_ONE = MAX + 1;

        [Fact]
        public void Less()
        {
            bool result;

            {
                result = Throw.Try.Arg.Range.Less("arg", MIN_MINUS_ONE, MIN, out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentOutOfRangeException>();
            }
            {
                result = Throw.Try.Arg.Range.Less("arg", MIN_EQUAL, MIN, out var exception);
                result.Should().BeFalse();
                exception.Should().BeNull();
            }
            {
                result = Throw.Try.Arg.Range.Less("arg", MIN_PLUS_ONE, MIN, out var exception);
                result.Should().BeFalse();
                exception.Should().BeNull();
            }
        }

        [Fact]
        public void LessOrEqual()
        {
            bool result;

            {
                result = Throw.Try.Arg.Range.LessOrEqual("arg", MIN_MINUS_ONE, MIN, out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentOutOfRangeException>();
            }
            {
                result = Throw.Try.Arg.Range.LessOrEqual("arg", MIN_EQUAL, MIN, out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentOutOfRangeException>();
            }
            {
                result = Throw.Try.Arg.Range.LessOrEqual("arg", MIN_PLUS_ONE, MIN, out var exception);
                result.Should().BeFalse();
                exception.Should().BeNull();
            }
        }

        [Fact]
        public void More()
        {
            bool result;

            {
                result = Throw.Try.Arg.Range.More("arg", MAX_MINUS_ONE, MAX, out var exception);
                result.Should().BeFalse();
                exception.Should().BeNull();
            }
            {
                result = Throw.Try.Arg.Range.More("arg", MAX_EQUAL, MAX, out var exception);
                result.Should().BeFalse();
                exception.Should().BeNull();
            }
            {
                result = Throw.Try.Arg.Range.More("arg", MAX_PLUS_ONE, MAX, out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentOutOfRangeException>();
            }
        }

        [Fact]
        public void MoreOrEqual()
        {
            bool result;

            {
                result = Throw.Try.Arg.Range.MoreOrEqual("arg", MAX_MINUS_ONE, MAX, out var exception);
                result.Should().BeFalse();
                exception.Should().BeNull();
            }
            {
                result = Throw.Try.Arg.Range.MoreOrEqual("arg", MAX_EQUAL, MAX, out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentOutOfRangeException>();
            }
            {
                result = Throw.Try.Arg.Range.MoreOrEqual("arg", MAX_PLUS_ONE, MAX, out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentOutOfRangeException>();
            }
        }

        [Fact]
        public void Outside_Min_Inclusive()
        {
            bool result;

            {
                result = Throw.Try.Arg.Range.Outside("arg", MIN_MINUS_ONE, MIN, MAX, true, true, out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentOutOfRangeException>();
            }
            {
                result = Throw.Try.Arg.Range.Outside("arg", MIN_EQUAL, MIN, MAX, true, true, out var exception);
                result.Should().BeFalse();
                exception.Should().BeNull();
            }
            {
                result = Throw.Try.Arg.Range.Outside("arg", MIN_PLUS_ONE, MIN, MAX, true, true, out var exception);
                result.Should().BeFalse();
                exception.Should().BeNull();
            }
        }

        [Fact]
        public void Outside_Min_Exclusive()
        {
            bool result;

            {
                result = Throw.Try.Arg.Range.Outside("arg", MIN_MINUS_ONE, MIN, MAX, false, true, out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentOutOfRangeException>();
            }
            {
                result = Throw.Try.Arg.Range.Outside("arg", MIN_EQUAL, MIN, MAX, false, true, out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentOutOfRangeException>();
            }
            {
                result = Throw.Try.Arg.Range.Outside("arg", MIN_PLUS_ONE, MIN, MAX, false, true, out var exception);
                result.Should().BeFalse();
                exception.Should().BeNull();
            }
        }

        [Fact]
        public void Outside_Max_Inclusive()
        {
            bool result;

            {
                result = Throw.Try.Arg.Range.Outside("arg", MAX_MINUS_ONE, MIN, MAX, true, true, out var exception);
                result.Should().BeFalse();
                exception.Should().BeNull();
            }
            {
                result = Throw.Try.Arg.Range.Outside("arg", MAX_EQUAL, MIN, MAX, true, true, out var exception);
                result.Should().BeFalse();
                exception.Should().BeNull();
            }
            {
                result = Throw.Try.Arg.Range.Outside("arg", MAX_PLUS_ONE, MIN, MAX, true, true, out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentOutOfRangeException>();
            }
        }

        [Fact]
        public void Outside_Max_Exclusive()
        {
            bool result;

            {
                result = Throw.Try.Arg.Range.Outside("arg", MAX_MINUS_ONE, MIN, MAX, true, false, out var exception);
                result.Should().BeFalse();
                exception.Should().BeNull();
            }
            {
                result = Throw.Try.Arg.Range.Outside("arg", MAX_EQUAL, MIN, MAX, true, false, out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentOutOfRangeException>();
            }
            {
                result = Throw.Try.Arg.Range.Outside("arg", MAX_PLUS_ONE, MIN, MAX, true, false, out var exception);
                result.Should().BeTrue();
                exception.Should().NotBeNull().And.BeOfType<ArgumentOutOfRangeException>();
            }
        }
    }
}
