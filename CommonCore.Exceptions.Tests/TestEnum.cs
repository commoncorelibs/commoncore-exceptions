﻿#region Copyright (c) 2021 Jay Jeckel
// Copyright (c) 2021 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

namespace CommonCore.Exceptions.Tests
{
    public enum TestEnum
    {
        NegOne = -1,
        Zero = 0,
        One,
        Two,
        Three,
        Four,
        Six = 6,
        Eight = 8
    }
}
