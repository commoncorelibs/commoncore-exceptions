# Release v1.0.0-pre.7

* Removed all deprecated code #23
* Add `Path` hooks #29

## Dependencies

* [CommonCore.Shims](https://gitlab.com/commoncorelibs/commoncore-shims) made a non-conditional dependency #28
