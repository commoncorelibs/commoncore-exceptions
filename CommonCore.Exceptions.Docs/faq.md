﻿# Frequently Asked Questions

## Where is the stacktrace generated?

Stacktraces are always generated where the exception is thrown.

In the case of `Throw.If` methods, that would be inside the helper method itself. It is also possible that, when compiled in release mode with optimizations on, some or all of the helper method calls may be inlined away. In these cases the helper method may not appear in the stacktrace.

While `Throw.As` only constructs an exception instance, which doesn't generate a stacktrace, and `Throw.Try` only passes back an exception instance without throwing it. In the later case, any stacktrace would be generated if/where the calling code throws it; making this one way to remove any helper method from the stacktrace.

Although not currently on the roadmap, when the library eventually upgrades to dotnet 6 the [StackTraceHidden](https://docs.microsoft.com/en-us/dotnet/api/system.diagnostics.stacktracehiddenattribute?view=net-6.0) attribute could be used to remove the helper method from the stacktrace completely.

## Are throw helpers inefficient?

Technically they can be less efficient than the same operations written natively, just as any code is technically less efficient wrapped in a method than it is ouside that method.

At worst, when an exception isn't thrown, the `Throw.If` and `Throw.Try` methods require a total of **2** additional calls to static extension methods over what the native implementation would require. In the case of the throwing branch, it rises to **4** total calls to static extension methods.

It is possible that some or all of the calls could be inlined by the compiler, making the helper methods just as efficient as hand writing the validation. However, regardless of inlining, it premature optimization to worry about four extra static calls without solid profiling showing them to be a problem.

## Will helper methods be inlined?

The simple answer is, I don't know, yes, no, maybe sometimes.

Worst case, no, and the trivial cost of a few extra extension method calls is still offset by the stability brought through standardization and ease of implementing argument validation. Best case, you still get those benefits, but it's inlined and entirely free.

Several avenues are being explored to answer this question with more concrete information, such as benchmarking and possible compiler-related attributes to encourage inlining where appropriate.

Bottom line, trust the compiler to do its job and focus on validating your arguments.
