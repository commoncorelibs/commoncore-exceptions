﻿# Throw If Pattern

`Throw.If` provides methods for throwing exceptions based on validation. Representing the primary access point for the library, `Throw.If` and it's descendant methods provide the functionality needed in most common cases.

Extension Method signature:
* [Extensibility Hooks](extensibility.md): `ThrowIf*Hook`
* Parameters: value to validate, additional varies depending on validation
* Return: `void`

Library relations:
* Consumers: none
* Providers: [Throw.Is](throw_is.md), [Throw.As](throw_as.md)

Hierarchy ([Extensibility Hook](extensibility.md)):
* Throw.If (`ThrowIfHook`)
  * Arg (`ThrowIfArgHook`)
    * Enum (`ThrowIfArgEnumHook`)
    * Index (`ThrowIfArgIndexHook`)
    * Range (`ThrowIfArgRangeHook`)

```csharp
public static void SomeExtension(this object self, object model,
    double angle, List<Point> points, int activePointIndex)
{
    // Throws if 'self' is null, NullReferenceException.
    Throw.If.SelfNull(self);

    // Throws if model is null, ArgumentNullException.
    Throw.If.Arg.Null(nameof(model), model);

    // Throws if angle is lesser than 0, ArgumentOutOfRangeException.
    Throw.If.Arg.Range.Less(nameof(angle), angle, 0.0);

    // Throws if angle is greater than or equal to 360, ArgumentOutOfRangeException.
    Throw.If.Arg.Range.MoreOrEqual(nameof(angle), angle, 360.0);

    // Throws if points list is null, ArgumentNullException.
    Throw.If.Arg.Null(nameof(points), points);

    // Throws if points list is empty, ie Count == 0, ArgumentEmptyException.
    Throw.If.Arg.Empty(nameof(points), points);

    // Throws if activePointIndex is not a valid index into points list, ArgumentIndexOutOfRangeException.
    Throw.If.Arg.Index.Invalid(nameof(activePointIndex), activePointIndex, points);
}
```
