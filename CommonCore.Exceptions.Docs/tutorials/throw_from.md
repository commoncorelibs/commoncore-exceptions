﻿# Message Building

`Throw.From` provides methods for formatting exception messages. Along with [Throw.Is](throw_is.md), `Throw.From` and its descendant methods represent the two prongs the library's higher functionality is built upon.

Extension Method signature:
* [Extensibility Hooks](extensibility.md): `ThrowFrom*Hook`
* Parameters: varies depending on message format
* Return: `string`

Library relations:
* Consumers: [Throw.As](throw_as.md)
* Providers: none

Hierarchy ([Extensibility Hook](extensibility.md)):
* Throw.From (`ThrowFromHook`)
  * Arg (`ThrowFromArgHook`)
    * Enum (`ThrowFromArgEnumHook`)
    * Index (`ThrowFromArgIndexHook`)
    * Range (`ThrowFromArgRangeHook`)

Example:
```csharp
string message;
message = Throw.From.Arg.Null(nameof(value), value);
message = Throw.From.Index.Invalid(nameof(index), index, someArray);
message = Throw.From.Range.OutsideInclusiveExclusive(nameof(degrees), degrees, min: 0, max: 360);
```

`Throw.From` methods are effectively a wrapper around `string.Format()` and a specific format string, but their benefit is in explicitly declaring the parameters needed to format the message. To this end, they should avoid performing any logic or processing beyond taking the parameters they need to populate their format string and passing them along.

As a result, `Throw.From` methods can require more expressive names and more restrictive parameter requirements than related methods under other `Throw` properties. For example, the `Range.Outside()` family of methods allow two `bool` parameters to control the inclusiveness of the min and max targets; however, `Throw.From.Range.Outside()` doesn't exist. Instead, there are four methods, one each corresponding to the four states of two booleans; `OutsideInclusive()`, `OutsideExclusive()`, `OutsideInclusiveExclusive()`, `OutsideExclusiveInclusive()`, each one wrapping its own specific message format string worded for its specific state. The logic for deciding between these format methods can then be done in the related [Throw.As](throw_as.md) methods.
