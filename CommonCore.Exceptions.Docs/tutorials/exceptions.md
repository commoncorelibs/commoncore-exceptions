﻿# Exceptions

While primarily focused on throwing exceptions, in an effort to make handling exceptions more direct and specific the library provides several custom exceptions. The library's methods use and throw these custom exceptions where ever appropriate.

Derived from `ArgumentException`:
* `ArgumentDefaultException`
* `ArgumentDefaultOrEmptyException`
* `ArgumentEmptyException`
* `ArgumentNullOrEmptyException`
* `ArgumentReadOnlyException`

Derived from `ArgumentOutOfRangeException`:
* `ArgumentEnumOutOfRangeException`
* `ArgumentIndexOutOfRangeException`
