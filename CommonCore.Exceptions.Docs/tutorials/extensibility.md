﻿# Extensibility

One of the most important design goals of the library is being extendable by external libraries without loosing the efficiency or usability of library-provided functionality. The goal is accomplished through the use of extension methods and empty `readonly` `struct` types for them to extend.

## Overview

The library is centered around the `static` `Throw` `class` which is simply a convenient wrapper around several properties. Stripped of documentation and other noise, the class is six lines of code.

```csharp
    public static partial class Throw
    {
        public static ThrowFromHook From { get; }
        public static ThrowAsHook As { get; }
        public static ThrowIsHook Is { get; }
        public static ThrowIfHook If { get; }
        public static ThrowTryHook Try { get; }
    }
```

The magic is those `*Hook` types. It isn't anything inherent to those types themselves, they are equally hollow types being completely empty or containing only properties to other `*Hook` types. Instead, the magic comes from their mere existence and the ability that provides to tie extension methods to the central `Throw` class. All without any method registration mechanism or attribute fishing expedition, and it even avoids the few minor heap allocations by using `readonly` `struct` types instead of `class` types for the hooks.

Being nothing more than a hook, descendant extension methods don't even need the extended instance and it can be ignored with the discard symbol (`_`), ie `SomeExtentionMethod(this ThrowIfHook _)`. I haven't been able to find any information specific to discarding an extension method `this` parameter, but it is at worst good convention to make the intention clear.

As a result, new validation methods can be added to the system by any arbitrary project and those new methods will be available through the central `Throw` class to any consumer that uses both this library and the project providing the new methods.

To illustrate the process, we will implement the functionality to throw an `ArgumentException` if a `bool` is equal to `false`.

## Step One: Throw.Is

The first step is declaring a `Throw.Is` validation method. The method implementation is trivial, simply inverting the input value, but that will do for this example.

The meat of the example is in the extension part of the signature. The extended type, in this case `ThrowIsHook`, determines where in the `Throw` hierarchy the method will be called from. The convention is simple, take the property chain, eg `Throw.Is` or `Throw.If.Arg.Enum`, remove the dots, add the suffix `Hook`, and you have the needed extension type, eg `ThrowIsHook` or `ThrowIfArgEnumHook`. In this case, we want to call `Throw.Is.False()`, so the extended type is `ThrowIsHook`.

The actual validation should be kept as small and efficient as possible, similar to what one would put in an if-statement as opposed to complex logic one would put in a helper method. Likewise, these methods should not do any form of validation on their arguments; besides the goal of making the methods as small and inline-friendly as possible to the compiler, we also want to avoid circular error throwing.

For more details, see the article for [Throw.Is](throw_is.md).

```csharp
public static bool False(this ThrowIsHook _, bool value) => !value;

bool result = Throw.Is.False(true); // result == false
bool result = Throw.Is.False(false);// result == true
```

## Step Two: Throw.From

The second step is providing an exception message through a formatting method that specifically declares the needed parameters. There are several items of note here.

The extended type is `ThrowFromArgHook`, because we are making a `Throw.From` method for arguments, so the method should be callable from `Throw.From.Arg.False()` specifically.

The method requires two arguments, the name of the parameter being validated (this is usually provided through the `nameof()` function), and the value of the argument as an `object`.

The value is passed as an `object` according to the rule of least knowledge; the method just wraps a `string.Format()` call, since that call takes a series of `object` instances, that means we don't need to know the specific types of objects passed to us, so we can also accept `object`; that is the least amount of knowledge we need to do the job.

Contrast that with the `paramName` parameter. While, technically, we don't need to know that the name passed is a `string`, part of the contract of the library is that parameter names are of the `string` type and we do have a responsibility to follow and enforce signature contracts of the library.

Lastly, similar to `Throw.Is`, `Throw.From` methods shouldn't do any validation of arguments or any other extra work beyond their job of building a formatted string; so they are as appealing as possible for inlining by the compiler.

For more details, see the article for [Throw.From](throw_from.md).

```csharp
public static string False(this ThrowFromArgHook _, string paramName, object value)
    => string.Format("Argument '{0}' value '{1}' must not be false.", paramName, value);

public void SomeMethodSomeWhereElse(bool option)
{
    string result = Throw.From.Arg.False(nameof(option), option);
    // option == true
    // result == "Argument 'option' value 'true' must not be false."
    // option == false
    // result == "Argument 'option' value 'false' must not be false."
}
```

## Step Three: Throw.As

The third step is providing a method to construct the desired exception with its formatted message. As expected, the extended type is `ThrowAsArgHook` and the method would be called from `Throw.As.Arg.False()`.

`Throw.As` methods effectively define the api for their related family of functionality. For that reason, they will generally have more specific typing than their underlying `Throw.From` method. To make that point clear, we will break with the `Throw.From.Arg.False(string, object)` signature and explicitly declare `value` as `bool`, making the method's contract clear; its purpose is to validate `bool` values, nothing more, nothing less.

For more details, see the article for [Throw.As](throw_as.md).

```csharp
public static ArgumentException False(this ThrowAsArgHook _, string paramName, bool value)
    => new(paramName, Throw.From.Arg.False(paramName, value));

public void SomeMethodSomeWhereElse(bool option)
{
    // option == false
    // Throws ArgumentException with Throw.From.Arg.False() message.
    if (!option) { throw Throw.As.Arg.False(nameof(option), option);

    // option == true
    // Do whatever.
}
```

## Step Four: Throw.If

The forth step is declaring a method to throw the `Throw.As.Arg.False()` exception with the `Throw.From.Arg.False()` message if the `Throw.Is.False()` validation condition is met.

For more details, see the article for [Throw.If](throw_if.md).

```csharp
public static void False(this ThrowIfArgHook _, string paramName, bool value)
{ if (Throw.Is.False(value)) { throw Throw.As.Arg.False(paramName, value); } }

public void SomeMethodSomeWhereElse(bool option)
{
    // option == false
    // If Throw.Is.False() == true (it would)
    // then throws Throw.As.Arg.False() with Throw.From.Arg.False() message.
    Throw.If.Arg.False(nameof(option), option);

    // option == true
    // Throw.Is.False() == false
    // Do whatever.
}
```

## Step Five: Throw.Try

The fifth step is adding a method to construct a `Throw.As.Arg.False()` exception with the `Throw.From.Arg.False()` message if the `Throw.Is.False()` validation condition is met, but, instead of throwing it, return a `bool` and pass the exception back to the caller through an `out` parameter. The method should be self-explanatory; extend the `ThrowTryArgHook` for `Throw.Try.Arg.False()` access, value is type `bool` following the `Throw.As.Arg.False()` signature, add the `out` parameter `exception`, and return a `bool`.

A one-line implementation is provided, but commented out for this example in favor of the slightly more verbose form. Both operate in the same way; if `Throw.Is` condition is met, then set `exception` to `Throw.As` result, otherwise set `exception` to `null`; return `true` if `exception` is not `null`, otherwise return `false`. The one-liner takes advantage of parenthetical operations being done first and assignment operations collapsing to the left side of the operator.

For more details, see the article for [Throw.Try](throw_try.md).

```csharp
public static bool False(this ThrowTryArgHook _, string paramName, bool value, out ArgumentException exception)
//    => (exception = Throw.Is.False(value) ? Throw.As.Arg.False(paramName, value) : null) is not null;
{
    if (Throw.Is.False(value)) { exception = Throw.As.Arg.False(paramName, value); }
    else { exception = null; }
    return exception is not null;
}

public void SomeMethodSomeWhereElse(bool option)
{
    // option == false
    // If Throw.Is.False() == true (it would)
    // then exception set to Throw.As.Arg.False() with Throw.From.Arg.False() message
    // and returns true.
    if (Throw.Try.Arg.False(nameof(option), option, out ArgumentException exception))
    {
        // Do something like logging, whatever, etc.
        throw exception;
    }

    // option == true
    // Throw.Is.False() == false
    // Throw.Try.Arg.False() == false
    // Do whatever.
}
```

## Nameing Conventions

All `Throw` method names should clearly state the condition in which they would be true or an exception would be throw. The names should be informed by the context they are called from; so a `Under()` method would never be prefixed as `IsUnder()` or `IfUnder()` since that is within the context of `Throw.Is.Under()` or `Throw.If.Under()`. However, it might be prefixed with a `NotUnder()`, although some form of `Over()` alternative would be preferable when possible.

## Conclusion

This was a somewhat contrived example. In practice, the `Throw.From` and `Throw.As` methods wouldn't take the value, as the message only applies to values that are `false`, so including the `false` value in the message is redundant. However, its simplicity isn't out of line. The `Throw.Is.Null()` method is directly comparable as it merely wraps a `value is null` comparison. Take the following example.

```csharp
object thing = null;
Throw.If.Arg.Null(nameof(thing), thing);
// Throw.If.Arg.Null(Throw.Is.Null(thing), Throw.As.Arg.Null(Throw.As.Arg.Null(nameof(thing), thing)));
if (thing is null)
{
    throw new ArgumentNullException(nameof(thing),
        string.Format("Argument '{0}' value must not be null.",
            nameof(thing)));
}
```

The `Throw.If` call would expand conceptually into the pseudo-code in the commented out line and would expand practically to the native code below it. It would seem that, worst case, the failure path adds two static extension method calls, `Throw.If` and `Throw.Is`, and the success path adds two additional static extension method calls, `Throw.As` and `Throw.From`. That isn't bad compared to the native form. Even better, as has been alluded to several times in this article, if all of these methods are kept small and clean, when the compiler is in release mode with optimizations active it should be able to inline the original `Throw.If` call into its expanded native form for us, giving us the best of both worlds.
