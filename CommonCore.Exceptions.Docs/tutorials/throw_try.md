﻿# Throw Try Pattern

`Throw.Try` provides methods for retrieving an exception instance based on validation. This represents an alternative to `Throw.If` that, instead of directly throwing an exception, follows the **try-out-pattern**, similar to the `int.TryParse()` related family of methods, returning a `bool` and passing any constructed exception instance back to the caller via an `out` parameter.

Extension Method signature:
* [Extensibility Hooks](extensibility.md): `ThrowTry*Hook`
* Parameters: value to validate, additional varies depending on validation, `out` exception result
* Return: `bool`

Library relations:
* Consumers: none
* Providers: [Throw.Is](throw_is.md), [Throw.As](throw_as.md)

Hierarchy ([Extensibility Hook](extensibility.md)):
* Throw.Try (`ThrowTryHook`)
  * Arg (`ThrowTryArgHook`)
    * Enum (`ThrowTryArgEnumHook`)
    * Index (`ThrowTryArgIndexHook`)
    * Range (`ThrowTryArgRangeHook`)

```csharp
public static void SomeMethod(string text, int textIndex)
{
    List<Exception> exceptions = new();
    { if (Throw.Try.Arg.Null(nameof(text), text, out var exception) { exceptions.Add(excepion); } }
    { if (Throw.Try.Arg.Empty(nameof(text), text, out var exception) { exceptions.Add(excepion); } }
    { if (Throw.Try.Arg.Range.Less("text.Length", text.Length, 10, out var exception) { exceptions.Add(excepion); } }
    { if (Throw.Try.Arg.Index.Invalid(nameof(textIndex), textIndex, text, out var exception) { exceptions.Add(excepion); } }

    if (exceptions.Count > 0)
    {
        throw new AggregateException(exceptions);
    }
}
```

Each call to a `Throw.Try` method is wrapped in an extra set of braces (`{}`). This is a workaround to reuse `exception` as the `out` parameter name. Without the extra braces, each `Trow.Try` call would require a unique name for its `out` parameter.

```csharp
public static void SomeMethod(string text, int textIndex)
{
    List<Exception> exceptions = new();
    if (Throw.Try.Arg.Null(nameof(text), text, out var textNull) { exceptions.Add(textNull); }
    if (Throw.Try.Arg.Empty(nameof(text), text, out var textEmpty) { exceptions.Add(textEmpty); }
    if (Throw.Try.Arg.Range.Less("text.Length", text.Length, 10, out var textLengthLess) { exceptions.Add(textLengthLess); }
    if (Throw.Try.Arg.Index.Invalid(nameof(textIndex), textIndex, text, out var textIndexInvalid) { exceptions.Add(textIndexInvalid); }

    if (exceptions.Count > 0)
    {
        throw new AggregateException(exceptions);
    }
}
```

Some may find the former style off-putting, but in this specific case I find it useful. Regardless, the functionality of the library is the same.
