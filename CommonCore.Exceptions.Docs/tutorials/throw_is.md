﻿# Argument Validation

`Throw.Is` provides methods for validating values. Along with [Throw.From](throw_from.md), `Throw.Is` and its descendant methods represent the two prongs the library's higher functionality is built upon.

Extension Method signature:
* [Extensibility Hooks](extensibility.md): `ThrowIs*Hook`
* Parameters: value to validate, additional varies depending on validation
* Return: `bool`

Library relations:
* Consumers: [Throw.If](throw_if.md), [Throw.Try](throw_try.md)
* Providers: none

Hierarchy ([Extensibility Hook](extensibility.md)):
* Throw.Is (`ThrowIsHook`)
  * Enum (`ThrowIsEnumHook`)
  * Index (`ThrowIsIndexHook`)
  * Range (`ThrowIsRangeHook`)

Example:
```csharp
bool result;
result = Throw.Is.Null(nameof(value), value);
result = Throw.Is.Index.Invalid(nameof(index), index, someArray);
result = Throw.Is.Range.Outside(nameof(degrees), degrees, min: 0, max: 360, minInclusive: true, maxInclusive: false);
```

`Throw.Is` is unique in that it doesn't have an `Arg` sub-property. When validating a value, that value coming from an argument doesn't actually factor into any condition checking. In other words, a null argument is null in the same way as a null non-argument, so separate methods would be redundant.
