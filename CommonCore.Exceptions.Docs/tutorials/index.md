# CommonCore.Exceptions Introduction

**CommonCore.Exceptions** is a .Net Standard 2.0/2.1 library designed to simplify the validating and throwing of common exceptions.

The library was created to simplify and standardize argument validation, exception throwing, and exception message formatting with the goal of lowering the barrier of effort for implementing proper validation. In this way, hopefully, developers will be less likely to leave such an important task as a "TODO" item mentally scheduled for a vaguely defined point in the future.

Design goals:
* No internal state; the entire library should be pure, reading input and producing output.
* No intermediate heap allocations; only exceptions and message strings are instanced.
* Fail fast and cheap; not throwing should have no overhead beyond the validation call.
* Centralized access; all functionality should be available through a single static class.
* Extendable structure; other libraries should be able to define their own validation methods.

Usability goals:
* No nesting; all functionality is available directly under `CommonCore`.
* Only one entrance; all functionality is called from `Throw` properties.

## Overview

In an effort to improve exception handling as well as throwing, the library provides several [custom exceptions](exceptions.md) that expand upon and narrow down those offered by dotnet. For a detailed walk-through of the system's structure, see the article on [extensibility](extensibility.md).

The library provides all its primary functionality through static properties on the `Throw` class. Each property provides methods for dealing with the validation of values and the throwing of exceptions. The properties provide additional methods through sub-properties and, in some cases, those sub-properties may have sub-properties of their own. This is done to provide organization as well as helping to keep the api expressive and method names simple.

The library can be divided into five parts, each represented by a `Throw` property.

1. [Throw.From](throw_from.md) provides methods for formatting exception messages.
2. [Throw.As](throw_as.md) provides methods for constructing exception instances.
3. [Throw.Is](throw_is.md) provides methods for validating values.
4. [Throw.If](throw_if.md) is the primary access point for the library and provides methods for throwing exceptions based on validation.
5. [Throw.Try](throw_try.md) is an alternative to `Throw.If` that follows the **try-out-pattern**, instead of directly throwing an exception, similar to `int.TryParse()` and family of methods.

## General Usage

```csharp
public static void SomeExtension(this object self, object model,
    double angle, List<Point> points, int activePointIndex)
{
    // Throws if 'self' is null,
    // NullReferenceException.
    Throw.If.SelfNull(self);

    // Throws if model is null,
    // ArgumentNullException.
    Throw.If.Arg.Null(nameof(model), model);

    // Throws if angle is lesser than 0,
    // ArgumentOutOfRangeException.
    Throw.If.Arg.Range.Less(nameof(angle), angle, 0.0);

    // Throws if angle is greater than or equal to 360,
    // ArgumentOutOfRangeException.
    Throw.If.Arg.Range.MoreOrEqual(nameof(angle), angle, 360.0);

    // Throws if points list is null,
    // ArgumentNullException.
    Throw.If.Arg.Null(nameof(points), points);

    // Throws if points list is empty, ie Count == 0,
    // ArgumentEmptyException.
    Throw.If.Arg.Empty(nameof(points), points);

    // Throws if activePointIndex is not a valid index into points list,
    // ArgumentIndexOutOfRangeException.
    Throw.If.Arg.Index.Invalid(nameof(activePointIndex), activePointIndex, points);
}
```

## Partial Usage

```csharp
public static void SomeMethod(string text, int textIndex)
{
    // Do a normal if-throw, but let Throw.As construct the exception.
    if (text is null) { throw Throw.As.Arg.Null(nameof(text)); }

    // Do a normal if-throw, but use Throw.Is validation and Throw.From message formatting,
    // then throw your own custom exception.
    if (Throw.Is.Range.Outside(textIndex, min: 2, max: 9, minInclusive: false, maxInclusive: true))
    {
        throw new MySpecialStringIndexException(nameof(textIndex),
        Throw.From.Range.OutsideExclusiveInclusive(textIndex, 2, 9);
    }
}
```

## Alternative Usage

```csharp
public static void SomeMethod(string text, int textIndex)
{
    List<Exception> exceptions = new();
    { if (Throw.Try.Arg.Null(nameof(text), text, out var exception) { exceptions.Add(excepion); } }
    { if (Throw.Try.Arg.Empty(nameof(text), text, out var exception) { exceptions.Add(excepion); } }
    { if (Throw.Try.Arg.Range.Less("text.Length", text.Length, 10, out var exception) { exceptions.Add(excepion); } }
    { if (Throw.Try.Arg.Index.Invalid(nameof(textIndex), textIndex, text, out var exception) { exceptions.Add(excepion); } }

    if (exceptions.Count > 0)
    {
        throw new AggregateException(exceptions);
    }
}
```
