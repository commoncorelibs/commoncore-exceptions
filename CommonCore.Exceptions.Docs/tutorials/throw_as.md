﻿# Exception Building

`Throw.As` provides methods for constructing exception instances.

Extension Method signature:
* [Extensibility Hooks](extensibility.md): `ThrowAs*Hook`
* Parameters: varies depending on exception type
* Return: varies depending on exception type

Library relations:
* Consumers: [Throw.If](throw_if.md), [Throw.Try](throw_try.md)
* Providers: [Throw.From](throw_from.md)

Hierarchy ([Extensibility Hook](extensibility.md)):
* Throw.As (`ThrowAsHook`)
  * Arg (`ThrowAsArgHook`)
    * Enum (`ThrowAsArgEnumHook`)
    * Index (`ThrowAsArgIndexHook`)
    * Range (`ThrowAsArgRangeHook`)

Example:
```csharp
if (self is null) { throw Throw.As.Null(self); }
if (value is null) { throw Throw.As.Arg.Null(nameof(value), value);
if (index < 0 || index > someArray.Length)
{ throw Throw.As.Arg.Index.Invalid(nameof(index), index, someArray); }
```

`Throw.As` methods tend to dictate the api of the related `Throw.If` and `Throw.Try` methods. If the `Throw.As` methods can handle both `string` and `StringBuilder`, then similar `Throw.If` and `Throw.Try` methods will also be available. The reverse is also, generally true, a `Throw.If` or `Throw.Try` method will correspond to a `Throw.As` method with the same signature.

For this reason, if choosing the appropriate exception message takes some form of logic or processing, then it should be done in this method, so it is in one location and isn't propagated to higher level methods. That said, processing is necessary to choose the message and construct the exception should be kept as small and clean as possible to encourage inlining by the compiler.
