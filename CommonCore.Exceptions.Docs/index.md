# Vinland Solutions CommonCore.Exceptions Library

[![License](https://gitlab.com/commoncorelibs/pipeline/-/raw/master/badge/license/mit.svg)](https://opensource.org/licenses/MIT)
[![Platform](https://gitlab.com/commoncorelibs/pipeline/-/raw/master/badge/platform/netstandard/2.0_2.1.svg)](https://docs.microsoft.com/en-us/dotnet/standard/net-standard)
[![Repository](https://gitlab.com/commoncorelibs/pipeline/-/raw/master/badge/repository.svg)](https://gitlab.com/commoncorelibs/commoncore-exceptions)
[![Releases](https://gitlab.com/commoncorelibs/pipeline/-/raw/master/badge/releases.svg)](https://gitlab.com/commoncorelibs/commoncore-exceptions/-/releases)
[![Documentation](https://gitlab.com/commoncorelibs/pipeline/-/raw/master/badge/documentation.svg)](https://commoncorelibs.gitlab.io/commoncore-exceptions/)  
[![Nuget](https://badgen.net/nuget/v/VinlandSolutions.CommonCore.Exceptions/latest?icon)](https://www.nuget.org/packages/VinlandSolutions.CommonCore.Exceptions/)
[![Pipeline](https://gitlab.com/commoncorelibs/commoncore-exceptions/badges/master/pipeline.svg)](https://gitlab.com/commoncorelibs/commoncore-exceptions/commits/master)
[![Coverage](https://gitlab.com/commoncorelibs/commoncore-exceptions/badges/master/coverage.svg)](https://commoncorelibs.gitlab.io/commoncore-exceptions/reports/index.html)
[![Tests](https://commoncorelibs.gitlab.io/commoncore-exceptions/badge/tests.svg)](https://commoncorelibs.gitlab.io/commoncore-exceptions/reports/index.html)

NOTE: This project is in alpha development and the public api is extremely unstable. Breaking changes are possible with each pre-release.

**CommonCore.Exceptions** is a .Net Standard 2.0/2.1 library designed to simplify the validating and throwing of common exceptions.

## Installation

The official release versions of the **CommonCore.Exceptions** library are hosted on [NuGet](https://www.nuget.org/packages/VinlandSolutions.CommonCore.Exceptions/) and can be installed using the standard console means, or found through the Visual Studio NuGet Package Managers.

This library is compatible with projects targeting at least .Net Framework 4.6.1, .Net Core 2.0, or .Net 5.

## Usage

One simple use case is validating the extension argument of extension methods, throwing a `NullReferenceException` if the argument is `null`, imitating the behavior of non-extension methods. More generally, arguments can be validated for various states, throwing if the validation condition is met.

```csharp
public static void SomeExtension(this object self, object model,
    double angle, List<Point> points, int activePointIndex)
{
    // Throws if 'self' is null, NullReferenceException.
    Throw.If.SelfNull(self);

    // Throws if model is null, ArgumentNullException.
    Throw.If.Arg.Null(nameof(model), model);

    // Throws if angle is lesser than 0, ArgumentOutOfRangeException.
    Throw.If.Arg.Range.Less(nameof(angle), angle, 0.0);

    // Throws if angle is greater than or equal to 360, ArgumentOutOfRangeException.
    Throw.If.Arg.Range.MoreOrEqual(nameof(angle), angle, 360.0);

    // Throws if points list is null, ArgumentNullException.
    Throw.If.Arg.Null(nameof(points), points);

    // Throws if points list is empty, ie Count == 0, ArgumentEmptyException.
    Throw.If.Arg.Empty(nameof(points), points);

    // Throws if activePointIndex is not a valid index into points list, ArgumentIndexOutOfRangeException.
    Throw.If.Arg.Index.Invalid(nameof(activePointIndex), activePointIndex, points);
}
```

Occasionally greater control is needed and in these cases more specific library functionality can be used.

```csharp
public static void SomeMethod(string text, int textIndex)
{
    // Do a normal if-throw, but let Throw.As construct the exception.
    if (text is null) { throw Throw.As.Arg.Null(nameof(text)); }

    // Do a normal if-throw, but use Throw.Is validation and Throw.From message formatting,
    // then throw your own custom exception.
    if (Throw.Is.Range.Outside(textIndex, min: 2, max: 9, minInclusive: false, maxInclusive: true))
    {
        throw new MySpecialStringIndexException(nameof(textIndex),
        Throw.From.Range.OutsideExclusiveInclusive(textIndex, 2, 9);
    }
}
```

In certain situations, one may need to process an exception after it is constructed but before it is thrown. Such a situation can be handled using the alternative try-out-pattern.

```csharp
public static void SomeMethod(string text, int textIndex)
{
    List<Exception> exceptions = new();
    { if (Throw.Try.Arg.Null(nameof(text), text, out var exception) { exceptions.Add(excepion); } }
    { if (Throw.Try.Arg.Empty(nameof(text), text, out var exception) { exceptions.Add(excepion); } }
    { if (Throw.Try.Arg.Range.Less("text.Length", text.Length, 10, out var exception) { exceptions.Add(excepion); } }
    { if (Throw.Try.Arg.Index.Invalid(nameof(textIndex), textIndex, text, out var exception) { exceptions.Add(excepion); } }

    if (exceptions.Count > 0)
    {
        throw new AggregateException(exceptions);
    }
}
```

## Projects

The **CommonCore.Exceptions** repository is composed of three projects with the listed dependencies:

* **CommonCore.Exceptions**: The dotnet standard 2.0/2.1 library project.
  * [CommonCore.Shims](https://www.nuget.org/packages/VinlandSolutions.CommonCore.Shims/) (dotnet standard 2.0 target)
* **CommonCore.Exceptions.Docs**: The project for generating api documentation.
  * [DocFX](https://github.com/dotnet/docfx)
* **CommonCore.Exceptions.Tests**: The project for running unit tests and generating coverage reports.
  * [xUnit](https://github.com/xunit/xunit)
  * [FluentAssertions](https://github.com/fluentassertions/fluentassertions)

## Links

- Repository: https://gitlab.com/commoncorelibs/commoncore-exceptions
- Issues: https://gitlab.com/commoncorelibs/commoncore-exceptions/issues
- Docs: https://commoncorelibs.gitlab.io/commoncore-exceptions/index.html
- Nuget: https://www.nuget.org/packages/VinlandSolutions.CommonCore.Exceptions/
