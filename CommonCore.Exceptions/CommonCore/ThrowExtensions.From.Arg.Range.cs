﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using CommonCore.Exceptions.Hooks;

namespace CommonCore
{
    /// <summary>
    /// The <see cref="ThrowExtensions_From_Arg_Range"/> <c>static</c> <c>extension</c> <c>class</c>
    /// provides <see cref="ThrowFromArgRangeHook"/> extension methods for the <c>Throw.From.Arg.Range</c>
    /// property.
    /// </summary>
    /// <seealso cref="Throw.From"/>
    public static partial class ThrowExtensions_From_Arg_Range
    {

        /// <summary>
        /// Builds <see cref="string"/> exception message from the specified arguments for <see cref="ArgumentOutOfRangeException"/>.
        /// </summary>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="paramName">The name of the caller parameter.</param>
        /// <param name="value">The index to validate.</param>
        /// <param name="min">The minimum target to validate against.</param>
        /// <returns><see cref="string"/> exception message for <see cref="ArgumentOutOfRangeException"/>.</returns>
        public static string Less(this ThrowFromArgRangeHook _, string paramName, object value, object min)
            => "Argument '{0}' value '{1}' must be greater than or equal to '{2}'.".AsFormat(paramName, value, min);

        /// <inheritdoc cref="Less(ThrowFromArgRangeHook, string, object, object)"/>
        public static string LessOrEqual(this ThrowFromArgRangeHook _, string paramName, object value, object min)
            => "Argument '{0}' value '{1}' must be greater than '{2}'.".AsFormat(paramName, value, min);

        /// <summary>
        /// Builds <see cref="string"/> exception message from the specified arguments for <see cref="ArgumentOutOfRangeException"/>.
        /// </summary>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="paramName">The name of the caller parameter.</param>
        /// <param name="value">The index to validate.</param>
        /// <param name="max">The maximum target to validate against.</param>
        /// <returns><see cref="string"/> exception message for <see cref="ArgumentOutOfRangeException"/>.</returns>
        public static string More(this ThrowFromArgRangeHook _, string paramName, object value, object max)
            => "Argument '{0}' value '{1}' must be less than or equal to '{2}'.".AsFormat(paramName, value, max);

        /// <inheritdoc cref="More(ThrowFromArgRangeHook, string, object, object)"/>
        public static string MoreOrEqual(this ThrowFromArgRangeHook _, string paramName, object value, object max)
            => "Argument '{0}' value '{1}' must be less than '{2}'.".AsFormat(paramName, value, max);

        /// <summary>
        /// Builds <see cref="string"/> exception message from the specified arguments for <see cref="ArgumentOutOfRangeException"/>.
        /// </summary>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="paramName">The name of the caller parameter.</param>
        /// <param name="value">The index to validate.</param>
        /// <param name="min">The minimum target to validate against.</param>
        /// <param name="max">The maximum target to validate against.</param>
        /// <returns><see cref="string"/> exception message for <see cref="ArgumentOutOfRangeException"/>.</returns>
        public static string OutsideInclusive(this ThrowFromArgRangeHook _, string paramName, object value, object min, object max)
            => "Argument '{0}' value '{1}' must range from '{2}' to '{3}', inclusive.".AsFormat(paramName, value, min, max);

        /// <inheritdoc cref="OutsideInclusive(ThrowFromArgRangeHook, string, object, object, object)"/>
        public static string OutsideExclusive(this ThrowFromArgRangeHook _, string paramName, object value, object min, object max)
            => "Argument '{0}' value '{1}' must range from '{2}' to '{3}', exclusive.".AsFormat(paramName, value, min, max);

        /// <inheritdoc cref="OutsideInclusive(ThrowFromArgRangeHook, string, object, object, object)"/>
        public static string OutsideInclusiveExclusive(this ThrowFromArgRangeHook _, string paramName, object value, object min, object max)
            => "Argument '{0}' value '{1}' must range from '{2}', inclusive, to '{3}', exclusive.".AsFormat(paramName, value, min, max);

        /// <inheritdoc cref="OutsideInclusive(ThrowFromArgRangeHook, string, object, object, object)"/>
        public static string OutsideExclusiveInclusive(this ThrowFromArgRangeHook _, string paramName, object value, object min, object max)
            => "Argument '{0}' value '{1}' must range from '{2}', exclusive, to '{3}', inclusive.".AsFormat(paramName, value, min, max);

    }
}
