﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using System.Linq;
using CommonCore.Exceptions.Hooks;

namespace CommonCore
{
    /// <summary>
    /// The <see cref="ThrowExtensions_As_Arg_Enum"/> <c>static</c> <c>extension</c> <c>class</c>
    /// provides <see cref="ThrowAsArgEnumHook"/> extension methods for the <c>Throw.As.Arg.Enum</c>
    /// property.
    /// </summary>
    /// <seealso cref="Throw.As"/>
    public static partial class ThrowExtensions_As_Arg_Enum
    {

        /// <summary>
        /// Builds <see cref="ArgumentEnumOutOfRangeException"/> instance.
        /// </summary>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="paramName">The name of the caller parameter.</param>
        /// <param name="value">The value to validate.</param>
        /// <returns><see cref="ArgumentEnumOutOfRangeException"/> instance.</returns>
        public static ArgumentEnumOutOfRangeException Invalid<T>(this ThrowAsArgEnumHook _, string paramName, T value)
            where T : struct, Enum
        {
            var values = Enum.GetValues(typeof(T)).Cast<T>();
            return new(paramName, Throw.From.Arg.Enum.Invalid<T>(paramName, value, values.Min(), values.Max()));
        }

    }
}
