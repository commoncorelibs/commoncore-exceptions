﻿#region Copyright (c) 2014 Jay Jeckel
// Copyright (c) 2014 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System.Runtime.CompilerServices;
using CommonCore.Exceptions.Hooks;

namespace CommonCore
{
    /// <summary>
    /// The <see cref="Throw"/> <c>static</c> <c>helper</c> <c>class</c> provides
    /// centralized access to methods that perform various exception related operations,
    /// such as formating exception messages, constructing exception instances,
    /// validating arguments, and throwing exceptions.
    /// </summary>
    /// <seealso cref="Throw.From"/>
    /// <seealso cref="Throw.As"/>
    /// <seealso cref="Throw.Is"/>
    /// <seealso cref="Throw.If"/>
    /// <seealso cref="Throw.Try"/>
    public static partial class Throw
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal static string AsFormat(this string self, params object[] args)
            => string.Format(self, args);

        internal const bool USE_OF_OBSOLETE_OUT_PARAM_METHODS_IS_ERROR = true;

        internal const bool USE_OF_OBSOLETE_THROW_BUILD_CLASS_IS_ERROR = true;

        internal const bool USE_OF_OBSOLETE_THROW_FORMATS_CLASS_IS_ERROR = true;

        internal const bool USE_OF_OBSOLETE_ARG_INDEX_INCLUSIVE_METHODS_IS_ERROR = false;

        /// <summary>
        /// Gets the <c>Throw.From</c> hook that provides methods for building exception messages.
        /// 
        /// <para>
        /// All <c>Throw.From</c> methods accept necessary parameters and return a formated <see cref="string"/> exception message.
        /// </para>
        /// </summary>
        public static ThrowFromHook From { get; }

        /// <summary>
        /// Gets the <c>Throw.As</c> hook that provides methods for building exception objects.
        /// 
        /// <para>
        /// All <c>Throw.As</c> methods accept necessary parameters and return an exception object
        /// populated by its related <c>Throw.From</c> exception message.
        /// </para>
        /// </summary>
        public static ThrowAsHook As { get; }

        /// <summary>
        /// Gets the <c>Throw.Is</c> hook that provides methods for validating input.
        /// 
        /// <para>
        /// All <c>Throw.Is</c> methods accept the value to validate and any parameters needed
        /// to perform the validation and return a <see cref="bool"/> value.
        /// </para>
        /// </summary>
        public static ThrowIsHook Is { get; }

        /// <summary>
        /// Gets the <c>Throw.If</c> hook that provides methods for throwing exceptions.
        /// 
        /// <para>
        /// All <c>Throw.If</c> methods accept the value to validate and any parameters needed
        /// to perform the validation and throw an exception if the validation condition is met.
        /// The validation is performed by <c>Throw.Is</c> methods and the exception is built
        /// by <c>Throw.As</c> methods.
        /// </para>
        /// </summary>
        public static ThrowIfHook If { get; }

        /// <summary>
        /// Gets the <c>Throw.Try</c> hook that provides methods for validating input using the try-pattern.
        /// 
        /// <para>
        /// All <c>Throw.Try</c> methods accept the value to validate and any parameters needed
        /// to perform the validation and return a <see cref="bool"/> indicating if the validation
        /// condition was met as well as passing back an exception through an <c>out</c> parameter.
        /// The validation is performed by <c>Throw.Is</c> methods and the exception is built
        /// by <c>Throw.As</c> methods.
        /// </para>
        /// </summary>
        public static ThrowTryHook Try { get; }
    }
}
