﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using CommonCore.Exceptions.Hooks;

namespace CommonCore
{
    /// <summary>
    /// The <see cref="ThrowExtensions_As_Arg_Range"/> <c>static</c> <c>extension</c> <c>class</c>
    /// provides <see cref="ThrowAsArgRangeHook"/> extension methods for the <c>Throw.As.Arg.Range</c>
    /// property.
    /// </summary>
    /// <seealso cref="Throw.As"/>
    public static partial class ThrowExtensions_As_Arg_Range
    {

        /// <inheritdoc cref="Outside(ThrowAsArgRangeHook, string, object, object, object, bool, bool)"/>
        public static ArgumentOutOfRangeException Less(this ThrowAsArgRangeHook _, string paramName, object value, object min)
            => new(paramName, Throw.From.Arg.Range.Less(paramName, value, min));

        /// <inheritdoc cref="Outside(ThrowAsArgRangeHook, string, object, object, object, bool, bool)"/>
        public static ArgumentOutOfRangeException LessOrEqual(this ThrowAsArgRangeHook _, string paramName, object value, object min)
            => new(paramName, Throw.From.Arg.Range.LessOrEqual(paramName, value, min));

        /// <inheritdoc cref="Outside(ThrowAsArgRangeHook, string, object, object, object, bool, bool)"/>
        public static ArgumentOutOfRangeException More(this ThrowAsArgRangeHook _, string paramName, object value, object max)
            => new(paramName, Throw.From.Arg.Range.More(paramName, value, max));

        /// <inheritdoc cref="Outside(ThrowAsArgRangeHook, string, object, object, object, bool, bool)"/>
        public static ArgumentOutOfRangeException MoreOrEqual(this ThrowAsArgRangeHook _, string paramName, object value, object max)
            => new(paramName, Throw.From.Arg.Range.MoreOrEqual(paramName, value, max));

        /// <summary>
        /// Builds <see cref="ArgumentOutOfRangeException"/> instance.
        /// </summary>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="paramName">The name of the caller parameter.</param>
        /// <param name="value">The value to validate.</param>
        /// <param name="min">The minimum target to validate against.</param>
        /// <param name="max">The maximum target to validate against.</param>
        /// <param name="minInclusive">Whether the minimum target is inclusive; otherwise exclusive.</param>
        /// <param name="maxInclusive">Whether the maximum target is inclusive; otherwise exclusive.</param>
        /// <returns><see cref="ArgumentOutOfRangeException"/> instance.</returns>
        public static ArgumentOutOfRangeException Outside(this ThrowAsArgRangeHook _, string paramName,
            object value, object min, object max, bool minInclusive, bool maxInclusive)
            => new(paramName,
                (minInclusive && maxInclusive
                ? Throw.From.Arg.Range.OutsideInclusive(paramName, value, min, max)
                : minInclusive && !maxInclusive
                ? Throw.From.Arg.Range.OutsideInclusiveExclusive(paramName, value, min, max)
                : !minInclusive && maxInclusive
                ? Throw.From.Arg.Range.OutsideExclusiveInclusive(paramName, value, min, max)
                : Throw.From.Arg.Range.OutsideExclusive(paramName, value, min, max))
                );

    }
}
