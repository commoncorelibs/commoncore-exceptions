﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Security;
using System.Text;
using CommonCore.Exceptions.Hooks;

namespace CommonCore
{
    /// <summary>
    /// The <see cref="ThrowExtensions_If_Arg"/> <c>static</c> <c>extension</c> <c>class</c>
    /// provides <see cref="ThrowIfArgHook"/> extension methods for the <c>Throw.If.Arg</c>
    /// property.
    /// </summary>
    /// <seealso cref="Throw.If"/>
    public static partial class ThrowExtensions_If_Arg
    {

        /// <summary>
        /// Throws <see cref="ArgumentDefaultException"/> if the specified <paramref name="value"/> is <c>default</c>.
        /// </summary>
        /// <typeparam name="T">The type of <c>struct</c> object to validate.</typeparam>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="paramName">The name of the parameter being validated.</param>
        /// <param name="value">The value to validate.</param>
        [DebuggerStepThrough()]
        public static void Default<T>(this ThrowIfArgHook _, string paramName, T value) where T : struct
        { if (Throw.Is.Default(value)) { throw Throw.As.Arg.Default(paramName); } }

        /// <summary>
        /// Throws <see cref="ArgumentDefaultOrEmptyException"/> if the specified <paramref name="value"/> is <c>default</c> or <c>empty</c>.
        /// </summary>
        /// <typeparam name="T">The type of the wrapper value.</typeparam>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="paramName">The name of the parameter being validated.</param>
        /// <param name="value">The value to validate.</param>
        [DebuggerStepThrough()]
        public static void DefaultOrEmpty<T>(this ThrowIfArgHook _, string paramName, Span<T> value)
        { if (Throw.Is.DefaultOrEmpty(value)) { throw Throw.As.Arg.DefaultOrEmpty(paramName); } }

        /// <inheritdoc cref="DefaultOrEmpty{T}(ThrowIfArgHook, string, Span{T})"/>
        [DebuggerStepThrough()]
        public static void DefaultOrEmpty<T>(this ThrowIfArgHook _, string paramName, ReadOnlySpan<T> value)
        { if (Throw.Is.DefaultOrEmpty(value)) { throw Throw.As.Arg.DefaultOrEmpty(paramName); } }

        /// <inheritdoc cref="DefaultOrEmpty{T}(ThrowIfArgHook, string, Span{T})"/>
        [DebuggerStepThrough()]
        public static void DefaultOrEmpty<T>(this ThrowIfArgHook _, string paramName, Memory<T> value)
        { if (Throw.Is.DefaultOrEmpty(value)) { throw Throw.As.Arg.DefaultOrEmpty(paramName); } }

        /// <inheritdoc cref="DefaultOrEmpty{T}(ThrowIfArgHook, string, Span{T})"/>
        [DebuggerStepThrough()]
        public static void DefaultOrEmpty<T>(this ThrowIfArgHook _, string paramName, ReadOnlyMemory<T> value)
        { if (Throw.Is.DefaultOrEmpty(value)) { throw Throw.As.Arg.DefaultOrEmpty(paramName); } }

        /// <summary>
        /// Throws <see cref="ArgumentEmptyException"/> if the specified <paramref name="value"/> is <c>empty</c>.
        /// </summary>
        /// <typeparam name="T">The type of the wrapped value.</typeparam>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="paramName">The name of the parameter being validated.</param>
        /// <param name="value">The value to validate.</param>
        [DebuggerStepThrough()]
        public static void Empty<T>(this ThrowIfArgHook _, string paramName, Span<T> value)
        { if (Throw.Is.Empty(value)) { throw Throw.As.Arg.Empty(paramName); } }

        /// <inheritdoc cref="Empty{T}(ThrowIfArgHook, string, Span{T})"/>
        [DebuggerStepThrough()]
        public static void Empty<T>(this ThrowIfArgHook _, string paramName, ReadOnlySpan<T> value)
        { if (Throw.Is.Empty(value)) { throw Throw.As.Arg.Empty(paramName); } }

        /// <inheritdoc cref="Empty{T}(ThrowIfArgHook, string, Span{T})"/>
        [DebuggerStepThrough()]
        public static void Empty<T>(this ThrowIfArgHook _, string paramName, Memory<T> value)
        { if (Throw.Is.Empty(value)) { throw Throw.As.Arg.Empty(paramName); } }

        /// <inheritdoc cref="Empty{T}(ThrowIfArgHook, string, Span{T})"/>
        [DebuggerStepThrough()]
        public static void Empty<T>(this ThrowIfArgHook _, string paramName, ReadOnlyMemory<T> value)
        { if (Throw.Is.Empty(value)) { throw Throw.As.Arg.Empty(paramName); } }

        /// <inheritdoc cref="Empty{T}(ThrowIfArgHook, string, Span{T})"/>
        [DebuggerStepThrough()]
        public static void Empty(this ThrowIfArgHook _, string paramName, ICollection? value)
        { if (Throw.Is.Empty(value)) { throw Throw.As.Arg.Empty(paramName); } }

        /// <inheritdoc cref="Empty{T}(ThrowIfArgHook, string, Span{T})"/>
        [DebuggerStepThrough()]
        public static void Empty<T>(this ThrowIfArgHook _, string paramName, ICollection<T>? value)
        { if (Throw.Is.Empty(value)) { throw Throw.As.Arg.Empty(paramName); } }

        /// <inheritdoc cref="Empty{T}(ThrowIfArgHook, string, Span{T})"/>
        [DebuggerStepThrough()]
        public static void Empty(this ThrowIfArgHook _, string paramName, string? value)
        { if (Throw.Is.Empty(value)) { throw Throw.As.Arg.Empty(paramName); } }

        /// <inheritdoc cref="Empty{T}(ThrowIfArgHook, string, Span{T})"/>
        [DebuggerStepThrough()]
        public static void Empty(this ThrowIfArgHook _, string paramName, StringBuilder? value)
        { if (Throw.Is.Empty(value)) { throw Throw.As.Arg.Empty(paramName); } }

        /// <inheritdoc cref="Empty{T}(ThrowIfArgHook, string, Span{T})"/>
        [DebuggerStepThrough()]
        public static void Empty(this ThrowIfArgHook _, string paramName, SecureString? value)
        { if (Throw.Is.Empty(value)) { throw Throw.As.Arg.Empty(paramName); } }

        /// <summary>
        /// Throws <see cref="ArgumentNullException"/> if the specified <paramref name="value"/> is <c>null</c>.
        /// </summary>
        /// <typeparam name="T">The type of <c>class</c> object to validate.</typeparam>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="paramName">The name of the parameter being validated.</param>
        /// <param name="value">The value to validate.</param>
        [DebuggerStepThrough]
        public static void Null<T>(this ThrowIfArgHook _, string paramName, T? value) where T : class
        { if (Throw.Is.Null(value)) { throw Throw.As.Arg.Null(paramName); } }

        /// <summary>
        /// Throws <see cref="ArgumentNullOrEmptyException"/> if the specified <paramref name="value"/> is <c>null</c> or <c>empty</c>.
        /// </summary>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="paramName">The name of the parameter being validated.</param>
        /// <param name="value">The value to validate.</param>
        [DebuggerStepThrough()]
        public static void NullOrEmpty(this ThrowIfArgHook _, string paramName, ICollection? value)
        { if (Throw.Is.NullOrEmpty(value)) { throw Throw.As.Arg.NullOrEmpty(paramName); } }

        /// <inheritdoc cref="NullOrEmpty(ThrowIfArgHook, string, ICollection?)"/>
        [DebuggerStepThrough()]
        public static void NullOrEmpty<T>(this ThrowIfArgHook _, string paramName, ICollection<T>? value)
        { if (Throw.Is.NullOrEmpty(value)) { throw Throw.As.Arg.NullOrEmpty(paramName); } }

        /// <inheritdoc cref="NullOrEmpty(ThrowIfArgHook, string, ICollection?)"/>
        [DebuggerStepThrough()]
        public static void NullOrEmpty(this ThrowIfArgHook _, string paramName, string? value)
        { if (Throw.Is.NullOrEmpty(value)) { throw Throw.As.Arg.NullOrEmpty(paramName); } }

        /// <inheritdoc cref="NullOrEmpty(ThrowIfArgHook, string, ICollection?)"/>
        [DebuggerStepThrough()]
        public static void NullOrEmpty(this ThrowIfArgHook _, string paramName, StringBuilder? value)
        { if (Throw.Is.NullOrEmpty(value)) { throw Throw.As.Arg.NullOrEmpty(paramName); } }

        /// <inheritdoc cref="NullOrEmpty(ThrowIfArgHook, string, ICollection?)"/>
        [DebuggerStepThrough()]
        public static void NullOrEmpty(this ThrowIfArgHook _, string paramName, SecureString? value)
        { if (Throw.Is.NullOrEmpty(value)) { throw Throw.As.Arg.NullOrEmpty(paramName); } }

        /// <summary>
        /// Throws <see cref="ArgumentReadOnlyException"/> if the specified <paramref name="value"/> is not <c>readonly</c>.
        /// </summary>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="paramName">The name of the parameter being validated.</param>
        /// <param name="value">The value to validate.</param>
        [DebuggerStepThrough()]
        public static void ReadOnly(this ThrowIfArgHook _, string paramName, SecureString? value)
        { if (Throw.Is.ReadOnly(value)) { throw Throw.As.Arg.ReadOnly(paramName); } }

        /// <summary>
        /// Provides <see cref="ArgumentReadOnlyException"/> if the specified <paramref name="value"/> is <c>readonly</c>.
        /// </summary>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="paramName">The name of the parameter being validated.</param>
        /// <param name="value">The value to validate.</param>
        [DebuggerStepThrough()]
        public static void NotReadOnly(this ThrowIfArgHook _, string paramName, SecureString? value)
        { if (Throw.Is.NotReadOnly(value)) { throw Throw.As.Arg.NotReadOnly(paramName); } }

    }
}
