﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using System.Diagnostics;
using CommonCore.Exceptions.Hooks;

namespace CommonCore
{
    /// <summary>
    /// The <see cref="ThrowExtensions_If_Arg_Range"/> <c>static</c> <c>extension</c> <c>class</c>
    /// provides <see cref="ThrowIfArgRangeHook"/> extension methods for the <c>Throw.If.Arg.Range</c>
    /// property.
    /// </summary>
    /// <seealso cref="Throw.If"/>
    public static partial class ThrowExtensions_If_Arg_Range
    {

        /// <summary>
        /// Throws <see cref="ArgumentOutOfRangeException"/> if the specified <paramref name="value"/>
        /// is less than the specified <paramref name="min"/> target.
        /// </summary>
        /// <typeparam name="T">The type of <see cref="IComparable{T}"/> value to validate.</typeparam>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="paramName">The name of the parameter being validated.</param>
        /// <param name="value">The value to validate.</param>
        /// <param name="min">The minimum target to validate against.</param>
        [DebuggerStepThrough]
        public static void Less<T>(this ThrowIfArgRangeHook _, string paramName, T value, T min) where T : IComparable<T>
        { if (Throw.Is.Range.Less(value, min)) { throw Throw.As.Arg.Range.Less(paramName, value, min); } }

        /// <summary>
        /// Throws <see cref="ArgumentOutOfRangeException"/> if the specified <paramref name="value"/>
        /// is less than or equal to the specified <paramref name="min"/> target.
        /// </summary>
        /// <typeparam name="T">The type of <see cref="IComparable{T}"/> value to validate.</typeparam>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="paramName">The name of the parameter being validated.</param>
        /// <param name="value">The value to validate.</param>
        /// <param name="min">The minimum target to validate against.</param>
        [DebuggerStepThrough]
        public static void LessOrEqual<T>(this ThrowIfArgRangeHook _, string paramName, T value, T min) where T : IComparable<T>
        { if (Throw.Is.Range.LessOrEqual(value, min)) { throw Throw.As.Arg.Range.LessOrEqual(paramName, value, min); } }

        /// <summary>
        /// Throws <see cref="ArgumentOutOfRangeException"/> if the specified <paramref name="value"/>
        /// is more than the specified <paramref name="max"/> target.
        /// </summary>
        /// <typeparam name="T">The type of <see cref="IComparable{T}"/> value to validate.</typeparam>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="paramName">The name of the parameter being validated.</param>
        /// <param name="value">The value to validate.</param>
        /// <param name="max">The maximum target to validate against.</param>
        [DebuggerStepThrough]
        public static void More<T>(this ThrowIfArgRangeHook _, string paramName, T value, T max) where T : IComparable<T>
        { if (Throw.Is.Range.More(value, max)) { throw Throw.As.Arg.Range.More(paramName, value, max); } }

        /// <summary>
        /// Throws <see cref="ArgumentOutOfRangeException"/> if the specified <paramref name="value"/>
        /// is more than or equal to the specified <paramref name="max"/> target.
        /// </summary>
        /// <typeparam name="T">The type of <see cref="IComparable{T}"/> value to validate.</typeparam>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="paramName">The name of the parameter being validated.</param>
        /// <param name="value">The value to validate.</param>
        /// <param name="max">The maximum target to validate against.</param>
        [DebuggerStepThrough]
        public static void MoreOrEqual<T>(this ThrowIfArgRangeHook _, string paramName, T value, T max) where T : IComparable<T>
        { if (Throw.Is.Range.MoreOrEqual(value, max)) { throw Throw.As.Arg.Range.MoreOrEqual(paramName, value, max); } }

        /// <summary>
        /// Throws <see cref="ArgumentOutOfRangeException"/> if the specified <paramref name="value"/> is out of range.
        /// </summary>
        /// <typeparam name="T">The type of <see cref="IComparable{T}"/> value to validate.</typeparam>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="paramName">The name of the parameter being validated.</param>
        /// <param name="value">The value to validate.</param>
        /// <param name="min">The minimum target to validate against.</param>
        /// <param name="max">The maximum target to validate against.</param>
        /// <param name="minInclusive">Whether the minimum target is inclusive; otherwise exclusive.</param>
        /// <param name="maxInclusive">Whether the maximum target is inclusive; otherwise exclusive.</param>
        [DebuggerStepThrough]
        public static void Outside<T>(this ThrowIfArgRangeHook _, string paramName, T value,
            T min, T max, bool minInclusive, bool maxInclusive) where T : IComparable<T>
        { if (Throw.Is.Range.Outside(value, min, max, minInclusive, maxInclusive)) { throw Throw.As.Arg.Range.Outside(paramName, value, min, max, minInclusive, maxInclusive); } }

    }
}
