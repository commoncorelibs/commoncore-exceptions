﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using System.Diagnostics.CodeAnalysis;
using CommonCore.Exceptions.Hooks;

namespace CommonCore
{
    /// <summary>
    /// The <see cref="ThrowExtensions_Try"/> <c>static</c> <c>extension</c> <c>class</c>
    /// provides <see cref="ThrowTryHook"/> extension methods for the <c>Throw.Try</c>
    /// property.
    /// </summary>
    /// <seealso cref="Throw.Try"/>
    public static partial class ThrowExtensions_Try
    {

        /// <summary>
        /// Provides <see cref="NullReferenceException"/> if the specified <paramref name="value"/> is <c>null</c>.
        /// This method is primarily intended for extension methods to verify they are being called
        /// from a valid non-null object, mimicking the behavior of non-extension methods.
        /// </summary>
        /// <typeparam name="T">The type of <c>class</c> object to validate.</typeparam>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="value">The <c>class</c> object to validate.</param>
        /// <param name="exception">
        /// Set to a <see cref="NullReferenceException"/> if validation is <c>true</c>;
        /// otherwise <c>null</c>.
        /// </param>
        /// <returns><c>true</c> if <paramref name="value"/> is <c>null</c>; otherwise <c>false</c>.</returns>
        public static bool SelfNull<T>(this ThrowTryHook _, [NotNullWhen(true)] T? value,
            [NotNullWhen(true), MaybeNullWhen(false)] out NullReferenceException? exception)
            where T : class
            => (exception = Throw.Is.Null(value) ? Throw.As.Null() : null) is not null;

    }
}
