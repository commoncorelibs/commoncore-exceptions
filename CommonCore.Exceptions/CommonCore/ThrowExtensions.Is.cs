﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Security;
using System.Text;
using CommonCore.Exceptions.Hooks;

namespace CommonCore
{
    /// <summary>
    /// The <see cref="ThrowExtensions_Is"/> <c>static</c> <c>extension</c> <c>class</c>
    /// provides <see cref="ThrowIsHook"/> extension methods for the <c>Throw.Is</c>
    /// property.
    /// </summary>
    /// <seealso cref="Throw.Is"/>
    public static partial class ThrowExtensions_Is
    {

        #region Null

        /// <summary>
        /// Validates if the specified <paramref name="value"/> is <c>null</c>.
        /// </summary>
        /// <typeparam name="T">The type of <c>class</c> object to validate.</typeparam>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="value">The value to validate.</param>
        /// <returns><c>true</c> if <paramref name="value"/> is <c>null</c>; otherwise <c>false</c>.</returns>
        public static bool Null<T>(this ThrowIsHook _, T? value) where T : class => value is null;

        #endregion

        #region Default

        /// <summary>
        /// Validates if the specified <paramref name="value"/> is <c>default</c>.
        /// Throws <see cref="ArgumentDefaultException"/> if the specified <paramref name="value"/> is <c>default</c>.
        /// </summary>
        /// <typeparam name="T">The type of <c>struct</c> object to validate.</typeparam>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="value">The value to validate.</param>
        /// <returns><c>true</c> if <paramref name="value"/> is <c>default</c>; otherwise <c>false</c>.</returns>
        public static bool Default<T>(this ThrowIsHook _, T value) where T : struct => value.Equals(default(T));

        #endregion

        #region Empty

        /// <summary>
        /// Validates if the specified <paramref name="value"/> is not <c>null</c> and is <c>empty</c>.
        /// </summary>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="value">The value to validate.</param>
        /// <returns><c>true</c> if <paramref name="value"/> is not <c>null</c> and is <c>empty</c>; otherwise <c>false</c>.</returns>
        public static bool Empty(this ThrowIsHook _, ICollection? value) => value is not null && value.Count == 0;

        /// <inheritdoc cref="Empty(ThrowIsHook, ICollection?)"/>
        public static bool Empty<T>(this ThrowIsHook _, ICollection<T>? value) => value is not null && value.Count == 0;

        /// <inheritdoc cref="Empty(ThrowIsHook, ICollection?)"/>
        public static bool Empty(this ThrowIsHook _, string? value) => value is not null && value.Length == 0;

        /// <inheritdoc cref="Empty(ThrowIsHook, ICollection?)"/>
        public static bool Empty(this ThrowIsHook _, StringBuilder? value) => value is not null && value.Length == 0;

        /// <inheritdoc cref="Empty(ThrowIsHook, ICollection?)"/>
        public static bool Empty(this ThrowIsHook _, SecureString? value) => value is not null && value.Length == 0;

        /// <summary>
        /// Validates if the specified <paramref name="value"/> is <c>empty</c>.
        /// </summary>
        /// <typeparam name="T">The type of the wrapped value.</typeparam>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="value">The value to validate.</param>
        /// <returns><c>true</c> if <paramref name="value"/> is <c>empty</c>; otherwise <c>false</c>.</returns>
        public static bool Empty<T>(this ThrowIsHook _, Span<T> value) => value.Length == 0;

        /// <inheritdoc cref="Empty{T}(ThrowIsHook, Span{T})"/>
        public static bool Empty<T>(this ThrowIsHook _, ReadOnlySpan<T> value) => value.Length == 0;

        /// <inheritdoc cref="Empty{T}(ThrowIsHook, Span{T})"/>
        public static bool Empty<T>(this ThrowIsHook _, Memory<T> value) => value.Length == 0;

        /// <inheritdoc cref="Empty{T}(ThrowIsHook, Span{T})"/>
        public static bool Empty<T>(this ThrowIsHook _, ReadOnlyMemory<T> value) => value.Length == 0;

        #endregion

        #region DefaultOrEmpty

        /// <summary>
        /// Validates if the specified <paramref name="value"/> is <c>default</c> or <c>empty</c>.
        /// </summary>
        /// <typeparam name="T">The type of the wrapped value.</typeparam>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="value">The value to validate.</param>
        /// <returns><c>true</c> if <paramref name="value"/> is <c>default</c> or <c>empty</c>; otherwise <c>false</c>.</returns>
        public static bool DefaultOrEmpty<T>(this ThrowIsHook _, Span<T> value) => value == default || value.Length == 0;

        /// <inheritdoc cref="DefaultOrEmpty{T}(ThrowIsHook, Span{T})"/>
        public static bool DefaultOrEmpty<T>(this ThrowIsHook _, ReadOnlySpan<T> value) => value == default || value.Length == 0;

        /// <inheritdoc cref="DefaultOrEmpty{T}(ThrowIsHook, Span{T})"/>
        public static bool DefaultOrEmpty<T>(this ThrowIsHook _, Memory<T> value) => value.Equals(default) || value.Length == 0;

        /// <inheritdoc cref="DefaultOrEmpty{T}(ThrowIsHook, Span{T})"/>
        public static bool DefaultOrEmpty<T>(this ThrowIsHook _, ReadOnlyMemory<T> value) => value.Equals(default) || value.Length == 0;

        #endregion

        #region NullOrEmpty

        /// <summary>
        /// Validates if the specified <paramref name="value"/> is <c>null</c> or <c>empty</c>.
        /// </summary>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="value">The value to validate.</param>
        /// <returns><c>true</c> if <paramref name="value"/> is <c>null</c> or <c>empty</c>; otherwise <c>false</c>.</returns>
        public static bool NullOrEmpty(this ThrowIsHook _, ICollection? value) => value is null || value.Count == 0;

        /// <inheritdoc cref="NullOrEmpty(ThrowIsHook, ICollection?)"/>
        public static bool NullOrEmpty<T>(this ThrowIsHook _, ICollection<T>? value) => value is null || value.Count == 0;

        /// <inheritdoc cref="NullOrEmpty(ThrowIsHook, ICollection?)"/>
        public static bool NullOrEmpty(this ThrowIsHook _, string? value) => value is null || value.Length == 0;

        /// <inheritdoc cref="NullOrEmpty(ThrowIsHook, ICollection?)"/>
        public static bool NullOrEmpty(this ThrowIsHook _, StringBuilder? value) => value is null || value.Length == 0;

        /// <inheritdoc cref="NullOrEmpty(ThrowIsHook, ICollection?)"/>
        public static bool NullOrEmpty(this ThrowIsHook _, SecureString? value) => value is null || value.Length == 0;

        #endregion

        #region ReadOnly

        /// <summary>
        /// Validates if the specified <paramref name="value"/> is not <c>null</c> and is <c>readonly</c>.
        /// </summary>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="value">The value to validate.</param>
        /// <returns><c>true</c> if <paramref name="value"/> is not <c>null</c> and is <c>readonly</c>; otherwise <c>false</c>.</returns>
        public static bool ReadOnly(this ThrowIsHook _, SecureString? value) => value is not null && value.IsReadOnly();

        /// <inheritdoc cref="ReadOnly(ThrowIsHook, SecureString?)"/>
        public static bool ReadOnly(this ThrowIsHook _, IList? value) => value is not null && value.IsReadOnly;

        /// <inheritdoc cref="ReadOnly(ThrowIsHook, SecureString?)"/>
        public static bool ReadOnly<T>(this ThrowIsHook _, ICollection<T>? value) => value is not null && value.IsReadOnly;

        /// <inheritdoc cref="ReadOnly(ThrowIsHook, SecureString?)"/>
        public static bool ReadOnly<T>(this ThrowIsHook _, List<T>? value) => _.ReadOnly((ICollection<T>?) value);

        /// <inheritdoc cref="ReadOnly(ThrowIsHook, SecureString?)"/>
        public static bool ReadOnly<T>(this ThrowIsHook _, ReadOnlyCollection<T>? value) => _.ReadOnly((ICollection<T>?) value);

        #endregion

        #region NotReadOnly

        /// <summary>
        /// Validates if the specified <paramref name="value"/> is not <c>null</c> and is not <c>readonly</c>.
        /// </summary>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="value">The value to validate.</param>
        /// <returns><c>true</c> if <paramref name="value"/> is not <c>null</c> and is not <c>readonly</c>; otherwise <c>false</c>.</returns>
        public static bool NotReadOnly(this ThrowIsHook _, SecureString? value) => value is not null && !value.IsReadOnly();

        /// <inheritdoc cref="NotReadOnly(ThrowIsHook, SecureString?)"/>
        public static bool NotReadOnly(this ThrowIsHook _, IList? value) => value is not null && !value.IsReadOnly;

        /// <inheritdoc cref="NotReadOnly(ThrowIsHook, SecureString?)"/>
        public static bool NotReadOnly<T>(this ThrowIsHook _, ICollection<T>? value) => value is not null && !value.IsReadOnly;

        /// <inheritdoc cref="NotReadOnly(ThrowIsHook, SecureString?)"/>
        public static bool NotReadOnly<T>(this ThrowIsHook _, List<T>? value) => _.NotReadOnly((ICollection<T>?) value);

        /// <inheritdoc cref="NotReadOnly(ThrowIsHook, SecureString?)"/>
        public static bool NotReadOnly<T>(this ThrowIsHook _, ReadOnlyCollection<T>? value) => _.NotReadOnly((ICollection<T>?) value);

        #endregion

    }
}
