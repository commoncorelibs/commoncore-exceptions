﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using CommonCore.Exceptions.Hooks;

namespace CommonCore
{
    /// <summary>
    /// The <see cref="ThrowExtensions_Is_Enum"/> <c>static</c> <c>extension</c> <c>class</c>
    /// provides <see cref="ThrowIsEnumHook"/> extension methods for the <c>Throw.Is.Enum</c>
    /// property.
    /// </summary>
    /// <seealso cref="Throw.Is"/>
    public static partial class ThrowExtensions_Is_Enum
    {

        #region Invalid

        /// <summary>
        /// Validates if the specified <paramref name="value"/> is <c>invalid</c>.
        /// </summary>
        /// <typeparam name="T">The type of <c>enum</c> value to validate.</typeparam>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="value">The value to validate.</param>
        /// <returns><c>true</c> if <paramref name="value"/> is <c>invalid</c>; otherwise <c>false</c>.</returns>
        public static bool Invalid<T>(this ThrowIsEnumHook _, T value) where T : Enum => !Enum.IsDefined(typeof(T), value);

        #endregion

    }
}
