﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Security;
using System.Text;
using CommonCore.Exceptions.Hooks;

namespace CommonCore
{
    /// <summary>
    /// The <see cref="ThrowExtensions_If_Arg_Index"/> <c>static</c> <c>extension</c> <c>class</c>
    /// provides <see cref="ThrowIfArgIndexHook"/> extension methods for the <c>Throw.If.Arg.Index</c>
    /// property.
    /// </summary>
    /// <seealso cref="Throw.If"/>
    public static partial class ThrowExtensions_If_Arg_Index
    {

        /// <summary>
        /// Throws <see cref="ArgumentIndexOutOfRangeException"/> if the specified <paramref name="index"/> is out of range.
        /// </summary>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="paramName">The name of the parameter being validated.</param>
        /// <param name="index">The index to validate.</param>
        /// <param name="lengthLimiter">The object used to determine the maximum length target to validate against.</param>
        /// <param name="lengthIsValidIndex">Whether the maximum length target is inclusive; otherwise exclusive.</param>
        [DebuggerStepThrough]
        public static void Invalid(this ThrowIfArgIndexHook _, string paramName, int index, int lengthLimiter, bool lengthIsValidIndex = false)
        { if (Throw.Is.Index.Invalid(index, lengthLimiter, lengthIsValidIndex)) { throw Throw.As.Arg.Index.Invalid(paramName, index, lengthLimiter, lengthIsValidIndex); } }

        /// <inheritdoc cref="Invalid(ThrowIfArgIndexHook, string, int, int, bool)"/>
        /// <typeparam name="T">The type of the lengthLimiter items.</typeparam>
        [DebuggerStepThrough]
        public static void Invalid<T>(this ThrowIfArgIndexHook _, string paramName, int index, Span<T> lengthLimiter, bool lengthIsValidIndex = false)
        { if (Throw.Is.Index.Invalid(index, lengthLimiter, lengthIsValidIndex)) { throw Throw.As.Arg.Index.Invalid(paramName, index, lengthLimiter.Length, lengthIsValidIndex); } }

        /// <inheritdoc cref="Invalid{T}(ThrowIfArgIndexHook, string, int, Span{T}, bool)"/>
        [DebuggerStepThrough]
        public static void Invalid<T>(this ThrowIfArgIndexHook _, string paramName, int index, ReadOnlySpan<T> lengthLimiter, bool lengthIsValidIndex = false)
        { if (Throw.Is.Index.Invalid(index, lengthLimiter, lengthIsValidIndex)) { throw Throw.As.Arg.Index.Invalid(paramName, index, lengthLimiter.Length, lengthIsValidIndex); } }

        /// <inheritdoc cref="Invalid{T}(ThrowIfArgIndexHook, string, int, Span{T}, bool)"/>
        [DebuggerStepThrough]
        public static void Invalid<T>(this ThrowIfArgIndexHook _, string paramName, int index, Memory<T> lengthLimiter, bool lengthIsValidIndex = false)
        { if (Throw.Is.Index.Invalid(index, lengthLimiter, lengthIsValidIndex)) { throw Throw.As.Arg.Index.Invalid(paramName, index, lengthLimiter.Length, lengthIsValidIndex); } }

        /// <inheritdoc cref="Invalid{T}(ThrowIfArgIndexHook, string, int, Span{T}, bool)"/>
        [DebuggerStepThrough]
        public static void Invalid<T>(this ThrowIfArgIndexHook _, string paramName, int index, ReadOnlyMemory<T> lengthLimiter, bool lengthIsValidIndex = false)
        { if (Throw.Is.Index.Invalid(index, lengthLimiter, lengthIsValidIndex)) { throw Throw.As.Arg.Index.Invalid(paramName, index, lengthLimiter.Length, lengthIsValidIndex); } }

        /// <inheritdoc cref="Invalid{T}(ThrowIfArgIndexHook, string, int, Span{T}, bool)"/>
        [DebuggerStepThrough]
        public static void Invalid(this ThrowIfArgIndexHook _, string paramName, int index, ICollection? lengthLimiter, bool lengthIsValidIndex = false)
        { if (Throw.Is.Index.Invalid(index, lengthLimiter, lengthIsValidIndex)) { throw Throw.As.Arg.Index.Invalid(paramName, index, lengthLimiter!.Count, lengthIsValidIndex); } }

        /// <inheritdoc cref="Invalid{T}(ThrowIfArgIndexHook, string, int, Span{T}, bool)"/>
        [DebuggerStepThrough]
        public static void Invalid<T>(this ThrowIfArgIndexHook _, string paramName, int index, ICollection<T>? lengthLimiter, bool lengthIsValidIndex = false)
        { if (Throw.Is.Index.Invalid(index, lengthLimiter, lengthIsValidIndex)) { throw Throw.As.Arg.Index.Invalid(paramName, index, lengthLimiter!.Count, lengthIsValidIndex); } }

        /// <inheritdoc cref="Invalid{T}(ThrowIfArgIndexHook, string, int, Span{T}, bool)"/>
        [DebuggerStepThrough]
        public static void Invalid(this ThrowIfArgIndexHook _, string paramName, int index, string? lengthLimiter, bool lengthIsValidIndex = false)
        { if (Throw.Is.Index.Invalid(index, lengthLimiter, lengthIsValidIndex)) { throw Throw.As.Arg.Index.Invalid(paramName, index, lengthLimiter!.Length, lengthIsValidIndex); } }

        /// <inheritdoc cref="Invalid{T}(ThrowIfArgIndexHook, string, int, Span{T}, bool)"/>
        [DebuggerStepThrough]
        public static void Invalid(this ThrowIfArgIndexHook _, string paramName, int index, StringBuilder? lengthLimiter, bool lengthIsValidIndex = false)
        { if (Throw.Is.Index.Invalid(index, lengthLimiter, lengthIsValidIndex)) { throw Throw.As.Arg.Index.Invalid(paramName, index, lengthLimiter!.Length, lengthIsValidIndex); } }

        /// <inheritdoc cref="Invalid{T}(ThrowIfArgIndexHook, string, int, Span{T}, bool)"/>
        [DebuggerStepThrough]
        public static void Invalid(this ThrowIfArgIndexHook _, string paramName, int index, SecureString? lengthLimiter, bool lengthIsValidIndex = false)
        { if (Throw.Is.Index.Invalid(index, lengthLimiter, lengthIsValidIndex)) { throw Throw.As.Arg.Index.Invalid(paramName, index, lengthLimiter!.Length, lengthIsValidIndex); } }

    }
}
