﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using CommonCore.Exceptions.Hooks;

namespace CommonCore
{
    /// <summary>
    /// The <see cref="ThrowExtensions_As_Arg"/> <c>static</c> <c>extension</c> <c>class</c>
    /// provides <see cref="ThrowAsArgHook"/> extension methods for the <c>Throw.As.Arg</c>
    /// property.
    /// </summary>
    /// <seealso cref="Throw.As"/>
    public static partial class ThrowExtensions_As_Arg
    {

        /// <summary>
        /// Builds <see cref="ArgumentDefaultException"/> instance.
        /// </summary>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="paramName">The name of the caller parameter.</param>
        /// <returns><see cref="NullReferenceException"/> instance.</returns>
        public static ArgumentDefaultException Default(this ThrowAsArgHook _, string paramName)
            => new(paramName, Throw.From.Arg.Default(paramName));

        /// <summary>
        /// Builds <see cref="ArgumentDefaultOrEmptyException"/> instance.
        /// </summary>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="paramName">The name of the caller parameter.</param>
        /// <returns><see cref="ArgumentDefaultOrEmptyException"/> instance.</returns>
        public static ArgumentDefaultOrEmptyException DefaultOrEmpty(this ThrowAsArgHook _, string paramName)
            => new(paramName, Throw.From.Arg.DefaultOrEmpty(paramName));

        /// <summary>
        /// Builds <see cref="ArgumentEmptyException"/> instance.
        /// </summary>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="paramName">The name of the caller parameter.</param>
        /// <returns><see cref="ArgumentEmptyException"/> instance.</returns>
        public static ArgumentEmptyException Empty(this ThrowAsArgHook _, string paramName)
            => new(paramName, Throw.From.Arg.Empty(paramName));

        /// <summary>
        /// Builds <see cref="ArgumentNullException"/> instance.
        /// </summary>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="paramName">The name of the caller parameter.</param>
        /// <returns><see cref="ArgumentNullException"/> instance.</returns>
        public static ArgumentNullException Null(this ThrowAsArgHook _, string paramName)
            => new(paramName, Throw.From.Arg.Null(paramName));

        /// <summary>
        /// Builds <see cref="ArgumentNullOrEmptyException"/> instance.
        /// </summary>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="paramName">The name of the caller parameter.</param>
        /// <returns><see cref="ArgumentNullOrEmptyException"/> instance.</returns>
        public static ArgumentNullOrEmptyException NullOrEmpty(this ThrowAsArgHook _, string paramName)
            => new(paramName, Throw.From.Arg.NullOrEmpty(paramName));

        /// <summary>
        /// Builds <see cref="ArgumentReadOnlyException"/> instance.
        /// </summary>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="paramName">The name of the caller parameter.</param>
        /// <returns><see cref="ArgumentReadOnlyException"/> instance.</returns>
        public static ArgumentReadOnlyException ReadOnly(this ThrowAsArgHook _, string paramName)
            => new(paramName, Throw.From.Arg.ReadOnly(paramName));

        /// <summary>
        /// Builds <see cref="ArgumentReadOnlyException"/> instance.
        /// </summary>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="paramName">The name of the caller parameter.</param>
        /// <returns><see cref="ArgumentReadOnlyException"/> instance.</returns>
        public static ArgumentReadOnlyException NotReadOnly(this ThrowAsArgHook _, string paramName)
            => new(paramName, Throw.From.Arg.NotReadOnly(paramName));


    }
}
