﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

namespace CommonCore.Exceptions.Hooks
{
    /// <summary>
    /// The <see cref="ThrowAsHook"/> allows extension methods to be
    /// attached to the <c>Throw.As</c> property.
    /// </summary>
    public readonly struct ThrowAsHook
    {
        /// <summary>
        /// Gets the argument related hook.
        /// </summary>
        public ThrowAsArgHook Arg { get; }
    }

    /// <summary>
    /// The <see cref="ThrowAsArgHook"/> allows extension methods to be
    /// attached to the <c>Throw.As.Arg</c> property.
    /// </summary>
    public readonly struct ThrowAsArgHook
    {
        /// <summary>
        /// Gets the argument enum related hook.
        /// </summary>
        public ThrowAsArgEnumHook Enum { get; }

        /// <summary>
        /// Gets the argument index related hook.
        /// </summary>
        public ThrowAsArgIndexHook Index { get; }

        /// <summary>
        /// Gets the argument path related hook.
        /// </summary>
        public ThrowAsArgPathHook Path { get; }

        /// <summary>
        /// Gets the argument range related hook.
        /// </summary>
        public ThrowAsArgRangeHook Range { get; }
    }

    /// <summary>
    /// The <see cref="ThrowAsArgEnumHook"/> allows extension methods to be
    /// attached to the <c>Throw.As.Arg.Enum</c> property.
    /// </summary>
    public readonly struct ThrowAsArgEnumHook { }

    /// <summary>
    /// The <see cref="ThrowAsArgIndexHook"/> allows extension methods to be
    /// attached to the <c>Throw.As.Arg.Index</c> property.
    /// </summary>
    public readonly struct ThrowAsArgIndexHook { }

    /// <summary>
    /// The <see cref="ThrowAsArgPathHook"/> allows extension methods to be
    /// attached to the <c>Throw.As.Arg.Path</c> property.
    /// </summary>
    public readonly struct ThrowAsArgPathHook { }

    /// <summary>
    /// The <see cref="ThrowAsArgRangeHook"/> allows extension methods to be
    /// attached to the <c>Throw.As.Arg.Range</c> property.
    /// </summary>
    public readonly struct ThrowAsArgRangeHook { }
}
