﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

namespace CommonCore.Exceptions.Hooks
{
    /// <summary>
    /// The <see cref="ThrowFromHook"/> allows extension methods to be
    /// attached to the <c>Throw.From</c> property.
    /// </summary>
    public readonly struct ThrowFromHook
    {
        /// <summary>
        /// Gets the argument related hook.
        /// </summary>
        public ThrowFromArgHook Arg { get; }
    }

    /// <summary>
    /// The <see cref="ThrowFromArgHook"/> allows extension methods to be
    /// attached to the <c>Throw.From.Arg</c> property.
    /// </summary>
    public readonly struct ThrowFromArgHook
    {
        /// <summary>
        /// Gets the argument enum related hook.
        /// </summary>
        public ThrowFromArgEnumHook Enum { get; }

        /// <summary>
        /// Gets the argument index related hook.
        /// </summary>
        public ThrowFromArgIndexHook Index { get; }

        /// <summary>
        /// Gets the argument path related hook.
        /// </summary>
        public ThrowFromArgPathHook Path { get; }

        /// <summary>
        /// Gets the argument range related hook.
        /// </summary>
        public ThrowFromArgRangeHook Range { get; }
    }

    /// <summary>
    /// The <see cref="ThrowFromArgEnumHook"/> allows extension methods to be
    /// attached to the <c>Throw.From.Arg.Enum</c> property.
    /// </summary>
    public readonly struct ThrowFromArgEnumHook { }

    /// <summary>
    /// The <see cref="ThrowFromArgIndexHook"/> allows extension methods to be
    /// attached to the <c>Throw.From.Arg.Index</c> property.
    /// </summary>
    public readonly struct ThrowFromArgIndexHook { }

    /// <summary>
    /// The <see cref="ThrowFromArgPathHook"/> allows extension methods to be
    /// attached to the <c>Throw.From.Arg.Path</c> property.
    /// </summary>
    public readonly struct ThrowFromArgPathHook { }

    /// <summary>
    /// The <see cref="ThrowFromArgRangeHook"/> allows extension methods to be
    /// attached to the <c>Throw.From.Arg.Range</c> property.
    /// </summary>
    public readonly struct ThrowFromArgRangeHook { }
}
