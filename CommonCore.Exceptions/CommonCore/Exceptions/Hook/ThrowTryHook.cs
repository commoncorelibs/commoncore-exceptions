﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

namespace CommonCore.Exceptions.Hooks
{
    /// <summary>
    /// The <see cref="ThrowTryHook"/> allows extension methods to be
    /// attached to the <c>Throw.Try</c> property.
    /// </summary>
    public readonly struct ThrowTryHook
    {
        /// <summary>
        /// Gets the argument related hook.
        /// </summary>
        public ThrowTryArgHook Arg { get; }
    }

    /// <summary>
    /// The <see cref="ThrowTryArgHook"/> allows extension methods to be
    /// attached to the <c>Throw.Try.Arg</c> property.
    /// </summary>
    public readonly struct ThrowTryArgHook
    {
        /// <summary>
        /// Gets the argument enum related hook.
        /// </summary>
        public ThrowTryArgEnumHook Enum { get; }

        /// <summary>
        /// Gets the argument index related hook.
        /// </summary>
        public ThrowTryArgIndexHook Index { get; }

        /// <summary>
        /// Gets the argument path related hook.
        /// </summary>
        public ThrowTryArgPathHook Path { get; }

        /// <summary>
        /// Gets the argument range related hook.
        /// </summary>
        public ThrowTryArgRangeHook Range { get; }
    }

    /// <summary>
    /// The <see cref="ThrowTryArgEnumHook"/> allows extension methods to be
    /// attached to the <c>Throw.Try.Arg.Enum</c> property.
    /// </summary>
    public readonly struct ThrowTryArgEnumHook { }

    /// <summary>
    /// The <see cref="ThrowTryArgIndexHook"/> allows extension methods to be
    /// attached to the <c>Throw.Try.Arg.Index</c> property.
    /// </summary>
    public readonly struct ThrowTryArgIndexHook { }

    /// <summary>
    /// The <see cref="ThrowTryArgPathHook"/> allows extension methods to be
    /// attached to the <c>Throw.Try.Arg.Path</c> property.
    /// </summary>
    public readonly struct ThrowTryArgPathHook { }

    /// <summary>
    /// The <see cref="ThrowTryArgRangeHook"/> allows extension methods to be
    /// attached to the <c>Throw.Try.Arg.Range</c> property.
    /// </summary>
    public readonly struct ThrowTryArgRangeHook { }
}
