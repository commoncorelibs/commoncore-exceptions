﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

namespace CommonCore.Exceptions.Hooks
{
    /// <summary>
    /// The <see cref="ThrowIfHook"/> allows extension methods to be
    /// attached to the <c>Throw.If</c> property.
    /// </summary>
    public readonly struct ThrowIfHook
    {
        /// <summary>
        /// Gets the argument related hook.
        /// </summary>
        public ThrowIfArgHook Arg { get; }
    }

    /// <summary>
    /// The <see cref="ThrowIfArgHook"/> allows extension methods to be
    /// attached to the <c>Throw.If.Arg</c> property.
    /// </summary>
    public readonly struct ThrowIfArgHook
    {
        /// <summary>
        /// Gets the argument enum related hook.
        /// </summary>
        public ThrowIfArgEnumHook Enum { get; }

        /// <summary>
        /// Gets the argument index related hook.
        /// </summary>
        public ThrowIfArgIndexHook Index { get; }

        /// <summary>
        /// Gets the argument path related hook.
        /// </summary>
        public ThrowIfArgPathHook Path { get; }

        /// <summary>
        /// Gets the argument range related hook.
        /// </summary>
        public ThrowIfArgRangeHook Range { get; }
    }

    /// <summary>
    /// The <see cref="ThrowIfArgEnumHook"/> allows extension methods to be
    /// attached to the <c>Throw.If.Arg.Enum</c> property.
    /// </summary>
    public readonly struct ThrowIfArgEnumHook { }

    /// <summary>
    /// The <see cref="ThrowIfArgIndexHook"/> allows extension methods to be
    /// attached to the <c>Throw.If.Arg.Index</c> property.
    /// </summary>
    public readonly struct ThrowIfArgIndexHook { }

    /// <summary>
    /// The <see cref="ThrowIfArgPathHook"/> allows extension methods to be
    /// attached to the <c>Throw.If.Arg.Path</c> property.
    /// </summary>
    public readonly struct ThrowIfArgPathHook { }

    /// <summary>
    /// The <see cref="ThrowIfArgRangeHook"/> allows extension methods to be
    /// attached to the <c>Throw.If.Arg.Range</c> property.
    /// </summary>
    public readonly struct ThrowIfArgRangeHook { }
}
