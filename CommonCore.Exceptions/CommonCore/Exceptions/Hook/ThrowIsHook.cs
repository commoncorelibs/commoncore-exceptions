﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

namespace CommonCore.Exceptions.Hooks
{
    /// <summary>
    /// The <see cref="ThrowIsHook"/> allows extension methods to be
    /// attached to the <c>Throw.Is</c> property.
    /// </summary>
    public readonly struct ThrowIsHook
    {
        /// <summary>
        /// Gets the enum related hook.
        /// </summary>
        public ThrowIsEnumHook Enum { get; }

        /// <summary>
        /// Gets the index related hook.
        /// </summary>
        public ThrowIsIndexHook Index { get; }

        /// <summary>
        /// Gets the path related hook.
        /// </summary>
        public ThrowIsPathHook Path { get; }

        /// <summary>
        /// Gets the range related hook.
        /// </summary>
        public ThrowIsRangeHook Range { get; }
    }

    /// <summary>
    /// The <see cref="ThrowIsEnumHook"/> allows extension methods to be
    /// attached to the <c>Throw.Is.Enum</c> property.
    /// </summary>
    public readonly struct ThrowIsEnumHook { }

    /// <summary>
    /// The <see cref="ThrowIsIndexHook"/> allows extension methods to be
    /// attached to the <c>Throw.Is.Index</c> property.
    /// </summary>
    public readonly struct ThrowIsIndexHook { }

    /// <summary>
    /// The <see cref="ThrowIsPathHook"/> allows extension methods to be
    /// attached to the <c>Throw.Is.Path</c> property.
    /// </summary>
    public readonly struct ThrowIsPathHook { }

    /// <summary>
    /// The <see cref="ThrowIsRangeHook"/> allows extension methods to be
    /// attached to the <c>Throw.Is.Range</c> property.
    /// </summary>
    public readonly struct ThrowIsRangeHook { }
}
