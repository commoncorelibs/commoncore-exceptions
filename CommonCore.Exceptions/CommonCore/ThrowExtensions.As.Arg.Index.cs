﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using System.Diagnostics.CodeAnalysis;
using CommonCore.Exceptions.Hooks;

namespace CommonCore
{
    /// <summary>
    /// The <see cref="ThrowExtensions_As_Arg_Index"/> <c>static</c> <c>extension</c> <c>class</c>
    /// provides <see cref="ThrowAsArgIndexHook"/> extension methods for the <c>Throw.As.Arg.Index</c>
    /// property.
    /// </summary>
    /// <seealso cref="Throw.As"/>
    public static partial class ThrowExtensions_As_Arg_Index
    {

        /// <summary>
        /// Builds <see cref="ArgumentEnumOutOfRangeException"/> instance.
        /// </summary>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="paramName">The name of the caller parameter.</param>
        /// <param name="index">The index to validate.</param>
        /// <param name="length">The length target to validate against.</param>
        /// <param name="lengthIsValidIndex">Whether the maximum length target is inclusive; otherwise exclusive.</param>
        /// <returns><see cref="ArgumentEnumOutOfRangeException"/> instance.</returns>
        public static ArgumentIndexOutOfRangeException Invalid(this ThrowAsArgIndexHook _, string paramName, object index, object length, bool lengthIsValidIndex = false)
            => new(paramName, lengthIsValidIndex ? Throw.From.Arg.Index.InvalidInclusive(paramName, index, length) : Throw.From.Arg.Index.Invalid(paramName, index, length));

        /// <inheritdoc cref="Invalid(ThrowAsArgIndexHook, string, object, object, bool)"/>
        public static ArgumentIndexOutOfRangeException Valid(this ThrowAsArgIndexHook _, string paramName, object index, object length, bool lengthIsValidIndex = false)
            => new(paramName, lengthIsValidIndex ? Throw.From.Arg.Index.ValidInclusive(paramName, index, length) : Throw.From.Arg.Index.Valid(paramName, index, length));

    }
}
