﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using System.Diagnostics.CodeAnalysis;
using CommonCore.Exceptions.Hooks;

namespace CommonCore
{
    /// <summary>
    /// The <see cref="ThrowExtensions_Try_Arg_Enum"/> <c>static</c> <c>extension</c> <c>class</c>
    /// provides <see cref="ThrowTryArgEnumHook"/> extension methods for the <c>Throw.Try.Arg.Enum</c>
    /// property.
    /// </summary>
    /// <seealso cref="Throw.Try"/>
    public static partial class ThrowExtensions_Try_Arg_Enum
    {

        /// <summary>
        /// Provides <see cref="ArgumentEnumOutOfRangeException"/> if the specified <paramref name="value"/> is <c>invalid</c>.
        /// </summary>
        /// <typeparam name="T">The type of <c>enum</c> value to validate.</typeparam>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="paramName">The name of the parameter being validated.</param>
        /// <param name="value">The <c>enum</c> value to validate.</param>
        /// <param name="exception">
        /// Set to a <see cref="ArgumentEnumOutOfRangeException"/> if validation is <c>true</c>;
        /// otherwise <c>null</c>.
        /// </param>
        /// <returns><c>true</c> if <paramref name="value"/> is <c>invalid</c>; otherwise <c>false</c>.</returns>
        public static bool Invalid<T>(this ThrowTryArgEnumHook _, string paramName, T value,
            [NotNullWhen(true), MaybeNullWhen(false)] out ArgumentEnumOutOfRangeException? exception)
            where T : struct, Enum
            => (exception = Throw.Is.Enum.Invalid(value) ? Throw.As.Arg.Enum.Invalid(paramName, value) : null) is not null;

    }
}
