﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Security;
using System.Text;
using CommonCore.Exceptions.Hooks;

namespace CommonCore
{
    /// <summary>
    /// The <see cref="ThrowExtensions_Try_Arg_Index"/> <c>static</c> <c>extension</c> <c>class</c>
    /// provides <see cref="ThrowTryArgIndexHook"/> extension methods for the <c>Throw.Try.Arg.Index</c>
    /// property.
    /// </summary>
    /// <seealso cref="Throw.Try"/>
    public static partial class ThrowExtensions_Try_Arg_Index
    {

        /// <summary>
        /// Provides <see cref="ArgumentIndexOutOfRangeException"/> if the specified <paramref name="index"/> is <c>outside</c> the valid range.
        /// </summary>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="paramName">The name of the parameter being validated.</param>
        /// <param name="index">The index to validate.</param>
        /// <param name="lengthLimiter">The object used to determine the max index value.</param>
        /// <param name="lengthIsValidIndex">Whether the maximum length target is inclusive; otherwise exclusive.</param>
        /// <param name="exception">
        /// Set to a <see cref="ArgumentIndexOutOfRangeException"/> if validation is <c>true</c>;
        /// otherwise <c>null</c>.
        /// </param>
        /// <returns><c>true</c> if <paramref name="index"/> is <c>outside</c> the valid range; otherwise <c>false</c>.</returns>
        public static bool Invalid(this ThrowTryArgIndexHook _, string paramName, int index, int lengthLimiter,
            [NotNullWhen(true), MaybeNullWhen(false)] out ArgumentIndexOutOfRangeException? exception,
            bool lengthIsValidIndex = false)
            => (exception = Throw.Is.Index.Invalid(index, lengthLimiter, lengthIsValidIndex)
            ? Throw.As.Arg.Index.Invalid(paramName, index, lengthLimiter, lengthIsValidIndex)
            : null) is not null;

        /// <inheritdoc cref="Invalid(ThrowTryArgIndexHook, string, int, int, out ArgumentIndexOutOfRangeException?, bool)"/>
        /// <typeparam name="T">The type of the lengthLimiter items.</typeparam>
        public static bool Invalid<T>(this ThrowTryArgIndexHook _, string paramName, int index, Span<T> lengthLimiter,
            [NotNullWhen(true), MaybeNullWhen(false)] out ArgumentIndexOutOfRangeException? exception,
            bool lengthIsValidIndex = false)
            => (exception = Throw.Is.Index.Invalid(index, lengthLimiter, lengthIsValidIndex)
            ? Throw.As.Arg.Index.Invalid(paramName, index, lengthLimiter.Length, lengthIsValidIndex)
            : null) is not null;

        /// <inheritdoc cref="Invalid{T}(ThrowTryArgIndexHook, string, int, Span{T}, out ArgumentIndexOutOfRangeException?, bool)"/>
        public static bool Invalid<T>(this ThrowTryArgIndexHook _, string paramName, int index, ReadOnlySpan<T> lengthLimiter,
            [NotNullWhen(true), MaybeNullWhen(false)] out ArgumentIndexOutOfRangeException? exception,
            bool lengthIsValidIndex = false)
            => (exception = Throw.Is.Index.Invalid(index, lengthLimiter, lengthIsValidIndex)
            ? Throw.As.Arg.Index.Invalid(paramName, index, lengthLimiter.Length, lengthIsValidIndex)
            : null) is not null;

        /// <inheritdoc cref="Invalid{T}(ThrowTryArgIndexHook, string, int, Span{T}, out ArgumentIndexOutOfRangeException?, bool)"/>
        public static bool Invalid<T>(this ThrowTryArgIndexHook _, string paramName, int index, Memory<T> lengthLimiter,
            [NotNullWhen(true), MaybeNullWhen(false)] out ArgumentIndexOutOfRangeException? exception,
            bool lengthIsValidIndex = false)
            => (exception = Throw.Is.Index.Invalid(index, lengthLimiter, lengthIsValidIndex)
            ? Throw.As.Arg.Index.Invalid(paramName, index, lengthLimiter.Length, lengthIsValidIndex)
            : null) is not null;

        /// <inheritdoc cref="Invalid{T}(ThrowTryArgIndexHook, string, int, Span{T}, out ArgumentIndexOutOfRangeException?, bool)"/>
        public static bool Invalid<T>(this ThrowTryArgIndexHook _, string paramName, int index, ReadOnlyMemory<T> lengthLimiter,
            [NotNullWhen(true), MaybeNullWhen(false)] out ArgumentIndexOutOfRangeException? exception,
            bool lengthIsValidIndex = false)
            => (exception = Throw.Is.Index.Invalid(index, lengthLimiter, lengthIsValidIndex)
            ? Throw.As.Arg.Index.Invalid(paramName, index, lengthLimiter.Length, lengthIsValidIndex)
            : null) is not null;

        /// <inheritdoc cref="Invalid{T}(ThrowTryArgIndexHook, string, int, Span{T}, out ArgumentIndexOutOfRangeException?, bool)"/>
        public static bool Invalid(this ThrowTryArgIndexHook _, string paramName, int index,
            [NotNullWhen(true)] ICollection? lengthLimiter,
            [NotNullWhen(true), MaybeNullWhen(false)] out ArgumentIndexOutOfRangeException? exception,
            bool lengthIsValidIndex = false)
            => (exception = Throw.Is.Index.Invalid(index, lengthLimiter, lengthIsValidIndex)
            ? Throw.As.Arg.Index.Invalid(paramName, index, lengthLimiter!.Count, lengthIsValidIndex)
            : null) is not null;

        /// <inheritdoc cref="Invalid{T}(ThrowTryArgIndexHook, string, int, Span{T}, out ArgumentIndexOutOfRangeException?, bool)"/>
        public static bool Invalid<T>(this ThrowTryArgIndexHook _, string paramName, int index,
            [NotNullWhen(true)] ICollection<T>? lengthLimiter,
            [NotNullWhen(true), MaybeNullWhen(false)] out ArgumentIndexOutOfRangeException? exception,
            bool lengthIsValidIndex = false)
            => (exception = Throw.Is.Index.Invalid(index, lengthLimiter, lengthIsValidIndex)
            ? Throw.As.Arg.Index.Invalid(paramName, index, lengthLimiter!.Count, lengthIsValidIndex)
            : null) is not null;

        /// <inheritdoc cref="Invalid{T}(ThrowTryArgIndexHook, string, int, Span{T}, out ArgumentIndexOutOfRangeException?, bool)"/>
        public static bool Invalid(this ThrowTryArgIndexHook _, string paramName, int index,
            [NotNullWhen(true)] string? lengthLimiter,
            [NotNullWhen(true), MaybeNullWhen(false)] out ArgumentIndexOutOfRangeException? exception,
            bool lengthIsValidIndex = false)
            => (exception = Throw.Is.Index.Invalid(index, lengthLimiter, lengthIsValidIndex)
            ? Throw.As.Arg.Index.Invalid(paramName, index, lengthLimiter!.Length, lengthIsValidIndex)
            : null) is not null;

        /// <inheritdoc cref="Invalid{T}(ThrowTryArgIndexHook, string, int, Span{T}, out ArgumentIndexOutOfRangeException?, bool)"/>
        public static bool Invalid(this ThrowTryArgIndexHook _, string paramName, int index,
            [NotNullWhen(true)] StringBuilder? lengthLimiter,
            [NotNullWhen(true), MaybeNullWhen(false)] out ArgumentIndexOutOfRangeException? exception,
            bool lengthIsValidIndex = false)
            => (exception = Throw.Is.Index.Invalid(index, lengthLimiter, lengthIsValidIndex)
            ? Throw.As.Arg.Index.Invalid(paramName, index, lengthLimiter!.Length, lengthIsValidIndex)
            : null) is not null;

        /// <inheritdoc cref="Invalid{T}(ThrowTryArgIndexHook, string, int, Span{T}, out ArgumentIndexOutOfRangeException?, bool)"/>
        public static bool Invalid(this ThrowTryArgIndexHook _, string paramName, int index,
            [NotNullWhen(true)] SecureString? lengthLimiter,
            [NotNullWhen(true), MaybeNullWhen(false)] out ArgumentIndexOutOfRangeException? exception,
            bool lengthIsValidIndex = false)
            => (exception = Throw.Is.Index.Invalid(index, lengthLimiter, lengthIsValidIndex)
            ? Throw.As.Arg.Index.Invalid(paramName, index, lengthLimiter!.Length, lengthIsValidIndex)
            : null) is not null;

    }
}
