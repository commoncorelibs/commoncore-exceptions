﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using CommonCore.Exceptions.Hooks;

namespace CommonCore
{
    /// <summary>
    /// The <see cref="ThrowExtensions_As"/> <c>static</c> <c>extension</c> <c>class</c>
    /// provides <see cref="ThrowAsHook"/> extension methods for the <c>Throw.As</c>
    /// property.
    /// </summary>
    /// <seealso cref="Throw.As"/>
    public static partial class ThrowExtensions_As
    {

        /// <summary>
        /// Builds <see cref="NullReferenceException"/> instance.
        /// </summary>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <returns><see cref="NullReferenceException"/> instance.</returns>
        public static NullReferenceException Null(this ThrowAsHook _) => new();

    }
}
