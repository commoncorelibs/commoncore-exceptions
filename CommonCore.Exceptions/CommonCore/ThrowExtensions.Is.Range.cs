﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using CommonCore.Exceptions.Hooks;

namespace CommonCore
{
    /// <summary>
    /// The <see cref="ThrowExtensions_Is_Range"/> <c>static</c> <c>extension</c> <c>class</c>
    /// provides <see cref="ThrowIsRangeHook"/> extension methods for the <c>Throw.Is.Range</c>
    /// property.
    /// </summary>
    /// <seealso cref="Throw.Is"/>
    public static partial class ThrowExtensions_Is_Range
    {

        #region Equal

        /// <summary>
        /// Validates if the specified <paramref name="value"/> is <c>equal</c> to
        /// the specified <paramref name="target"/>.
        /// </summary>
        /// <typeparam name="T">The type of <see cref="IComparable{T}"/> value to validate.</typeparam>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="value">The value to validate.</param>
        /// <param name="target">The target to validate against.</param>
        /// <returns>
        /// <c>true</c> if <paramref name="value"/> is <c>equal</c> to <paramref name="target"/>;
        /// otherwise <c>false</c>.
        /// </returns>
        public static bool Equal<T>(this ThrowIsRangeHook _, T value, T target)
            where T : IComparable<T> => value.CompareTo(target) == 0;

        #endregion

        #region NotEqual

        /// <summary>
        /// Validates if the specified <paramref name="value"/> is not <c>equal</c> to
        /// the specified <paramref name="target"/>.
        /// </summary>
        /// <typeparam name="T">The type of <see cref="IComparable{T}"/> value to validate.</typeparam>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="value">The value to validate.</param>
        /// <param name="target">The target to validate against.</param>
        /// <returns>
        /// <c>true</c> if <paramref name="value"/> is not <c>equal</c> to <paramref name="target"/>;
        /// otherwise <c>false</c>.
        /// </returns>
        public static bool NotEqual<T>(this ThrowIsRangeHook _, T value, T target)
            where T : IComparable<T> => value.CompareTo(target) != 0;

        #endregion

        #region Less

        /// <summary>
        /// Validates if the specified <paramref name="value"/> is <c>less</c> than
        /// the specified <paramref name="min"/>.
        /// </summary>
        /// <typeparam name="T">The type of <see cref="IComparable{T}"/> value to validate.</typeparam>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="value">The value to validate.</param>
        /// <param name="min">The target to validate against.</param>
        /// <returns>
        /// <c>true</c> if <paramref name="value"/> is <c>less</c> than <paramref name="min"/>;
        /// otherwise <c>false</c>.
        /// </returns>
        public static bool Less<T>(this ThrowIsRangeHook _, T value, T min)
            where T : IComparable<T> => value.CompareTo(min) < 0;

        #endregion

        #region LessOrEqual

        /// <summary>
        /// Validates if the specified <paramref name="value"/> is <c>less</c> than or <c>equal</c> to
        /// the specified <paramref name="min"/>.
        /// </summary>
        /// <typeparam name="T">The type of <see cref="IComparable{T}"/> value to validate.</typeparam>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="value">The value to validate.</param>
        /// <param name="min">The target to validate against.</param>
        /// <returns>
        /// <c>true</c> if <paramref name="value"/> is <c>less</c> than or <c>equal</c> to <paramref name="min"/>;
        /// otherwise <c>false</c>.
        /// </returns>
        public static bool LessOrEqual<T>(this ThrowIsRangeHook _, T value, T min)
            where T : IComparable<T> => value.CompareTo(min) <= 0;

        #endregion

        #region More

        /// <summary>
        /// Validates if the specified <paramref name="value"/> is <c>more</c> than
        /// the specified <paramref name="max"/>.
        /// </summary>
        /// <typeparam name="T">The type of <see cref="IComparable{T}"/> value to validate.</typeparam>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="value">The value to validate.</param>
        /// <param name="max">The target to validate against.</param>
        /// <returns>
        /// <c>true</c> if <paramref name="value"/> is <c>more</c> than <paramref name="max"/>;
        /// otherwise <c>false</c>.
        /// </returns>
        public static bool More<T>(this ThrowIsRangeHook _, T value, T max)
            where T : IComparable<T> => value.CompareTo(max) > 0;

        #endregion

        #region MoreOrEqual

        /// <summary>
        /// Validates if the specified <paramref name="value"/> is <c>more</c> than or <c>equal</c> to
        /// the specified <paramref name="max"/>.
        /// </summary>
        /// <typeparam name="T">The type of <see cref="IComparable{T}"/> value to validate.</typeparam>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="value">The value to validate.</param>
        /// <param name="max">The target to validate against.</param>
        /// <returns>
        /// <c>true</c> if <paramref name="value"/> is <c>more</c> than or <c>equal</c> to <paramref name="max"/>;
        /// otherwise <c>false</c>.
        /// </returns>
        public static bool MoreOrEqual<T>(this ThrowIsRangeHook _, T value, T max)
            where T : IComparable<T> => value.CompareTo(max) >= 0;

        #endregion

        #region Inside

        /// <summary>
        /// Validates if the specified <paramref name="value"/>
        /// 
        /// is <c>more</c> than (<paramref name="minInclusive"/> is <c>false</c>)
        /// or <c>more</c> than or <c>equal</c> to (<paramref name="minInclusive"/> is <c>true</c>)
        /// the specified <paramref name="min"/> target;
        /// 
        /// or is <c>less</c> than (<paramref name="maxInclusive"/> is <c>false</c>)
        /// or <c>less</c> than or <c>equal</c> to (<paramref name="minInclusive"/> is <c>true</c>)
        /// the specified <paramref name="max"/> target.
        /// </summary>
        /// <typeparam name="T">The type of <see cref="IComparable{T}"/> value to validate.</typeparam>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="value">The value to validate.</param>
        /// <param name="min">The minimum target to validate against.</param>
        /// <param name="max">The maximum target to validate against.</param>
        /// <param name="minInclusive">Whether the minimum target is inclusive; otherwise exclusive.</param>
        /// <param name="maxInclusive">Whether the maximum target is inclusive; otherwise exclusive.</param>
        /// <returns>
        /// <c>true</c> if <paramref name="value"/> is <c>outside</c> the target range;
        /// otherwise <c>false</c>.
        /// </returns>
        public static bool Inside<T>(this ThrowIsRangeHook _, T value, T min, T max, bool minInclusive, bool maxInclusive)
            where T : IComparable<T>
            => (minInclusive ? value.CompareTo(min) >= 0 : value.CompareTo(min) > 0)
            || (maxInclusive ? value.CompareTo(max) <= 0 : value.CompareTo(max) < 0);

        /// <inheritdoc cref="Inside{T}(ThrowIsRangeHook, T, T, T, bool, bool)"/>
        public static bool Inside(this ThrowIsRangeHook _, int value, int min, int max, bool minInclusive, bool maxInclusive)
            => (minInclusive ? value.CompareTo(min) >= 0 : value.CompareTo(min) > 0)
            || (maxInclusive ? value.CompareTo(max) <= 0 : value.CompareTo(max) < 0);

        #endregion

        #region Outside

        /// <summary>
        /// Validates if the specified <paramref name="value"/>
        /// 
        /// is <c>less</c> than (<paramref name="minInclusive"/> is <c>true</c>)
        /// or <c>less</c> than or <c>equal</c> to (<paramref name="minInclusive"/> is <c>false</c>)
        /// the specified <paramref name="min"/> target;
        /// 
        /// or is <c>more</c> than (<paramref name="maxInclusive"/> is <c>true</c>)
        /// or <c>more</c> than or <c>equal</c> to (<paramref name="minInclusive"/> is <c>false</c>)
        /// the specified <paramref name="max"/> target.
        /// </summary>
        /// <typeparam name="T">The type of <see cref="IComparable{T}"/> value to validate.</typeparam>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="value">The value to validate.</param>
        /// <param name="min">The minimum target to validate against.</param>
        /// <param name="max">The maximum target to validate against.</param>
        /// <param name="minInclusive">Whether the minimum target is inclusive; otherwise exclusive.</param>
        /// <param name="maxInclusive">Whether the maximum target is inclusive; otherwise exclusive.</param>
        /// <returns>
        /// <c>true</c> if <paramref name="value"/> is <c>outside</c> the target range;
        /// otherwise <c>false</c>.
        /// </returns>
        public static bool Outside<T>(this ThrowIsRangeHook _, T value, T min, T max, bool minInclusive, bool maxInclusive)
            where T : IComparable<T>
            => (minInclusive ? value.CompareTo(min) < 0 : value.CompareTo(min) <= 0)
            || (maxInclusive ? value.CompareTo(max) > 0 : value.CompareTo(max) >= 0);

        #endregion

    }
}
