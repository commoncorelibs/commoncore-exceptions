﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using System.Diagnostics;
using CommonCore.Exceptions.Hooks;

namespace CommonCore
{
    /// <summary>
    /// The <see cref="ThrowExtensions_If_Arg_Enum"/> <c>static</c> <c>extension</c> <c>class</c>
    /// provides <see cref="ThrowIfArgEnumHook"/> extension methods for the <c>Throw.If.Arg.Enum</c>
    /// property.
    /// </summary>
    /// <seealso cref="Throw.If"/>
    public static partial class ThrowExtensions_If_Arg_Enum
    {

        /// <summary>
        /// Throws <see cref="ArgumentEnumOutOfRangeException"/> if the specified
        /// <paramref name="value"/> does not exist in its enumeration.
        /// </summary>
        /// <typeparam name="T">The type of <c>enum</c> value to validate.</typeparam>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="paramName">The name of the parameter being validated.</param>
        /// <param name="value">The <c>enum</c> value to validate.</param>
        [DebuggerStepThrough()]
        public static void Invalid<T>(this ThrowIfArgEnumHook _, string paramName, T value) where T : struct, Enum
        { if (Throw.Is.Enum.Invalid(value)) { throw Throw.As.Arg.Enum.Invalid(paramName, value); } }

    }
}
