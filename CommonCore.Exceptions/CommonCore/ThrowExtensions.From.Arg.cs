﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using CommonCore.Exceptions.Hooks;

namespace CommonCore
{
    /// <summary>
    /// The <see cref="ThrowExtensions_From_Arg"/> <c>static</c> <c>extension</c> <c>class</c>
    /// provides <see cref="ThrowFromArgHook"/> extension methods for the <c>Throw.From.Arg</c>
    /// property.
    /// </summary>
    /// <seealso cref="Throw.From"/>
    public static partial class ThrowExtensions_From_Arg
    {

        /// <summary>
        /// Builds <see cref="string"/> exception message from the specified arguments for <see cref="ArgumentDefaultException"/>.
        /// </summary>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="paramName">The name of the caller parameter.</param>
        /// <returns><see cref="string"/> exception message for <see cref="ArgumentDefaultException"/>.</returns>
        public static string Default(this ThrowFromArgHook _, string paramName)
            => "Argument '{0}' value must not be default.".AsFormat(paramName);

        /// <summary>
        /// Builds <see cref="string"/> exception message from the specified arguments for <see cref="ArgumentDefaultOrEmptyException"/>.
        /// </summary>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="paramName">The name of the caller parameter.</param>
        /// <returns><see cref="string"/> exception message for <see cref="ArgumentDefaultOrEmptyException"/>.</returns>
        public static string DefaultOrEmpty(this ThrowFromArgHook _, string paramName)
            => "Argument '{0}' value must not be default or empty.".AsFormat(paramName);

        /// <summary>
        /// Builds <see cref="string"/> exception message from the specified arguments for <see cref="ArgumentEmptyException"/>.
        /// </summary>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="paramName">The name of the caller parameter.</param>
        /// <returns><see cref="string"/> exception message for <see cref="ArgumentEmptyException"/>.</returns>
        public static string Empty(this ThrowFromArgHook _, string paramName)
            => "Argument '{0}' value must not be empty.".AsFormat(paramName);

        /// <summary>
        /// Builds <see cref="string"/> exception message from the specified arguments for <see cref="ArgumentNullException"/>.
        /// </summary>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="paramName">The name of the caller parameter.</param>
        /// <returns><see cref="string"/> exception message for <see cref="ArgumentNullException"/>.</returns>
        public static string Null(this ThrowFromArgHook _, string paramName)
            => "Argument '{0}' value must not be null.".AsFormat(paramName);

        /// <summary>
        /// Builds <see cref="string"/> exception message from the specified arguments for <see cref="ArgumentNullOrEmptyException"/>.
        /// </summary>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="paramName">The name of the caller parameter.</param>
        /// <returns><see cref="string"/> exception message for <see cref="ArgumentNullOrEmptyException"/>.</returns>
        public static string NullOrEmpty(this ThrowFromArgHook _, string paramName)
            => "Argument '{0}' value must not be null or empty.".AsFormat(paramName);

        /// <summary>
        /// Builds <see cref="string"/> exception message from the specified arguments for <see cref="ArgumentReadOnlyException"/>.
        /// </summary>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="paramName">The name of the caller parameter.</param>
        /// <returns><see cref="string"/> exception message for <see cref="ArgumentReadOnlyException"/>.</returns>
        public static string ReadOnly(this ThrowFromArgHook _, string paramName)
            => "Argument '{0}' value must not be readonly.".AsFormat(paramName);

        /// <summary>
        /// Builds <see cref="string"/> exception message from the specified arguments for <see cref="ArgumentReadOnlyException"/>.
        /// </summary>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="paramName">The name of the caller parameter.</param>
        /// <returns><see cref="string"/> exception message for <see cref="ArgumentReadOnlyException"/>.</returns>
        public static string NotReadOnly(this ThrowFromArgHook _, string paramName)
            => "Argument '{0}' value must be readonly.".AsFormat(paramName);

    }
}
