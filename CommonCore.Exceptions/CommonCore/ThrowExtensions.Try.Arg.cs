﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Security;
using System.Text;
using CommonCore.Exceptions.Hooks;

namespace CommonCore
{
    /// <summary>
    /// The <see cref="ThrowExtensions_Try_Arg"/> <c>static</c> <c>extension</c> <c>class</c>
    /// provides <see cref="ThrowTryArgHook"/> extension methods for the <c>Throw.Try.Arg</c>
    /// property.
    /// </summary>
    /// <seealso cref="Throw.Try"/>
    public static partial class ThrowExtensions_Try_Arg
    {

        /// <summary>
        /// Provides <see cref="ArgumentDefaultException"/> if the specified <paramref name="value"/> is <c>default</c>.
        /// </summary>
        /// <typeparam name="T">The type of <c>struct</c> object to validate.</typeparam>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="paramName">The name of the parameter being validated.</param>
        /// <param name="value">The value to validate.</param>
        /// <param name="exception">
        /// Set to a <see cref="ArgumentDefaultException"/> if validation is <c>true</c>;
        /// otherwise <c>null</c>.
        /// </param>
        /// <returns><c>true</c> if <paramref name="value"/> is <c>default</c>; otherwise <c>false</c>.</returns>
        public static bool Default<T>(this ThrowTryArgHook _, string paramName, T value,
            [NotNullWhen(true), MaybeNullWhen(false)] out ArgumentDefaultException? exception)
            where T : struct
            => (exception = Throw.Is.Default(value) ? Throw.As.Arg.Default(paramName) : null) is not null;

        /// <summary>
        /// Provides <see cref="ArgumentDefaultOrEmptyException"/> if the specified <paramref name="value"/> is <c>default</c> or <c>empty</c>.
        /// </summary>
        /// <typeparam name="T">The type of <c>struct</c> object to validate.</typeparam>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="paramName">The name of the parameter being validated.</param>
        /// <param name="value">The value to validate.</param>
        /// <param name="exception">
        /// Set to a <see cref="ArgumentDefaultOrEmptyException"/> if validation is <c>true</c>;
        /// otherwise <c>null</c>.
        /// </param>
        /// <returns><c>true</c> if <paramref name="value"/> is <c>default</c> or <c>empty</c>; otherwise <c>false</c>.</returns>
        public static bool DefaultOrEmpty<T>(this ThrowTryArgHook _, string paramName, Span<T> value,
            [NotNullWhen(true), MaybeNullWhen(false)] out ArgumentDefaultOrEmptyException? exception)
            => (exception = Throw.Is.DefaultOrEmpty(value) ? Throw.As.Arg.DefaultOrEmpty(paramName) : null) is not null;

        /// <inheritdoc cref="DefaultOrEmpty{T}(ThrowTryArgHook, string, Span{T}, out ArgumentDefaultOrEmptyException?)"/>
        public static bool DefaultOrEmpty<T>(this ThrowTryArgHook _, string paramName, ReadOnlySpan<T> value,
            [NotNullWhen(true), MaybeNullWhen(false)] out ArgumentDefaultOrEmptyException? exception)
            => (exception = Throw.Is.DefaultOrEmpty(value) ? Throw.As.Arg.DefaultOrEmpty(paramName) : null) is not null;

        /// <inheritdoc cref="DefaultOrEmpty{T}(ThrowTryArgHook, string, Span{T}, out ArgumentDefaultOrEmptyException?)"/>
        public static bool DefaultOrEmpty<T>(this ThrowTryArgHook _, string paramName, Memory<T> value,
            [NotNullWhen(true), MaybeNullWhen(false)] out ArgumentDefaultOrEmptyException? exception)
            => (exception = Throw.Is.DefaultOrEmpty(value) ? Throw.As.Arg.DefaultOrEmpty(paramName) : null) is not null;

        /// <inheritdoc cref="DefaultOrEmpty{T}(ThrowTryArgHook, string, Span{T}, out ArgumentDefaultOrEmptyException?)"/>
        public static bool DefaultOrEmpty<T>(this ThrowTryArgHook _, string paramName, ReadOnlyMemory<T> value,
            [NotNullWhen(true), MaybeNullWhen(false)] out ArgumentDefaultOrEmptyException? exception)
            => (exception = Throw.Is.DefaultOrEmpty(value) ? Throw.As.Arg.DefaultOrEmpty(paramName) : null) is not null;

        /// <summary>
        /// Provides <see cref="ArgumentEmptyException"/> if the specified <paramref name="value"/> is <c>empty</c>.
        /// </summary>
        /// <typeparam name="T">The type of the wrapper value.</typeparam>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="paramName">The name of the parameter being validated.</param>
        /// <param name="value">The value to validate.</param>
        /// <param name="exception">
        /// Set to a <see cref="ArgumentEmptyException"/> if validation is <c>true</c>;
        /// otherwise <c>null</c>.
        /// </param>
        /// <returns><c>true</c> if <paramref name="value"/> is <c>empty</c>; otherwise <c>false</c>.</returns>
        public static bool Empty<T>(this ThrowTryArgHook _, string paramName, Span<T> value,
            [NotNullWhen(true), MaybeNullWhen(false)] out ArgumentEmptyException? exception)
            => (exception = Throw.Is.Empty(value) ? Throw.As.Arg.Empty(paramName) : null) is not null;

        /// <inheritdoc cref="Empty{T}(ThrowTryArgHook, string, Span{T}, out ArgumentEmptyException?)"/>
        public static bool Empty<T>(this ThrowTryArgHook _, string paramName, ReadOnlySpan<T> value,
            [NotNullWhen(true), MaybeNullWhen(false)] out ArgumentEmptyException? exception)
            => (exception = Throw.Is.Empty(value) ? Throw.As.Arg.Empty(paramName) : null) is not null;

        /// <inheritdoc cref="Empty{T}(ThrowTryArgHook, string, Span{T}, out ArgumentEmptyException?)"/>
        public static bool Empty<T>(this ThrowTryArgHook _, string paramName, Memory<T> value,
            [NotNullWhen(true), MaybeNullWhen(false)] out ArgumentEmptyException? exception)
            => (exception = Throw.Is.Empty(value) ? Throw.As.Arg.Empty(paramName) : null) is not null;

        /// <inheritdoc cref="Empty{T}(ThrowTryArgHook, string, Span{T}, out ArgumentEmptyException?)"/>
        public static bool Empty<T>(this ThrowTryArgHook _, string paramName, ReadOnlyMemory<T> value,
            [NotNullWhen(true), MaybeNullWhen(false)] out ArgumentEmptyException? exception)
            => (exception = Throw.Is.Empty(value) ? Throw.As.Arg.Empty(paramName) : null) is not null;

        /// <summary>
        /// Provides <see cref="ArgumentEmptyException"/> if the specified <paramref name="value"/> is not <c>null</c> and is <c>empty</c>.
        /// </summary>
        /// <returns><c>true</c> if <paramref name="value"/> is not <c>null</c> and is <c>empty</c>; otherwise <c>false</c>.</returns>
        /// <inheritdoc cref="Empty{T}(ThrowTryArgHook, string, Span{T}, out ArgumentEmptyException?)"/>
        public static bool Empty(this ThrowTryArgHook _, string paramName, [NotNullWhen(true)] ICollection? value,
            [NotNullWhen(true), MaybeNullWhen(false)] out ArgumentEmptyException? exception)
            => (exception = Throw.Is.Empty(value) ? Throw.As.Arg.Empty(paramName) : null) is not null;

        /// <inheritdoc cref="Empty(ThrowTryArgHook, string, ICollection?, out ArgumentEmptyException?)"/>
        public static bool Empty<T>(this ThrowTryArgHook _, string paramName, [NotNullWhen(true)] ICollection<T>? value,
            [NotNullWhen(true), MaybeNullWhen(false)] out ArgumentEmptyException? exception)
            => (exception = Throw.Is.Empty(value) ? Throw.As.Arg.Empty(paramName) : null) is not null;

        /// <inheritdoc cref="Empty(ThrowTryArgHook, string, ICollection?, out ArgumentEmptyException?)"/>
        public static bool Empty(this ThrowTryArgHook _, string paramName, [NotNullWhen(true)] string? value,
            [NotNullWhen(true), MaybeNullWhen(false)] out ArgumentEmptyException? exception)
            => (exception = Throw.Is.Empty(value) ? Throw.As.Arg.Empty(paramName) : null) is not null;

        /// <inheritdoc cref="Empty(ThrowTryArgHook, string, ICollection?, out ArgumentEmptyException?)"/>
        public static bool Empty(this ThrowTryArgHook _, string paramName, [NotNullWhen(true)] StringBuilder? value,
            [NotNullWhen(true), MaybeNullWhen(false)] out ArgumentEmptyException? exception)
            => (exception = Throw.Is.Empty(value) ? Throw.As.Arg.Empty(paramName) : null) is not null;

        /// <inheritdoc cref="Empty(ThrowTryArgHook, string, ICollection?, out ArgumentEmptyException?)"/>
        public static bool Empty(this ThrowTryArgHook _, string paramName, [NotNullWhen(true)] SecureString? value,
            [NotNullWhen(true), MaybeNullWhen(false)] out ArgumentEmptyException? exception)
            => (exception = Throw.Is.Empty(value) ? Throw.As.Arg.Empty(paramName) : null) is not null;

        /// <summary>
        /// Provides <see cref="ArgumentNullException"/> if the specified <paramref name="value"/> is <c>null</c>.
        /// </summary>
        /// <typeparam name="T">The type of <c>class</c> object to validate.</typeparam>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="paramName">The name of the parameter being validated.</param>
        /// <param name="value">The value to validate.</param>
        /// <param name="exception">
        /// Set to a <see cref="ArgumentNullException"/> if validation is <c>true</c>;
        /// otherwise <c>null</c>.
        /// </param>
        /// <returns><c>true</c> if <paramref name="value"/> is <c>null</c>; otherwise <c>false</c>.</returns>
        public static bool Null<T>(this ThrowTryArgHook _, string paramName, [NotNullWhen(true)] T? value,
            [NotNullWhen(true), MaybeNullWhen(false)] out ArgumentNullException? exception)
            where T : class
            => (exception = Throw.Is.Null(value) ? Throw.As.Arg.Null(paramName) : null) is not null;

        /// <summary>
        /// Provides <see cref="ArgumentNullOrEmptyException"/> if the specified <paramref name="value"/> is <c>null</c> or <c>empty</c>.
        /// </summary>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="paramName">The name of the parameter being validated.</param>
        /// <param name="value">The value to validate.</param>
        /// <param name="exception">
        /// Set to a <see cref="ArgumentNullOrEmptyException"/> if validation is <c>true</c>;
        /// otherwise <c>null</c>.
        /// </param>
        /// <returns><c>true</c> if <paramref name="value"/> is <c>null</c> or <c>empty</c>; otherwise <c>false</c>.</returns>
        public static bool NullOrEmpty(this ThrowTryArgHook _, string paramName, [NotNullWhen(true)] ICollection? value,
            [NotNullWhen(true), MaybeNullWhen(false)] out ArgumentNullOrEmptyException? exception)
            => (exception = Throw.Is.NullOrEmpty(value) ? Throw.As.Arg.NullOrEmpty(paramName) : null) is not null;

        /// <inheritdoc cref="NullOrEmpty(ThrowTryArgHook, string, ICollection?, out ArgumentNullOrEmptyException?)"/>
        public static bool NullOrEmpty<T>(this ThrowTryArgHook _, string paramName, [NotNullWhen(true)] ICollection<T>? value,
            [NotNullWhen(true), MaybeNullWhen(false)] out ArgumentNullOrEmptyException? exception)
            => (exception = Throw.Is.NullOrEmpty(value) ? Throw.As.Arg.NullOrEmpty(paramName) : null) is not null;

        /// <inheritdoc cref="NullOrEmpty(ThrowTryArgHook, string, ICollection?, out ArgumentNullOrEmptyException?)"/>
        public static bool NullOrEmpty(this ThrowTryArgHook _, string paramName, [NotNullWhen(true)] string? value,
            [NotNullWhen(true), MaybeNullWhen(false)] out ArgumentNullOrEmptyException? exception)
            => (exception = Throw.Is.NullOrEmpty(value) ? Throw.As.Arg.NullOrEmpty(paramName) : null) is not null;

        /// <inheritdoc cref="NullOrEmpty(ThrowTryArgHook, string, ICollection?, out ArgumentNullOrEmptyException?)"/>
        public static bool NullOrEmpty(this ThrowTryArgHook _, string paramName, [NotNullWhen(true)] StringBuilder? value,
            [NotNullWhen(true), MaybeNullWhen(false)] out ArgumentNullOrEmptyException? exception)
            => (exception = Throw.Is.NullOrEmpty(value) ? Throw.As.Arg.NullOrEmpty(paramName) : null) is not null;

        /// <inheritdoc cref="NullOrEmpty(ThrowTryArgHook, string, ICollection?, out ArgumentNullOrEmptyException?)"/>
        public static bool NullOrEmpty(this ThrowTryArgHook _, string paramName, [NotNullWhen(true)] SecureString? value,
            [NotNullWhen(true), MaybeNullWhen(false)] out ArgumentNullOrEmptyException? exception)
            => (exception = Throw.Is.NullOrEmpty(value) ? Throw.As.Arg.NullOrEmpty(paramName) : null) is not null;

        /// <summary>
        /// Provides <see cref="ArgumentReadOnlyException"/> if the specified <paramref name="value"/> is not <c>null</c> and is <c>readonly</c>.
        /// </summary>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="paramName">The name of the parameter being validated.</param>
        /// <param name="value">The value to validate.</param>
        /// <param name="exception">
        /// Set to a <see cref="ArgumentReadOnlyException"/> if validation is <c>true</c>;
        /// otherwise <c>null</c>.
        /// </param>
        /// <returns><c>true</c> if <paramref name="value"/> is not <c>null</c> and is <c>readonly</c>; otherwise <c>false</c>.</returns>
        public static bool ReadOnly(this ThrowTryArgHook _, string paramName, [NotNullWhen(true)] SecureString? value,
            [NotNullWhen(true), MaybeNullWhen(false)] out ArgumentException? exception)
            => (exception = Throw.Is.ReadOnly(value) ? Throw.As.Arg.ReadOnly(paramName) : null) is not null;

        /// <summary>
        /// Provides <see cref="ArgumentReadOnlyException"/> if the specified <paramref name="value"/> is not <c>null</c> and is not <c>readonly</c>.
        /// </summary>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="paramName">The name of the parameter being validated.</param>
        /// <param name="value">The value to validate.</param>
        /// <param name="exception">
        /// Set to a <see cref="ArgumentReadOnlyException"/> if validation is <c>true</c>;
        /// otherwise <c>null</c>.
        /// </param>
        /// <returns><c>true</c> if <paramref name="value"/> is not <c>null</c> and is not <c>readonly</c>; otherwise <c>false</c>.</returns>
        public static bool NotReadOnly(this ThrowTryArgHook _, string paramName, [NotNullWhen(true)] SecureString? value,
            [NotNullWhen(true), MaybeNullWhen(false)] out ArgumentException? exception)
            => (exception = Throw.Is.NotReadOnly(value) ? Throw.As.Arg.NotReadOnly(paramName) : null) is not null;

    }
}
