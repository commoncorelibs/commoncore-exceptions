﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using System.Collections;
using System.Diagnostics.CodeAnalysis;
using System.Security;
using System.Text;
using CommonCore.Exceptions.Hooks;

namespace CommonCore
{
    /// <summary>
    /// The <see cref="ThrowExtensions_Try_Arg_Range"/> <c>static</c> <c>extension</c> <c>class</c>
    /// provides <see cref="ThrowTryArgRangeHook"/> extension methods for the <c>Throw.Try.Arg.Range</c>
    /// property.
    /// </summary>
    /// <seealso cref="Throw.Try"/>
    public static partial class ThrowExtensions_Try_Arg_Range
    {

        /// <summary>
        /// Provides <see cref="ArgumentOutOfRangeException"/> if the specified <paramref name="value"/>
        /// is <c>less</c> than the specified <paramref name="min"/>.
        /// </summary>
        /// <typeparam name="T">The type of the <see cref="IComparable{T}"/> implementing value to validate.</typeparam>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="paramName">The name of the parameter being validated.</param>
        /// <param name="value">The value to validate.</param>
        /// <param name="min">The minimum value to validate against.</param>
        /// <param name="exception">
        /// Set to a <see cref="ArgumentOutOfRangeException"/> if validation is <c>true</c>;
        /// otherwise <c>null</c>.
        /// </param>
        /// <returns><c>true</c> if <paramref name="value"/> is <c>less</c> than the specified <paramref name="min"/>; otherwise <c>false</c>.</returns>
        public static bool Less<T>(this ThrowTryArgRangeHook _, string paramName, T value, T min,
            [NotNullWhen(true), MaybeNullWhen(false)] out ArgumentOutOfRangeException? exception)
            where T : IComparable<T>
            => (exception = Throw.Is.Range.Less(value, min) ? Throw.As.Arg.Range.Less(paramName, value, min) : null) is not null;

        /// <summary>
        /// Provides <see cref="ArgumentOutOfRangeException"/> if the specified <paramref name="value"/>
        /// is <c>less</c> than or <c>equal</c> to the specified <paramref name="min"/>.
        /// </summary>
        /// <typeparam name="T">The type of the <see cref="IComparable{T}"/> implementing value to validate.</typeparam>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="paramName">The name of the parameter being validated.</param>
        /// <param name="value">The value to validate.</param>
        /// <param name="min">The minimum value to validate against.</param>
        /// <param name="exception">
        /// Set to a <see cref="ArgumentOutOfRangeException"/> if validation is <c>true</c>;
        /// otherwise <c>null</c>.
        /// </param>
        /// <returns><c>true</c> if <paramref name="value"/> is out of range; otherwise <c>false</c>.</returns>
        public static bool LessOrEqual<T>(this ThrowTryArgRangeHook _, string paramName, T value, T min,
            [NotNullWhen(true), MaybeNullWhen(false)] out ArgumentOutOfRangeException? exception)
            where T : IComparable<T>
            => (exception = Throw.Is.Range.LessOrEqual(value, min) ? Throw.As.Arg.Range.LessOrEqual(paramName, value, min) : null) is not null;

        /// <summary>
        /// Provides <see cref="ArgumentOutOfRangeException"/> if the specified <paramref name="value"/>
        /// is <c>more</c> than the specified <paramref name="max"/>.
        /// </summary>
        /// <typeparam name="T">The type of the <see cref="IComparable{T}"/> implementing value to validate.</typeparam>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="paramName">The name of the parameter being validated.</param>
        /// <param name="value">The value to validate.</param>
        /// <param name="max">The maximum value to validate against.</param>
        /// <param name="exception">
        /// Set to a <see cref="ArgumentOutOfRangeException"/> if validation is <c>true</c>;
        /// otherwise <c>null</c>.
        /// </param>
        /// <returns><c>true</c> if <paramref name="value"/> is <c>more</c> than the specified <paramref name="max"/>; otherwise <c>false</c>.</returns>
        public static bool More<T>(this ThrowTryArgRangeHook _, string paramName, T value, T max,
            [NotNullWhen(true), MaybeNullWhen(false)] out ArgumentOutOfRangeException? exception)
            where T : IComparable<T>
            => (exception = Throw.Is.Range.More(value, max) ? Throw.As.Arg.Range.More(paramName, value, max) : null) is not null;

        /// <summary>
        /// Provides <see cref="ArgumentOutOfRangeException"/> if the specified <paramref name="value"/>
        /// is <c>more</c> than or <c>equal</c> to the specified <paramref name="max"/>.
        /// </summary>
        /// <typeparam name="T">The type of the <see cref="IComparable{T}"/> implementing value to validate.</typeparam>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="paramName">The name of the parameter being validated.</param>
        /// <param name="value">The value to validate.</param>
        /// <param name="max">The maximum value to validate against.</param>
        /// <param name="exception">
        /// Set to a <see cref="ArgumentOutOfRangeException"/> if validation is <c>true</c>;
        /// otherwise <c>null</c>.
        /// </param>
        /// <returns><c>true</c> if <paramref name="value"/> is out of range; otherwise <c>false</c>.</returns>
        public static bool MoreOrEqual<T>(this ThrowTryArgRangeHook _, string paramName, T value, T max,
            [NotNullWhen(true), MaybeNullWhen(false)] out ArgumentOutOfRangeException? exception)
            where T : IComparable<T>
            => (exception = Throw.Is.Range.MoreOrEqual(value, max) ? Throw.As.Arg.Range.MoreOrEqual(paramName, value, max) : null) is not null;

        /// <summary>
        /// Provides <see cref="ArgumentOutOfRangeException"/> if the specified <paramref name="value"/> is <c>outside</c> the target range.
        /// </summary>
        /// <typeparam name="T">The type of the <see cref="IComparable{T}"/> implementing value to validate.</typeparam>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="paramName">The name of the parameter being validated.</param>
        /// <param name="value">The value to validate.</param>
        /// <param name="min">The minimum target to validate against.</param>
        /// <param name="max">The maximum target to validate against.</param>
        /// <param name="minInclusive">Whether the minimum target is inclusive; otherwise exclusive.</param>
        /// <param name="maxInclusive">Whether the maximum target is inclusive; otherwise exclusive.</param>
        /// <param name="exception">
        /// Set to a <see cref="ArgumentOutOfRangeException"/> if validation is <c>true</c>;
        /// otherwise <c>null</c>.
        /// </param>
        /// <returns><c>true</c> if <paramref name="value"/> is <c>outside</c> the target range; otherwise <c>false</c>.</returns>
        public static bool Outside<T>(this ThrowTryArgRangeHook _, string paramName, T value, T min, T max, bool minInclusive, bool maxInclusive,
            [NotNullWhen(true), MaybeNullWhen(false)] out ArgumentOutOfRangeException? exception)
            where T : IComparable<T>
            => (exception = Throw.Is.Range.Outside(value, min, max, minInclusive, maxInclusive)
            ? Throw.As.Arg.Range.Outside(paramName, value, min, max, minInclusive, maxInclusive)
            : null) is not null;

    }
}
