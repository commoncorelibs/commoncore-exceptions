﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using System.Collections;
using System.Collections.Generic;
using System.Security;
using System.Text;
using CommonCore.Exceptions.Hooks;

namespace CommonCore
{
    /// <summary>
    /// The <see cref="ThrowExtensions_Is_Index"/> <c>static</c> <c>extension</c> <c>class</c>
    /// provides <see cref="ThrowIsIndexHook"/> extension methods for the <c>Throw.Is.Index</c>
    /// property.
    /// </summary>
    /// <seealso cref="Throw.Is"/>
    public static partial class ThrowExtensions_Is_Index
    {

        #region Valid

        /// <summary>
        /// Validates if the specified <paramref name="index"/> is <c>inside</c> the valid range.
        /// </summary>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="index">The <c>index</c> to validate.</param>
        /// <param name="lengthLimit">The maximum length target to validate against.</param>
        /// <param name="lengthIsValidIndex">Whether the maximum length target is inclusive; otherwise exclusive.</param>
        /// <returns><c>true</c> if <paramref name="index"/> is <c>inside</c> the valid range; otherwise <c>false</c>.</returns>
        public static bool Valid(this ThrowIsIndexHook _, int index, int lengthLimit, bool lengthIsValidIndex = false)
            => index >= 0 && (index < lengthLimit || (lengthIsValidIndex && index == lengthLimit));

        /// <summary>
        /// Validates if the specified <paramref name="index"/> is <c>inside</c> the valid range.
        /// </summary>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="index">The <c>index</c> to validate.</param>
        /// <param name="lengthLimiter">The object used to determine the maximum length target to validate against.</param>
        /// <param name="lengthIsValidIndex">Whether the maximum length target is inclusive; otherwise exclusive.</param>
        /// <returns><c>true</c> if <paramref name="index"/> is <c>inside</c> the valid range; otherwise <c>false</c>.</returns>
        public static bool Valid<T>(this ThrowIsIndexHook _, int index, Span<T> lengthLimiter, bool lengthIsValidIndex = false)
            => _.Valid(index, lengthLimiter.Length, lengthIsValidIndex);

        /// <inheritdoc cref="Valid{T}(ThrowIsIndexHook, int, Span{T}, bool)"/>
        public static bool Valid<T>(this ThrowIsIndexHook _, int index, ReadOnlySpan<T> lengthLimiter, bool lengthIsValidIndex = false)
            => _.Valid(index, lengthLimiter.Length, lengthIsValidIndex);

        /// <inheritdoc cref="Valid{T}(ThrowIsIndexHook, int, Span{T}, bool)"/>
        public static bool Valid<T>(this ThrowIsIndexHook _, int index, Memory<T> lengthLimiter, bool lengthIsValidIndex = false)
            => _.Valid(index, lengthLimiter.Length, lengthIsValidIndex);

        /// <inheritdoc cref="Valid{T}(ThrowIsIndexHook, int, Span{T}, bool)"/>
        public static bool Valid<T>(this ThrowIsIndexHook _, int index, ReadOnlyMemory<T> lengthLimiter, bool lengthIsValidIndex = false)
            => _.Valid(index, lengthLimiter.Length, lengthIsValidIndex);

        /// <inheritdoc cref="Valid{T}(ThrowIsIndexHook, int, Span{T}, bool)"/>
        public static bool Valid(this ThrowIsIndexHook _, int index, ICollection? lengthLimiter, bool lengthIsValidIndex = false)
            => lengthLimiter is not null && _.Valid(index, lengthLimiter.Count, lengthIsValidIndex);

        /// <inheritdoc cref="Valid{T}(ThrowIsIndexHook, int, Span{T}, bool)"/>
        public static bool Valid<T>(this ThrowIsIndexHook _, int index, ICollection<T>? lengthLimiter, bool lengthIsValidIndex = false)
            => lengthLimiter is not null && _.Valid(index, lengthLimiter.Count, lengthIsValidIndex);

        /// <inheritdoc cref="Valid{T}(ThrowIsIndexHook, int, Span{T}, bool)"/>
        public static bool Valid(this ThrowIsIndexHook _, int index, string? lengthLimiter, bool lengthIsValidIndex = false)
            => lengthLimiter is not null && _.Valid(index, lengthLimiter.Length, lengthIsValidIndex);

        /// <inheritdoc cref="Valid{T}(ThrowIsIndexHook, int, Span{T}, bool)"/>
        public static bool Valid(this ThrowIsIndexHook _, int index, StringBuilder? lengthLimiter, bool lengthIsValidIndex = false)
            => lengthLimiter is not null && _.Valid(index, lengthLimiter.Length, lengthIsValidIndex);

        /// <inheritdoc cref="Valid{T}(ThrowIsIndexHook, int, Span{T}, bool)"/>
        public static bool Valid(this ThrowIsIndexHook _, int index, SecureString? lengthLimiter, bool lengthIsValidIndex = false)
            => lengthLimiter is not null && _.Valid(index, lengthLimiter.Length, lengthIsValidIndex);

        #endregion

        #region Invalid

        /// <summary>
        /// Validates if the specified <paramref name="index"/> is <c>outside</c> the valid range.
        /// </summary>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="index">The <c>index</c> to validate.</param>
        /// <param name="lengthLimit">The maximum length target to validate against.</param>
        /// <param name="lengthIsValidIndex">Whether the maximum length target is inclusive; otherwise exclusive.</param>
        /// <returns><c>true</c> if <paramref name="index"/> is <c>outside</c> the valid range; otherwise <c>false</c>.</returns>
        public static bool Invalid(this ThrowIsIndexHook _, int index, int lengthLimit, bool lengthIsValidIndex = false)
            => index < 0 || index > lengthLimit || (!lengthIsValidIndex && index == lengthLimit);

        /// <summary>
        /// Validates if the specified <paramref name="index"/> is <c>outside</c> the valid range.
        /// </summary>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="index">The <c>index</c> to validate.</param>
        /// <param name="lengthLimiter">The object used to determine the maximum length target to validate against.</param>
        /// <param name="lengthIsValidIndex">Whether the maximum length target is inclusive; otherwise exclusive.</param>
        /// <returns><c>true</c> if <paramref name="index"/> is <c>outside</c> the valid range; otherwise <c>false</c>.</returns>
        public static bool Invalid<T>(this ThrowIsIndexHook _, int index, Span<T> lengthLimiter, bool lengthIsValidIndex = false)
            => _.Invalid(index, lengthLimiter.Length, lengthIsValidIndex);

        /// <inheritdoc cref="Invalid{T}(ThrowIsIndexHook, int, Span{T}, bool)"/>
        public static bool Invalid<T>(this ThrowIsIndexHook _, int index, ReadOnlySpan<T> lengthLimiter, bool lengthIsValidIndex = false)
            => _.Invalid(index, lengthLimiter.Length, lengthIsValidIndex);

        /// <inheritdoc cref="Invalid{T}(ThrowIsIndexHook, int, Span{T}, bool)"/>
        public static bool Invalid<T>(this ThrowIsIndexHook _, int index, Memory<T> lengthLimiter, bool lengthIsValidIndex = false)
            => _.Invalid(index, lengthLimiter.Length, lengthIsValidIndex);

        /// <inheritdoc cref="Invalid{T}(ThrowIsIndexHook, int, Span{T}, bool)"/>
        public static bool Invalid<T>(this ThrowIsIndexHook _, int index, ReadOnlyMemory<T> lengthLimiter, bool lengthIsValidIndex = false)
            => _.Invalid(index, lengthLimiter.Length, lengthIsValidIndex);

        /// <inheritdoc cref="Invalid{T}(ThrowIsIndexHook, int, Span{T}, bool)"/>
        public static bool Invalid(this ThrowIsIndexHook _, int index, ICollection? lengthLimiter, bool lengthIsValidIndex = false)
            => lengthLimiter is not null && _.Invalid(index, lengthLimiter.Count, lengthIsValidIndex);

        /// <inheritdoc cref="Invalid{T}(ThrowIsIndexHook, int, Span{T}, bool)"/>
        public static bool Invalid<T>(this ThrowIsIndexHook _, int index, ICollection<T>? lengthLimiter, bool lengthIsValidIndex = false)
            => lengthLimiter is not null && _.Invalid(index, lengthLimiter.Count, lengthIsValidIndex);

        /// <inheritdoc cref="Invalid{T}(ThrowIsIndexHook, int, Span{T}, bool)"/>
        public static bool Invalid(this ThrowIsIndexHook _, int index, string? lengthLimiter, bool lengthIsValidIndex = false)
            => lengthLimiter is not null && _.Invalid(index, lengthLimiter.Length, lengthIsValidIndex);

        /// <inheritdoc cref="Invalid{T}(ThrowIsIndexHook, int, Span{T}, bool)"/>
        public static bool Invalid(this ThrowIsIndexHook _, int index, StringBuilder? lengthLimiter, bool lengthIsValidIndex = false)
            => lengthLimiter is not null && _.Invalid(index, lengthLimiter.Length, lengthIsValidIndex);

        /// <inheritdoc cref="Invalid{T}(ThrowIsIndexHook, int, Span{T}, bool)"/>
        public static bool Invalid(this ThrowIsIndexHook _, int index, SecureString? lengthLimiter, bool lengthIsValidIndex = false)
            => lengthLimiter is not null && _.Invalid(index, lengthLimiter.Length, lengthIsValidIndex);

        #endregion

    }
}
