﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using CommonCore.Exceptions.Hooks;

namespace CommonCore
{
    /// <summary>
    /// The <see cref="ThrowExtensions_From_Arg_Index"/> <c>static</c> <c>extension</c> <c>class</c>
    /// provides <see cref="ThrowFromArgIndexHook"/> extension methods for the <c>Throw.From.Arg.Index</c>
    /// property.
    /// </summary>
    /// <seealso cref="Throw.From"/>
    public static partial class ThrowExtensions_From_Arg_Index
    {

        /// <summary>
        /// Builds <see cref="string"/> exception message from the specified arguments for <see cref="ArgumentIndexOutOfRangeException"/>.
        /// </summary>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="paramName">The name of the caller parameter.</param>
        /// <param name="index">The index to validate.</param>
        /// <param name="length">The length target to validate against.</param>
        /// <returns><see cref="string"/> exception message for <see cref="ArgumentIndexOutOfRangeException"/>.</returns>
        public static string Valid(this ThrowFromArgIndexHook _, string paramName, object index, object length)
            => "Argument '{0}' value '{1}' must be less than zero or greater than or equal to length '{2}'.".AsFormat(paramName, index, length);

        /// <inheritdoc cref="Valid(ThrowFromArgIndexHook, string, object, object)"/>
        public static string ValidInclusive(this ThrowFromArgIndexHook _, string paramName, object index, object length)
            => "Argument '{0}' value '{1}' must be less than zero or greater than length '{2}'.".AsFormat(paramName, index, length);

        /// <inheritdoc cref="Valid(ThrowFromArgIndexHook, string, object, object)"/>
        public static string Invalid(this ThrowFromArgIndexHook _, string paramName, object index, object length)
            => "Argument '{0}' value '{1}' must be greater than or equal to zero and less than length '{2}'.".AsFormat(paramName, index, length);

        /// <inheritdoc cref="Valid(ThrowFromArgIndexHook, string, object, object)"/>
        public static string InvalidInclusive(this ThrowFromArgIndexHook _, string paramName, object index, object length)
            => "Argument '{0}' value '{1}' must be greater than or equal to zero and less than or equal to length '{2}'.".AsFormat(paramName, index, length);

    }
}
