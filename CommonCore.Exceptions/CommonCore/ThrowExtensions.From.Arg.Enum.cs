﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using CommonCore.Exceptions.Hooks;

namespace CommonCore
{
    /// <summary>
    /// The <see cref="ThrowExtensions_From_Arg_Enum"/> <c>static</c> <c>extension</c> <c>class</c>
    /// provides <see cref="ThrowFromArgEnumHook"/> extension methods for the <c>Throw.From.Arg.Enum</c>
    /// property.
    /// </summary>
    /// <seealso cref="Throw.From"/>
    public static partial class ThrowExtensions_From_Arg_Enum
    {

        /// <summary>
        /// Builds <see cref="string"/> exception message from the specified arguments for <see cref="ArgumentEnumOutOfRangeException"/>.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="paramName">The name of the caller parameter.</param>
        /// <param name="value">The value to validate.</param>
        /// <param name="min">The minimum target to validate against.</param>
        /// <param name="max">The maximum target to validate against.</param>
        /// <returns><see cref="string"/> exception message for <see cref="ArgumentEnumOutOfRangeException"/>.</returns>
        public static string Invalid<T>(this ThrowFromArgEnumHook _, string paramName, T value, T min, T max)
            where T : struct, Enum
            => "Argument '{0}' enum '{1}' value '{2}' must range from '{3}' to '{4}', inclusive."
            .AsFormat(paramName, typeof(T).Name, value, min, max);

    }
}
