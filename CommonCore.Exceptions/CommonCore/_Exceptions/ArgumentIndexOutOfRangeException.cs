﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;

namespace CommonCore
{
    /// <summary>
    /// The exception that is thrown when an <c>index</c> value is passed
    /// to a method that does not accept it as a valid argument.
    /// </summary>
    [ExcludeFromCodeCoverage]
    [Serializable]
    public class ArgumentIndexOutOfRangeException : ArgumentOutOfRangeException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ArgumentIndexOutOfRangeException"/> class.
        /// </summary>
        public ArgumentIndexOutOfRangeException()
            : this(null, "Argument value must be less than zero or greater than or equal to length.") { }

        /// <summary>
        /// Initializes a new instance of the <see cref="ArgumentIndexOutOfRangeException"/> class
        /// with the name of the parameter that causes this exception.
        /// </summary>
        /// <param name="paramName">The name of the parameter that caused the exception.</param>
        public ArgumentIndexOutOfRangeException(string paramName)
            : this(paramName, $"Argument '{paramName}' value must be less than zero or greater than or equal to length.") { }

        /// <summary>
        /// Initializes a new instance of the <see cref="ArgumentIndexOutOfRangeException"/> class
        /// with the name of the parameter that causes this exception and a specified
        /// error message.
        /// </summary>
        /// <param name="paramName">The name of the parameter that caused the exception.</param>
        /// <param name="message">A message that describes the error.</param>
        public ArgumentIndexOutOfRangeException(string paramName, string message)
            : base(paramName, message) { }

        ///// <summary>
        ///// Initializes a new instance of the <see cref="ArgumentIndexOutOfRangeException"/> class
        ///// with the name of the parameter that causes this exception, a specified error
        ///// message, and a specified inner exception.
        ///// </summary>
        ///// <param name="paramName">The name of the parameter that caused the exception.</param>
        ///// <param name="message">A message that describes the error.</param>
        ///// <param name="innerException">
        ///// The exception that is the cause of the current exception or <c>null</c>
        ///// if no inner exception is specified.
        ///// </param>
        //public ArgumentIndexOutOfRangeException(string paramName, string message, Exception innerException)
        //    : base(message, paramName, innerException) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="ArgumentIndexOutOfRangeException"/> class
        /// with serialized data.
        /// </summary>
        /// <param name="info">The object that holds the serialized object data.</param>
        /// <param name="context">An object that describes the source or destination of the serialized data.</param>
        [ExcludeFromCodeCoverage]
        [System.Security.SecurityCritical]
        protected ArgumentIndexOutOfRangeException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
