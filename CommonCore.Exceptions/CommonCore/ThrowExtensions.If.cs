﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using System.Diagnostics;
using CommonCore.Exceptions.Hooks;

namespace CommonCore
{
    /// <summary>
    /// The <see cref="ThrowExtensions_If"/> <c>static</c> <c>extension</c> <c>class</c>
    /// provides <see cref="ThrowIfHook"/> extension methods for the <c>Throw.If</c>
    /// property.
    /// </summary>
    /// <seealso cref="Throw.If"/>
    public static partial class ThrowExtensions_If
    {

        /// <summary>
        /// Throws <see cref="NullReferenceException"/> if the specified <paramref name="value"/> is <c>null</c>.
        /// This method is primarily intended for extension methods to verify they are being called
        /// from a valid non-null object, mimicking the behavior of non-extension methods.
        /// </summary>
        /// <typeparam name="T">The type of <c>class</c> object to validate.</typeparam>
        /// <param name="_">Dummy type to allow extension methods.</param>
        /// <param name="value">The <c>class</c> object to validate.</param>
        [DebuggerStepThrough()]
        public static void SelfNull<T>(this ThrowIfHook _, T? value) where T : class
        { if (Throw.Is.Null(value)) { throw Throw.As.Null(); } }

    }
}
